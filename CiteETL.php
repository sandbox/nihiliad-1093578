<?php

class CiteETL
{
    protected $extractor_factory;
    public function extractor_factory()
    {
        return $this->extractor_factory;
    }

    protected $transformer;
    public function transformer()
    {
        return $this->transformer;
    }

    protected $loader;
    public function loader()
    {
        return $this->loader;
    }
    
    protected $file_names;
    public function file_names()
    {
        return $this->file_names;
    }
    
    public function __construct($params)
    {
        $param_names = array(
            'extractor_factory',
            'transformer',
            'loader',
            'file_names',
        );

        // TODO: Add validation and documentation!!!
        foreach ($param_names as $param_name) {
            if (!isset($params[$param_name])) {
                throw new Exception("Missing required param '$param_name'");
            }
            $this->$param_name = $params[$param_name];
        }
        //print_r( $this );
    }
    
    public function run()
    {
        foreach ($this->file_names() as $file_name) {

            $extractor = $this->extractor_factory()->create( $file_name );
            $extractor->rewind();
            while ($extractor->valid()) {
        
                $original_citation = $extractor->current();
                $id = $extractor->key();
                //print_r($original_citation);
        
                try {
                    $transformed_citation = $this->transformer()->transform(
                        $original_citation
                    );
                } catch (Exception $e) {
                    //$serialized_citation = print_r( $original_citation, true );
                    $message = $e->getMessage();
                    error_log("$message; id: '$id'; file: '$file_name'\n");
        
                    $extractor->next();
                    continue;
                }
        
                try {
                    $this->loader()->load( $transformed_citation );
                } catch (Exception $e) {
                    //$serialized_citation = print_r( $transformed_citation, true );
                    $message = $e->getMessage();
                    error_log("$message; id: '$id'; file: '$file_name'\n");
                }
                $extractor->next();
            }
            $this->compress_loaded_file( $file_name );
        }
    }

    public function compress_loaded_file( $file_name )
    {
        $zip_file_name = "$file_name.gz";
        $zip_file_contents = file_get_contents( $file_name );
        $zip_file = gzopen($zip_file_name, "w9");
        gzwrite( $zip_file, $zip_file_contents );
        gzclose( $zip_file );
        unlink( $file_name );
    }

} // end class CiteETL
