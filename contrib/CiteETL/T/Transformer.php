<?php

//require PEAR::* ???

class CiteETL_T_Transformer {

protected $issn = '';
protected $journal_name = '';
protected $type = 'News/Magazine';

// Will these always be true????
protected $format = 'Print';
protected $language = 'eng';

protected $filter_pattern;

function __construct() {
}

function filter( $citation ) {

    list($title, $abstract, $url) =
        array(
            $citation['values']['title'],
            $citation['values']['field_abstract'][0]['value'],
            $citation['values']['field_url_0'][0]['value'],
        );

    $subjects = array();
    foreach ($citation['values']['field_subject'] as $subject_array) {
        $subjects[] = $subject_array['value'];
    }
    $subject = join('; ', $subjects);

    $combined_content = "$title $abstract $subject";

    $html_report =<<<HTML
<ul>
 <li><a href="$url">$title</a></li>
 <li>
  <h4>Abstract</h4>
  <p>
   $abstract
  </p>
 </li>
 <li>
  <h4>Subject</h4>
  <p>
   $subject
  </p>
 </li>
</ul>
HTML;

    if (!preg_match($this->filter_pattern, $combined_content)) {
        error_log( "<h3>Rejected</h3>\n$html_report\n" );
        // Note: the id and file name will be added by the CiteETL module when
        // it catches these exceptions.
        throw new Exception(
            "The article '" . $citation['title'] . "' was rejected by the relevance filter."
        );
    }
    else {
        error_log( "<h3>Accepted</h3>\n$html_report\n" );
    }
}

} // end class CiteETL_T_Transformer 
