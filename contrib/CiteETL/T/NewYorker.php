<?php

//require PEAR::* ???

require_once 'CiteETL/T/Transformer.php';
require_once 'CiteETL/T/Search/ScienceHealthNews.php';

class CiteETL_T_NewYorker extends CiteETL_T_Transformer
{
    protected $issn = '0028-792X';
    protected $journal_name = 'New Yorker';
    protected $publisher = 'The Condé Nast Publications';
    protected $filter_pattern;
    
    function __construct()
    {
        $search = new CiteETL_T_Search_ScienceHealthNews();
        $this->filter_pattern = $search->pattern();
    }
    
    function transform( $record )
    {
        // output
        $citation = array();
        $citation['values'] = array();
        $citation_values = &$citation['values'];

        $simplepie_item = $record->as_simplepie_item();
    
        $citation_values['field_abstract'][0]['value'] =
            $record->description();
    
        // May need to do something with this...
        //'callbacks' => array('required','verify_scalar','limit_title_length:128'),
        $citation_values['title'] = $record->title();
    
        $citation_values['field_journal_name'][0]['value'] = $this->journal_name;
    
        $citation_values['field_author'][0]['value'] = $record->creator();
    
        $citation_values['field_issn'][0]['value'] = $this->issn;

        $citation_values['field_publisher'][0]['value'] = $this->publisher;
    
        $year = $simplepie_item->get_date('Y');
    
        $citation_values['field_year_published'][0]['value'] = $year;
        $citation_values['field_datetime_published'][0]['value'] =
            $simplepie_item->get_date('Y-m-d H:i:s');
    
        $citation_values['timestamp'] = $simplepie_item->get_date('U');
    /* Attempting to import this field seems to cause errors...
        $citation_values['field_full_date_published'][0]['value']['mday'] =
            $simplepie_item->get_date('d'); 
        $citation_values['field_full_date_published'][0]['value']['mon'] =
            $simplepie_item->get_date('m'); 
        $citation_values['field_full_date_published'][0]['value']['year'] = $year; 
    */
    
        $citation_values['field_type'][0]['value'] = $this->type;
    
        $citation_values['field_format'][0]['value'] = $this->format;
    
        $citation_values['field_language'][0]['value'] = $this->language;
    
        // TODO: How do I prevent this from getting set to "true"? Just not even try to set it?
        //$citation_values['field_scholarly']['value'] = 0;
    
        $url = $record->primary_id();
        $citation_values['field_url_0'][0]['value'] = $url;
    
        $citation_values['field_source'][0]['value'] = 'New Yorker';

        // Collect the sources and source IDs:
        $sources = array();
        $sources[] = array(
            'source' => $this->journal_name,
            'source_id' => $url,
        );
        $citation['sources'] = $sources;
    
        $this->filter( $citation );
    
        return $citation;
    }

} // end class CiteETL_T_NewYorker 
