<?php

//require PEAR::* ???

class CiteETL_T_PubMed
{

    function transform($from)
    {
        $from_array = $from->as_array();
    
        // output
        $citation = array();
        $citation['values'] = array();
        $citation_values = &$citation['values'];
    
        // callback 'verify_scalar' ?
        $pmid = $this->transform_pmid( $from_array['MedlineCitation']['PMID'] );
        if (!isset($pmid)) {
            throw new Exception("Missing required field 'PMID'");
        }
        $citation_values['field_pmid'][0]['value'] = $pmid;
    
        $ids = $from->ids();
        foreach ($ids as $id) {
            if ('doi' == $id['type']) {
                $citation_values['field_doi'][0]['value'] = $id['value'];
                // Not using any other ids from this list right now, 
                // though we could get the pmid from it, too.
                break;
            }
        }

        list($type, $subtypes) = $this->transform_publication_types(
            $from_array['MedlineCitation']['Article']['PublicationTypeList']['PublicationType']
        );
        if (!isset($type)) {
            throw new Exception("Missing required field 'type'");
        }
        $citation_values['field_type'][0]['value'] = $type;
        foreach ($subtypes as $index => $subtype) {
            $citation_values['field_subtype'][$index]['value'] = $subtype;
        }
    
        list($title, $alternative_title) = $this->transform_title(
            $from_array['MedlineCitation']['Article']['ArticleTitle']
        );
        if (!isset($title)) {
            throw new Exception("Missing required field 'title'");
        }
        $citation_values['title'] = $title;
        $citation_values['field_alternative_title'][0]['value'] = $alternative_title;
        
        $authors = $this->transform_author_list(
            $from_array['MedlineCitation']['Article']['AuthorList']
        );
        foreach ($authors as $index => $author) {
            $citation_values['field_author'][$index]['value'] = $author;
        }
    
        $datetime_epublished = 
            $this->transform_pubmed_date( $from_array['MedlineCitation']['Article']['ArticleDate'] );
        $citation_values['field_datetime_epublished'][0]['value'] = $datetime_epublished;

        // callback 'verify_array'?
        list($datetime_published, $season, $issue_chronology, $year_published) = $this->transform_pubdate(
            $from_array['MedlineCitation']['Article']['Journal']['JournalIssue']['PubDate']
        );
        // In some cases, the PubMed main publication date will contain only the year, and the electronic
        // publication date will be more precise:
        $citation_values['field_datetime_published'][0]['value'] = (
            (preg_match('/^\d{4}-00-00/', $datetime_published) && isset($datetime_epublished)) ?
            $datetime_epublished :
            $datetime_published
        );
        $citation_values['field_season'][0]['value'] = $season;
        $citation_values['field_issue_chronology'][0]['value'] = $issue_chronology;
        $citation_values['field_year_published'][0]['value'] = $year_published;
    
        // callback 'verify_scalar' ?
        $citation_values['field_abstract'][0]['value'] = $this->transform_abstract(
            $from_array['MedlineCitation']['Article']['Abstract']['AbstractText']
        );
    
        // callback 'verify_array'?
        $subjects = $this->transform_mesh_heading_list(
            $from_array['MedlineCitation']['MeshHeadingList']['MeshHeading']
        );
        foreach ($subjects as $index => $subject) {
            $citation_values['field_subject'][$index]['value'] = $subject;
        }
    
        // callback 'verify_scalar' ?
        $citation_values['field_journal_name'][0]['value'] = 
            $from_array['MedlineCitation']['Article']['Journal']['Title'];
    
        // callback 'verify_scalar' ?
        $citation_values['field_issn'][0]['value'] = 
            $from_array['MedlineCitation']['Article']['Journal']['ISSN']['_content'];
    
        // callback 'verify_scalar' ?
        $citation_values['field_volume'][0]['value'] = 
            $from_array['MedlineCitation']['Article']['Journal']['JournalIssue']['Volume'];
    
        // callback 'verify_scalar' ?
        $citation_values['field_issue'][0]['value'] = 
            $from_array['MedlineCitation']['Article']['Journal']['JournalIssue']['Issue'];
    
        // callback 'verify_scalar' ?
        list($pagination, $start_page, $end_page) = $this->transform_pagination(
            $from_array['MedlineCitation']['Article']['Pagination']['MedlinePgn']
        );
        $citation_values['field_pagination'][0]['value'] = $pagination;
        $citation_values['field_start_page'][0]['value'] = $start_page;
        $citation_values['field_end_page'][0]['value'] = $end_page;
        
        // callback 'verify_scalar' ?
        $citation_values['field_format'][0]['value'] = 
            $from_array['MedlineCitation']['Article']['PubModel'];
    
        // Source fields:

        $citation_values['field_source'][0]['value'] = 'PubMed';

        $citation_values['field_source_datetime_created'][0]['value'] =
            $this->transform_pubmed_date( $from_array['MedlineCitation']['DateCreated'] );

        $citation_values['field_source_datetime_completed'][0]['value'] =
            $this->transform_pubmed_date( $from_array['MedlineCitation']['DateCompleted'] );

        $citation_values['field_source_datetime_revised'][0]['value'] =
            $this->transform_pubmed_date( $from_array['MedlineCitation']['DateRevised'] );

        $citation_values['field_source_status'][0]['value'] = 
            $from_array['MedlineCitation']['Status'];
    
        $citation_values['field_source_url'][0]['value'] = 
            'http://www.ncbi.nlm.nih.gov/pubmed/' . $pmid;
    
        // Publisher fields:

        $citation_values['field_publisher_status'][0]['value'] = 
            $from_array['PubmedData']['PublicationStatus'];
    
        $citation_values['field_language'][0]['value'] = $this->transform_language(
            $from_array['MedlineCitation']['Article']['Language']
        );
    
        // Collect the sources and source IDs:
        $sources = array();
        $sources[] = array(
            'source' => 'PubMed',
            'source_id' => $citation_values['field_pmid'][0]['value'],
        );
        $citation['sources'] = $sources;
    
        return $citation;
    }
    
    function transform_pmid($pubmed_pmid)
    {
        // The PMID element may have an attribute, in which case it wall be an array.
        return (is_array($pubmed_pmid) ? $pubmed_pmid[_content] : $pubmed_pmid);
    }

    function transform_abstract($pubmed_abstract)
    {
        // There may be only one AbstractText element, or there may be several, in which case each will
        // have a Label, NlmCategory, and _content:
        if (is_array($pubmed_abstract)) {
            $abstract_sections = array();
            foreach ($pubmed_abstract as $abstract_array) {
                $abstract_sections[] = $abstract_array['Label'] . ": " . $abstract_array['_content'];
            }
            $abstract = join(' ', $abstract_sections);
        } else {
            $abstract = $pubmed_abstract;
        }
        return $abstract;
    }

    protected $month_abbr_to_digits_map = array(
     'jan' => '01',
     'feb' => '02',
     'mar' => '03',
     'apr' => '04',
     'may' => '05',
     'jun' => '06',
     'jul' => '07',
     'aug' => '08',
     'sep' => '09',
     'oct' => '10',
     'nov' => '11',
     'dec' => '12',
    );
    
    // Transform the PubDate, which is mostly set by the publisher of
    // the work and not by PubMed. These can get hairy.
    function transform_pubdate($pubdate)
    {
        unset( $year );
        if (array_key_exists('MedlineDate', $pubdate)) {
            $issue_chronology = trim( $pubdate['MedlineDate'] );
            $chron_parts = preg_split('/[\s-]+/', $issue_chronology);
            //echo "chron_parts = \n"; print_r( $chron_parts );
            $year = array_shift( $chron_parts );
            if (!preg_match('/^\d{4}$/', $year)) {
                throw new Exception("Unrecognized MedlineDate format: '$issue_chronology'");
            }
    
            $month = strtolower(trim(array_shift( $chron_parts )));
            if (array_key_exists($month, $this->month_abbr_to_digits_map)) {
                $month = $this->month_abbr_to_digits_map[$month];
            } else {
                $month = '00';
            }
    
            $day = trim(array_shift( $chron_parts ));
            if ($month != '00' && preg_match('/^\d(\d)?$/', $day)) {
                $day = sprintf('%02d', $day);
            } else {
                $day = '00';
            }
        } else {   
            if (!array_key_exists('Year', $pubdate)) {
                throw new Exception("No 'Year' value found in publication date");
            }
            $year = trim( $pubdate['Year'] );
        
            if (array_key_exists('Month', $pubdate)) {
                $from_month = $pubdate['Month'];
                $month = trim(strtolower($from_month));
                if (preg_match('/^[a-z]+$/', $month)) {
                    $month = $this->month_abbr_to_digits_map[$month];
                    if (!preg_match('/^\d\d$/', $month)) {
                        throw new Exception("Unexpected month abbreviation '$from_month'");
                    }
    
                // Don't think PubMed uses this format, but just in case:
                } else if (preg_match('/^\d(\d)?$/', $month)) {
                    $month = sprintf('%02d', $month);
                } else {
                    throw new Exception("Unexpected month abbreviation '$from_month'");
                }
    
            } else {
                $month = '00';
            }
        
            if ($month != '00' && array_key_exists('Day', $pubdate)) {
                $day = trim( $pubdate['Day'] );
                if (!preg_match('/^\d(\d)?$/', $day)) {
                    throw new Exception("Unexpected day fromat '$day'");
                }
                $day = sprintf('%02d', $day);
            } else {
                $day = '00';
            }
        
            if (array_key_exists('Season', $pubdate)) {
                $season = $pubdate['Season'];
            }
        }
    
        return array(
            // TODO: Is there a way to make this datetime more precise?
            "$year-$month-$day 00:00:00",
            $season,
            $issue_chronology,
            $year,
        );
    }
    
    // Transform dates created internally by PubMed.
    function transform_pubmed_date($medline_date)
    {
        // Some of these dates may not be defined, and that's OK.
        // We only use them for hints about when to reload, anyway.
        if (null === $medline_date) {
            return null;
        }
        $year = trim( $medline_date['Year'] );
        $month = sprintf('%02d', $medline_date['Month']);
        $day = sprintf('%02d', $medline_date['Day']);
        return "$year-$month-$day 00:00:00";
    }

    function transform_title($title)
    {
        unset($alternative_title);
        $length_limit = 128;
        if (strlen($title) <= $length_limit) {
            return array($title, $alternative_title);
        }
    
        $alternative_title = $title;
        $title = substr($title, 0, $length_limit - 3);
        // TODO: Poor man's ellipsis: replace with utf8 0x2026.
        $title .= '...';
            
        return array($title, $alternative_title);
    }
    
     //'' => 'Book Review',
     //'' => 'Research Trial',
     //'' => 'Bibliography',
     //'' => 'Conference Proceedings',
     //'' => 'Address',
     //'' => 'Extramural', // ???
     //'' => 'Lecture',
     //'' => 'Book',
     //'' => 'Book Chapter',
     //'' => 'Dissertations',
     //'' => 'Thesis',
     //'' => 'Bibliography',
     //'' => 'Congress',
     //'' => 'Extramural', // ???
     //'' => 'Article',
     //'' => 'Editorial',
     //'' => 'Letter to the Editor',
     //'' => 'Book Review',
     //'' => 'Commission Report',
     //'' => 'US (?)',
     //'' => 'International (?)',
     //'' => 'Anything with a GPO #',
     //'' => 'Legislation (?)',
     //'' => 'Government Reports (?)',
     //'' => 'Digital Object',
     //'' => 'Sound',
     //'' => 'Video',
     //'' => 'Image',
     //'' => 'Syllabus',
     //'' => 'Assignment',
     //'' => 'Lecture',
    protected $subtypes_map = array(
     'Introductory Journal Article' => 'Article',
     'Historical Article' => 'Article',
     'Directory' => 'Article',
     'Letter' => 'Letter',
     'Comment' => 'Comment',
     'Editorial' => 'Editorial',
     'Review' => 'Review',
     'News' => 'News',
     'Interview' => 'Interview',
     'Biography' => 'Biography',
     'Case Reports' => 'Case Report',
     'Randomized Controlled Trial' => 'Randomized Controlled Trial',
     'Multicenter Study' => 'Multicenter Study',
     'Validation Studies' => 'Validation Study',
     'Clinical Trial' => 'Clinical Trial',
     'Twin Study' => 'Twin Study',
     'Comparative Study' => 'Comparative Study',
     'Practice Guideline' => 'Practice Guideline',
     'Guideline' => 'Guideline',
     'Evaluation Studies' => 'Evaluation Study',
     'Technical Report' => 'Technical Report',
     'Meta-Analysis' => 'Meta-Analysis',
     'Congresses' => 'Congress',
     'Consensus Development Conference' => 'Conference Proceedings',
     'Addresses' => 'Address',
     'Lectures' => 'Lecture',
    );
    
    function transform_publication_types($types)
    {
        if (is_string($types)) {
            $types = array($types);
        }
    
        $type = 'Journal Article';
        if (in_array('Newspaper Article', $types)) {
            $type = 'News/Magazine';
        } else if (in_array('Legal Cases', $types)) {
            $type = 'Report/Government/Legal';
        }
    
        // Gather some data to determine possible necessity of
        // improving our type resolution:
        /* Temporarily commenting this out, because it's polluting the logs with too much noise.
        if ($type == 'Journal Article' && !in_array('Journal Article', $types)) {
            $serialized_types = print_r( $types, true );
            error_log("type resolved to 'Journal Article', which is not present in original types:\n$serialized_types");
        }
        */
        
        $subtypes = array();
        foreach ($types as $subtype) {
            // Previously used in_array() instead of array_key_exists(), which didn't
            // what I expected: it didn't find all the keys it should have. Buggy.
            if (array_key_exists($subtype, $this->subtypes_map)) {
                $subtypes[] = $this->subtypes_map[$subtype];
            }
        }
    
        // TODO: Set a default subtype of "Article" for "Journal Article"
        // and/or "News/Magazine"?
        return array($type, $subtypes);
    }
    
    function transform_mesh_heading_list($list)
    {
        // TODO: Unsure that this is robust enough...
        // Meant to handle cases in which these values are empty.
        if (!is_array($list)) {
            return array();
        }
    
        $subjects = array();
        foreach ($list as $heading) {
            $subjects[] = $heading['DescriptorName']['_content'];
        }
        return $subjects;
    }
    
    // TODO: Allow multiple languages in EthicShare database, and update
    // this function accordingly.
    function transform_language($language)
    {
        $languages = array();
        if (!is_array($language)) {
            $languages[] = $language;
        } else {
            $languages = $language;
        }
        $lc_languages = array();
        foreach ($languages as $mc_language) {
            $lc_languages[] = strtolower($mc_language);
        }
        return join('; ', $lc_languages);
    }
    
/* TODO: Eliminate these errors/warnings:
WD php: array_key_exists(): The second argument should be either an array or an object in                                                                                           [error]
/opt/www/ethics01.oit.umn.edu/htdocs/prod/sites/all/modules/cite/contrib/CiteETL/T/PubMed.php on line 375.
WD php: Invalid argument supplied for foreach() in /opt/www/ethics01.oit.umn.edu/htdocs/prod/sites/all/modules/cite/contrib/CiteETL/T/PubMed.php on line 385.                       [error]
*/
    function transform_author_list($author_list)
    {
        //echo "author_list = "; var_dump($author_list); 
        /* List normalizing:
           The author list is an array that will contain 
           either a single array, in the case of one author,
           or an array of arrays, in the case of multiple
           authors. We normalize this list so that it's an
           array of arrays no matter what.
        */
    
        // TODO: Unsure that this is robust enough...
        // Meant to handle cases in which these values are empty.
        if (!is_array($author_list)) {
            return array();
        }
    
        // Some versions of the PubMed schema include information about
        // whether or not the author list is complete, which we don't use:
        unset($author_list['CompleteYN']);
    
        $first = array_shift( $author_list );
        $names = array();
    
        // If the first element has a LastName or CollectiveName array key,
        // we know that it's a simple array and not an 
        // array of arrays:
        if (array_key_exists('LastName', $first) || array_key_exists('CollectiveName', $first)) {
            $names[0] = $first;
    
        // If the above key doesn't exist in the first
        // element, we know it's an array of arrays:
        } else {
            $names = $first;
        }
    
        $authors = array();
        foreach ($names as $name) {
            $authors[] = $this->flatten_author_names( $name );
        }
        return $authors;
    }
    
    function flatten_author_names( $names )
    {
        if (array_key_exists('CollectiveName', $names)) {
            return $names['CollectiveName'];
        } else {
            // Different versions of the PubMed schema use different labels for first name:
            $first_name = array_key_exists('ForeName', $names) ? $names['ForeName'] : $names['FirstName'];
            return $names['LastName'] . ', ' . $first_name;
        }
    }
    
    function transform_pagination($pagination)
    {
    
        // Keep the original pagination string, and add separate start and end page values 
        // to the end of the from_values array, if possible:
        if (preg_match('/-/', $pagination)) {
            $pages = preg_split('/-/', $pagination);
            if (preg_match('/^\d+$/', $pages[0]) && preg_match('/^\d+$/', $pages[1])) {
                $start_page = $pages[0];
    
                // Set the end page. If the number after the dash has an equal or greater number
                // of characters than the start page, e.g. 98-116, assume that it can be used as-is.
                if (strlen($pages[1]) >= strlen($start_page)) {
                    $end_page = $pages[1];
    
                // Handle ranges of the form 52-7.
                } else {
                    $end_page = substr_replace(
                        $pages[0],
                        $pages[1],
                        strlen($pages[0]) - strlen($pages[1]),
                        strlen($pages[1])
                    );
                }
            } else {
                // This may not be technically necessary, but it makes the intent clear:
                list($start_page, $end_page) = array(NULL, NULL);
            }
        } elseif (preg_match('/^\d+$/', $pagination)) {
            $start_page = $pagination;
            $end_page = $pagination;
        }
        return array($pagination, $start_page, $end_page);
    }

} // end class CiteETL_T_PubMed
