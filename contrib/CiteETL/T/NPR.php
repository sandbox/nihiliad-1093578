<?php

//require PEAR::* ???

require_once 'CiteETL/T/Transformer.php';
require_once 'CiteETL/T/Search/ScienceHealthNews.php';

class CiteETL_T_NPR extends CiteETL_T_Transformer
{
    // No ISSN for a website. What to use instead???
    // Look at: http://firstsearch.oclc.org/WebZ/FSFETCH?fetchtype=fullrecord:sessionid=fsapp9-58055-fo0la3e2-wgyspe:entitypagenum=25:0:recno=2:resultset=9:format=FI:next=html/record.html:bad=error/badfetch.html:entitytoprecno=2:entitycurrecno=2:numrecs=1
    //protected $issn = '';
    protected $journal_name = 'NPR';
    protected $filter_pattern;
    
    function __construct() 
    {
        $search = new CiteETL_T_Search_ScienceHealthNews();
        $this->filter_pattern = $search->pattern();
    }
    
    function transform( $record ) {
    
        // output
        $citation = array();
        $citation['values'] = array();
        $citation_values = &$citation['values'];
    
        $simplepie_item = $record->as_simplepie_item();
    
        // May need to do something with this...
        //'callbacks' => array('required','verify_scalar','limit_title_length:128'),
        $citation_values['title'] = $record->title();
    
        $citation_values['field_abstract'][0]['value'] =
            $record->description();
    
        $citation_values['field_journal_name'][0]['value'] = $this->journal_name;
    
        /* BBC doesn't seem to include an author/creator...
        $dc_creator = $simplepie_item->get_item_tags(
            'http://dublincore.org/documents/dcmi-namespace/',
            'creator'
        );
        $author = $dc_creator[0]['data'];
        $citation_values['field_author'][0]['value'] = $author;
        */
    
        //$citation_values['field_issn'][0]['value'] = $this->issn;
    
    /*
     'volume' => array(
      'to' => 'field_volume/0/value',
      'from' => 'MedlineCitation/Article/Journal/JournalIssue/Volume',
      'callbacks' => array('verify_scalar'),
     ),
    
     'issue' => array(
      'to' => 'field_issue/0/value',
      'from' => 'MedlineCitation/Article/Journal/JournalIssue/Issue',
      'callbacks' => array('verify_scalar'),
     ),
    */
    
        $year = $simplepie_item->get_date('Y');
    
        $citation_values['field_year_published'][0]['value'] = $year;
        $citation_values['field_datetime_published'][0]['value'] =
            $simplepie_item->get_date('Y-m-d H:i:s');
    
        $citation_values['timestamp'] = $simplepie_item->get_date('U');
    /* Attempting to import this field seems to cause errors...
        $citation_values['field_full_date_published'][0]['value']['mday'] =
            $simplepie_item->get_date('d'); 
        $citation_values['field_full_date_published'][0]['value']['mon'] =
            $simplepie_item->get_date('m'); 
        $citation_values['field_full_date_published'][0]['value']['year'] = $year; 
    */
    
        $citation_values['field_type'][0]['value'] = $this->type;
    
        $citation_values['field_format'][0]['value'] = $this->format;
    
        $citation_values['field_language'][0]['value'] = $this->language;
    
        // TODO: How do I prevent this from getting set to "true"? Just not even try to set it?
        //$citation_values['field_scholarly']['value'] = 0;
    
        //$url = $simplepie_item->get_id();
        $url = $record->primary_id();
        $citation_values['field_url_0'][0]['value'] = $url;
    
        $citation_values['field_source'][0]['value'] = 'NPR';

        // Collect the sources and source IDs:
        $sources = array();
        $sources[] = array(
            'source' => $this->journal_name,
            'source_id' => $url,
        );
        $citation['sources'] = $sources;
    
        $this->filter( $citation );
    
        return $citation;
    }

} // end class CiteETL_T_NPR 
