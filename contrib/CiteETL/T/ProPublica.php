<?php

//require PEAR::* ???

require_once 'CiteETL/T/Transformer.php';
require_once 'CiteETL/T/Search/ScienceHealthNews.php';

class CiteETL_T_ProPublica extends CiteETL_T_Transformer
{
    // No ISSN for a website. What to use instead???
    // Look at: http://firstsearch.oclc.org/WebZ/FSFETCH?fetchtype=fullrecord:sessionid=fsapp9-58055-fo0la3e2-wgyspe:entitypagenum=25:0:recno=2:resultset=9:format=FI:next=html/record.html:bad=error/badfetch.html:entitytoprecno=2:entitycurrecno=2:numrecs=1
    //protected $issn = '';
    protected $journal_name = 'ProPublica';
    protected $filter_pattern;
    
    function __construct() 
    {
        $search = new CiteETL_T_Search_ScienceHealthNews();
        $this->filter_pattern = $search->pattern();
    }
    
    function transform( $record ) {
    
        // output
        $citation = array();
        $citation['values'] = array();
        $citation_values = &$citation['values'];
    
        $simplepie_item = $record->as_simplepie_item();
    
        // May need to do something with this...
        //'callbacks' => array('required','verify_scalar','limit_title_length:128'),
        $citation_values['title'] = $record->title();
    
        // authors
        $authors = $this->get_authors( $record );
        foreach ($authors as $index => $author) {
            $citation_values['field_author'][$index]['value'] = $author;
        }

        $citation_values['field_abstract'][0]['value'] = $this->generate_abstract( $record, $authors );
    
        foreach ($record->categories() as $categories_string) {
            // ProPublica puts all categories in a single field and separates them with commas.
            $categories = preg_split('/,/', $categories_string);
            foreach ($categories as $index => $category) {
                $citation_values['field_subject'][$index]['value'] = $category;
            }
        }
    
        $citation_values['field_journal_name'][0]['value'] = $this->journal_name;
    
        $year = $simplepie_item->get_date('Y');
    
        $citation_values['field_year_published'][0]['value'] = $year;
        $citation_values['field_datetime_published'][0]['value'] =
            $simplepie_item->get_date('Y-m-d H:i:s');
    
        $citation_values['timestamp'] = $simplepie_item->get_date('U');
    
        $citation_values['field_type'][0]['value'] = $this->type;
    
        $citation_values['field_format'][0]['value'] = $this->format;
    
        $citation_values['field_language'][0]['value'] = $this->language;
    
        // TODO: How do I prevent this from getting set to "true"? Just not even try to set it?
        //$citation_values['field_scholarly']['value'] = 0;
    
        $url = $record->primary_id();
        $citation_values['field_url_0'][0]['value'] = $url;
    
        $citation_values['field_source'][0]['value'] = 'ProPublica';

        // Collect the sources and source IDs:
        $sources = array();
        $sources[] = array(
            'source' => $this->journal_name,
            'source_id' => $url,
        );
        $citation['sources'] = $sources;
    
        $this->filter( $citation );
    
        return $citation;
    }

    public function get_authors( $record )
    {
        $authors = array( $record->author() );

        // ProPublica puts all the authors in links inside the description, usually in
        // the first paragraph. There is always only one author in the 'author' field, 
        // but if there are multiple authors, we can sometimes get the rest from these links.
        // TODO: Doesn't RSS/Atom allow multiple authors? Why does ProPublica do this?
        $record_dom = new DOMDocument();
        $record_dom->loadXML( $record->as_string() );
        foreach ($record_dom->getElementsByTagName('description') as $description_elem) {

            // ProPublica descriptions are HTML with all the angle brackets and such xml-escaped.
            $description_decoded = html_entity_decode(trim($description_elem->nodeValue), ENT_QUOTES, 'UTF-8');

            // We always seem to get 2 description elements, and the first is empty. Hmmm...
            if (!preg_match('/\w+/', $description_decoded)) continue;
            //echo "description_decoded = ", var_export($description_decoded), "\n";

            // Experience shows that sometimes this will fail:
            try {
                $description_dom = new DOMDocument();
                $description_dom->loadXML('<xml>' . $description_decoded . '</xml>');
            } catch (Exception $e) {
                // TODO: Put a warning with the exception message in the error log?
                break;
            }

            // TODO: How about using only the first paragraph of the description?
            $seen_only_authors = true; // Author links always come first in the description.
            foreach ($description_dom->getElementsByTagName('a') as $link) {
                $url = $link->getAttribute('href');
                $text = trim($link->nodeValue);
                //echo "url = $url, text = $text\n";
                if (preg_match('!propublica\.org\/site\/author!', $url)) {
                    //echo "propublica author link\n";
                    if (in_array($text, $authors)) continue;
                    $authors[] = $text;

                // ProPublica often seems to list authors from other organizations directly after their own authors.
                // So far, have seen mostly authors from NPR. Not sure how to identify all the others.
                // TODO: They're often separated by "and". Maybe we can use that?
                } else if (preg_match('!npr\.org\/templates\/story!', $url) && $seen_only_authors) {
                    //echo "npr author link\n";
                    $seen_only_authors = false;
                    if (in_array($text, $authors)) continue;
                    $authors[] = $text;
                } else {
                    //echo "probably not an author link\n";
                    $seen_only_authors = false;
                }
            } 
        }
        return $authors;
    }

    public function generate_abstract( $record, $authors )
    {
        // Note: Easy generation of an abstract depends on stripping of many HTML tags
        // from the description via SimplePie in XML_Record_FeedItem_ProPublica.
        $description = $record->description();

        // Experience shows this may fail:
        try {
            $description_sxe = new SimpleXMLElement('<xml>' . $description . '</xml>');
            //echo "sxe = ", var_export( $sxe ), "\n";
        } catch (Exception $e) {
            // TODO: Put a warning with the exception message in the error log?
            return '';
        }

        $paragraphs = array();
        foreach ($description_sxe->p as $p) {
            $paragraph = trim((string) $p);
            //echo "paragraph = ", var_export( $paragraph ), "\n";
            
            // ProPublica always puts the authoris in the first paragraph of the
            // description, if they include an author.
            $paragraph_contains_authors = false;
            foreach ($authors as $author) {
                if (preg_match('/' . $author . '/', $paragraph)) {
                    $paragraph_contains_authors = true;
                }
            }
            if ($paragraph_contains_authors) continue;
            $paragraphs[] = $paragraph;

            // Include at most 3 paragraphs in the abstract (they tend to be short).
            if (count($paragraphs) == 3) break;
        }
        return join(' ', $paragraphs);
    }

} // end class CiteETL_T_ProPublica 

