<?php

//require PEAR::* ???

class CiteETL_T_FeministBioethics {

private $issn = '1937-4585';
private $journal_name = 'International Journal of Feminist Approaches to Bioethics';

// TODO: Use this in all other transformers!!!
private $source_name = 'Feminist Bioethics';

private $publisher = 'Indiana University Press';
private $type = 'Journal Article';

// Will these always be true????
private $format = 'Print';
private $language = 'eng';

//private $filter_pattern;

function __construct() {
}

function transform( $simplepie_item ) {
    unset($citation);
    $citation = array();

    // May need to do something with this...
    //'callbacks' => array('required','verify_scalar','limit_title_length:128'),
    $citation['title'] = trim( $simplepie_item->get_title() );

    unset($header, $abstract);
    list($header, $abstract) = split('Abstract', $simplepie_item->get_content()); 
    unset($journal_title, $volume, $issue, $pages, $season);
    list($journal_title, $volume, $issue, $pages, $season) = 
        split(",", $header); 
 
    $citation['field_volume'][0]['value'] =
        trim( preg_replace('/Volume/', '', $volume) ); 
 
    $citation['field_issue'][0]['value'] =
        trim( preg_replace('/Issue/', '', $issue) ); 
 
    unset($pages);
    $pages = trim( preg_replace('/Page/', '', $pages) ); 
    unset($start_page, $end_page);
    list($start_page, $end_page) = split('-', $pages); 
    $citation['field_start_page'][0]['value'] = $start_page; 
    $citation['field_end_page'][0]['value'] = $end_page; 
 
    $citation['field_season'][0]['value'] =
        trim( preg_replace('/\d+\.\s*$/', '', $season) ); 
 
    $citation['field_abstract'][0]['value'] = trim( $abstract ); 

    $citation['field_journal_name'][0]['value'] = $this->journal_name;
    $citation['field_publisher'][0]['value'] = $this->publisher;

    unset($author_email);
    $author_email = $simplepie_item->get_author()->email;
    unset($matches);
    preg_match('/\(([^\)]+)\)/', $author_email, $matches);
    $citation['field_author'][0]['value'] = $matches[1];

    $citation['field_issn'][0]['value'] = $this->issn;

    unset($year);
    $year = $simplepie_item->get_date('Y');
    $citation['field_year_published'][0]['value'] = $year;
    $citation['timestamp'] = $simplepie_item->get_date('U');
/* Attempting to import this field seems to cause errors...
    $citation['field_full_date_published'][0]['value']['mday'] =
        $simplepie_item->get_date('d'); 
    $citation['field_full_date_published'][0]['value']['mon'] =
        $simplepie_item->get_date('m'); 
    $citation['field_full_date_published'][0]['value']['year'] = $year; 
*/

    $citation['field_type'][0]['value'] = $this->type;

    $citation['field_format'][0]['value'] = $this->format;

    $citation['field_language'][0]['value'] = $this->language;

    $citation['field_popular_media'][0]['value'] = 'Scholarly';

    $citation['source'] = $this->source_name;
    $citation['source_id'] = $simplepie_item->get_id();
    $citation['field_url_0'][0]['value'] = $citation['source_id'];

    $this->filter( $citation );

    return $citation;
}

function filter( $citation ) {
    unset($title);
    $title = $citation['title'];
    unset($abstract);
    $abstract = $citation['field_abstract'][0]['value'];

    if (preg_match('/^(Introduction|Acknowledg(e)?ments)$/', $citation['title'])) {
        throw new Exception(
            "The article '" . $citation['title'] . "', id: " .
            $citation['source_id'] . " was rejected by the relevancy filter"
        );
    }
    if (!isset($abstract) || !preg_match('/\w+/', $abstract)) {
        throw new Exception(
            "The article '" . $citation['title'] . "', id: " .
            $citation['source_id'] . " was rejected by the relevancy filter because it has no abstract"
        );

    }
}

} // end class CiteETL_T_FeministBioethics
