<?php

require_once 'ISBN/Factory.php';

class CiteETL_T_WorldCatNewSchema
{
    // TODO: This won't always be true. Come up with better handling for types.
    protected $type = 'Book';
    
    // Will these always be true????
    protected $format = 'Print';
    
    function __construct()
    {
    }
    
    function transform( $marc_xml ) {

        $citation = array();
        $citation['values'] = array();
        $citation_values = &$citation['values'];

        // Collect the sources and source IDs:
        $sources = array();
        $oclc_id = $marc_xml->primary_id();
        $citation_values['field_oclc_id'][0]['value'] = $oclc_id;
        $sources[] = array(
            'source' => 'OCLC',
            'source_id' => $oclc_id,
        );
    
        $citation_values['field_source'][0]['value'] = 'OCLC';
        $citation_values['field_source_uri'][0]['url'] = 'http://worldcat.org/oclc/' . $oclc_id;
        $citation_values['field_source_uri'][0]['title'] = 'OCLC';

        $urls = $this->get_urls($marc_xml);
        if (NULL != $urls['full_text']) {
            $citation_values['field_full_text_url'][0]['url'] = $urls['full_text']['url'];    
            $citation_values['field_full_text_url'][0]['title'] = $urls['full_text']['title'];    
        }
        $i = 0;
        foreach ($urls['others'] as $other_url) {
            $citation_values['field_other_urls'][$i]['url'] = $other_url['url'];    
            $citation_values['field_other_urls'][$i]['title'] = $other_url['title'];    
            $i++;
        }

        $ids = $marc_xml->ids();

        // IDs continued: SuDoc Numbers
        $sudoc_numbers = $this->get_sudoc_numbers( $ids );
        // TODO: After the rollover to Drupal 6, allow the main citation record to have multiple SuDoc Numbers.
        if (count($sudoc_numbers) > 0) {
            //echo "sudoc_numbers = "; print_r( $sudoc_numbers );
            $citation_values['field_sudoc_number'][0]['value'] = $sudoc_numbers[0];
            $citation_values['field_type'][0]['value'] = 'Report/Government/Legal';
        } else {
            $citation_values['field_type'][0]['value'] = $this->type;
        }
        foreach ($sudoc_numbers as $sudoc_number) {
            $sources[] = array(
                'source' => 'SuDoc',
                'source_id' => $sudoc_number,
            );
        }

        $citation_values['field_types'][0]['value'] = $this->get_type( $marc_xml );

        $citation_values['field_doi'][0]['value'] = $this->get_doi( $ids );
        

        // IDs continued: ISBNs
        try {
            $isbns = $this->get_isbns( $ids );
        } catch (Exception $e) {
            throw new Exception("OCLC record $oclc_id: " . $e->getMessage());
        }
        // TODO: After the rollover to Drupal 6, allow the main citation record to have multiple ISBNs.
        if (count($isbns) > 0) {
            $citation_values['field_isbn'][0]['value'] = $isbns[0];
        }
        foreach ($isbns as $isbn) {
            $sources[] = array(
                'source' => 'ISBN',
                'source_id' => $isbn,
            );
        }


        // IDs continued (sort of): LCCN
        $citation_values['field_lccn'][0]['value'] = $this->get_lccn( $marc_xml );

        // IDs continued (sort of): LCCN
        $citation_values['field_isrc'][0]['value'] = $this->get_isrc( $marc_xml );

        // TODO: Add URLs as IDs? Will be tricky, since, in WorldCat data, URLs are not always unique IDs.

        // IDs continued: Assign sources and source IDs for de-duping:
        $citation['sources'] = $sources;

        try {
            $title = $this->get_title( $marc_xml );
        } catch (Exception $e) {
            throw new Exception("OCLC record $oclc_id: " . $e->getMessage());
        }
        $citation_values['field_full_title'][0]['value'] = $title;
        if (strlen($title) > 128) {
            $citation_values['field_alternative_title'][0]['value'] = $title;
            $title = substr($title, 0, 125) . '...';
        }
        $citation_values['title'] = $title;
    
        $translated_titles = $this->get_translated_titles( $marc_xml );
        $alternative_titles = array();
        $english_translated_title = NULL;
        foreach ($translated_titles as $translated_title) {
            if (NULL == $english_translated_title && 'eng' == $translated_title['language']) {
                $english_translated_title = $translated_title['title'];
                continue;
            } 
            $alternative_titles[] = $translated_title['title'];
        }
        if ($english_translated_title) {
            $citation_values['field_translated_title'][0]['value'] = $english_translated_title;
        } else if (0 < count($alternative_titles)) {
            $citation_values['field_translated_title'][0]['value'] = array_shift($alternative_titles);
        }

        //$alternative_titles = array_merge($alterntive_titles, $this->get_alternative_titles($marc_xml));
        $more_alternative_titles = $this->get_alternative_titles($marc_xml);
        $alternative_titles = array_merge($alternative_titles, $more_alternative_titles);
        $i = 0;
        foreach ($alternative_titles as $alternative_title) {
            $citation_values['field_alternative_titles'][$i]['value'] = $alternative_title;    
            $i++;
        }

        $citation_values['field_edition'][0]['value'] = $this->get_edition( $marc_xml );

        $authors = $this->get_authors( $marc_xml );
        foreach ($authors as $index => $author) {
            $citation_values['field_author'][$index]['value'] = $author;
        }
    
        $contributors = $this->get_contributors( $marc_xml );
        $i = 0;
        foreach ($contributors as $contributor) {
            $citation_values['field_contributors'][$i]['value'] = $contributor['value'];    
            $citation_values['field_contributors'][$i]['attribute'] = $contributor['attribute'];    
            $i++;
        }

        $citation_values['field_affiliation'][0]['value'] = $this->get_affiliations( $marc_xml );

        $subjects = $this->get_subjects( $marc_xml );
        if (is_array($subjects) && count($subjects) > 0) {
            foreach ($subjects as $index => $subject) {
                $citation_values['field_subject'][$index]['value'] = $subject;
            }
        }

        $citation_values['field_abstract'][0]['value'] = $this->get_summary( $marc_xml );
        $citation_values['field_contents'][0]['value'] = $this->get_contents( $marc_xml );
        $citation_values['field_audience_education_level'][0]['value'] = $this->get_target_audience_notes( $marc_xml );

        // TODO: Do I really need to initialize this array? Probably. Probably not good enough, even.
        $pub_data = array();
        try { 
            $pub_data = $this->get_publication_data( $marc_xml );
        } catch (Exception $e) {
            throw new Exception("OCLC record $oclc_id: " . $e->getMessage());
        }
        $citation_values['field_publisher'][0]['value'] = $pub_data['publisher'];
        $citation_values['field_publisher_location'][0]['value'] = $pub_data['location'];

        try {
            $dates = $this->get_publication_dates( $marc_xml );
        } catch (Exception $e) {
            throw new Exception("OCLC record $oclc_id: " . $e->getMessage());
        }
        $citation_values['field_datetime_published'][0]['value'] = $dates['datetime_published'];
        $citation_values['field_year_published'][0]['value'] = $dates['year_published'];
        $citation_values['field_end_datetime_published'][0]['value'] = $dates['end_datetime_published'];
        $citation_values['field_datetime_copyrighted'][0]['value'] = $dates['datetime_copyrighted'];
    
        $citation_values['field_size'][0]['value'] = $this->get_size( $marc_xml );
    
        $citation_values['field_format'][0]['value'] = $this->format;
    
        // Old field:
        $citation_values['field_language'][0]['value'] = $this->get_language( $marc_xml );
        // New fields:
        $languages = $this->get_languages( $marc_xml );
        $citation_values['field_primary_language'][0]['value'] = $languages['primary'];
        $i = 0;
        foreach ($languages['others'] as $language) {
            $citation_values['field_languages'][$i]['value'] = $language['value'];    
            $citation_values['field_languages'][$i]['attribute'] = $language['attribute'];    
            $i++;
        }
        

        // TODO: How do I prevent this from getting set to "true"? Just not even try to set it?
        //$citation_values['field_scholarly'][0]['value'] = 'Non-scholarly';

        $citation_values['field_access_rights'][0]['value'] = $this->get_access_rights( $marc_xml );
        $citation_values['field_note'][0]['value'] = $this->get_note( $marc_xml );
        $citation_values['field_funding_info'][0]['value'] = $this->get_funding_info( $marc_xml );
        $citation_values['field_physical_description'][0]['value'] = $this->get_physical_description( $marc_xml );
        $citation_values['field_physical_location'][0]['value'] = $this->get_physical_location( $marc_xml );
        $citation_values['field_provenance'][0]['value'] = $this->get_provenance( $marc_xml );

        $citation_values['field_part_of'][0]['value'] = $this->get_part_of( $marc_xml );

        $event_info = $this->get_event_info( $marc_xml );
        $citation_values['field_event'][0]['value'] = $event_info['event'];
        $citation_values['field_cite_event_location'][0]['value'] = $event_info['location'];

        return $citation;
    }
    
    public function get_authors( $marc_xml )
    {
        $authors = array(); 
        foreach ($marc_xml->datafield('100','110') as $datafield) {
            foreach ($datafield->subfield('a') as $author) {
                $author = trim($author);
                $author = preg_replace('/[\.,]+$/', '', $author);
                if (!in_array($author, $authors)) {
                    $authors[] = $author;
                }
            }
        }
        if (count($authors) > 0) return $authors;

        // If we don't have an author yet, try to get one from the title tag:
        foreach ($marc_xml->datafield('245') as $datafield) {
            foreach($datafield->subfield('c') as $author) {
                $author = trim($author);
                $author = preg_replace('/[\.,\/]+$/', '', $author);
                if (!in_array($author, $authors)) {
                    $authors[] = $author;
                }
            }
        }
        return $authors;

        /* Some OCLC/WorldCat records have no authors.
        throw new Exception("No author information found.");
        */
    }

    public function get_contributors( $marc_xml )
    {
        // Add fields 700, 710?
        $contributors = array(); 
        foreach ($marc_xml->datafield('508','511') as $datafield) {
            foreach($datafield->subfield('a') as $value) { // Should be only one of these.
                $values = explode(';', $value);
                foreach ($values as $contributor) {
                    // Was hoping there would be a predictable way to extract attributes (roles)
                    // from these contributor values, but there doesn't seem to be.
                    $contributors[] = array('value' => trim($contributor), 'attribute' => NULL);
                }
            }
        }
        return $contributors;
    }

    public function get_affiliations( $marc_xml )
    {
        $affiliations = array();

        // Only one of these should be defined, but it won't to check both:
        foreach (array('100','110') as $tag) {
            foreach ($marc_xml->datafield($tag) as $datafield) {
                foreach ($datafield->subfield('u') as $affiliation) {
                    $affiliation = trim($affiliation);
                    if (!in_array($affiliation, $affiliations)) {
                        $affiliations[] = $affiliation;
                    }
                }
            }
        }
        return join('; ', $affiliations);
    }

    public function get_title( $marc_xml )
    {
        $titles = array(); // To check for multiple titles.
        foreach ($marc_xml->datafield('245') as $datafield) { // Should be only one of these.
            unset($title);
            foreach($datafield->subfield('a') as $title) { // Should be only one of these.
                $title = trim($title);
                $title = preg_replace('/[\.,\/]+$/', '', $title);
            }

            unset($title_remainder);
            foreach($datafield->subfield('b') as $title_remainder) { // Should be only one of these, & only if we have a subfield 'a'.
                $title_remainder = trim($title_remainder);
                $title_remainder = preg_replace('/[\.,\/]+$/', '', $title_remainder);
            }

            if (isset($title)) {
                if (isset($title_remainder)) {
                    $title .= " $title_remainder";
                }
                $title = preg_replace('/\s+([:;,])\s+/', '$1 ', $title);
                $title = preg_replace('/  /', ' ', $title);
                $titles[] = $title;
            } else {
                // If we didn't find a title yet, just concatenate together whatever 245 subfields exist:
                $title_subfields = array();
                foreach($datafield->subfields() as $subfield) { // May be many of these.
                    $value = array_shift(array_values($subfield)); // Should be only one of these.
                    $title_subfields[] = trim($value);
                }
                $titles[] = join(' ', $title_subfields);
            }
        }
        if (count($titles) == 0) {
            throw new Exception("No title information found.");
        }
        if (count($titles) > 1) {
            error_log("Warning: more than 1 title found: OCLC ID " . $marc_xml->primary_id());
        }
        return $titles[0];
    }

    public function get_translated_titles( $marc_xml )
    {
        $titles = array();
        foreach ($marc_xml->datafield('242') as $datafield) { // Should be only one of these.
            unset($title);
            foreach($datafield->subfield('a') as $title) { // Should be only one of these.
                $title = trim($title);
                $title = preg_replace('/[\.,\/]+$/', '', $title);
            }

            unset($title_remainder);
            foreach($datafield->subfield('b') as $title_remainder) { // Should be only one of these, & only if we have a subfield 'a'.
                $title_remainder = trim($title_remainder);
                $title_remainder = preg_replace('/[\.,\/]+$/', '', $title_remainder);
            }

            unset($language);
            foreach($datafield->subfield('y') as $language) { // Should be only one of these, & only if we have a subfield 'a'.
                $language = trim($language);
            }

            if (isset($title)) {
                if (isset($title_remainder)) {
                    $title .= " $title_remainder";
                }
                $title = preg_replace('/\s+([:;,])\s+/', '$1 ', $title);
                $title = preg_replace('/  /', ' ', $title);
                $titles[] = array('title' => $title, 'language' => $language);
            } 
        }
        return $titles;
    }

    public function get_alternative_titles( $marc_xml )
    {
        // TODO: Add 740 to this?
        // TODO: Include subfield n: "Number of part/section of a work" 
        $titles = array();
        foreach (array('130','246','247','730') as $tag) {
            foreach ($marc_xml->datafield($tag) as $datafield) { // Should be only one of these.
                unset($title);
                foreach($datafield->subfield('a') as $title) { // Should be only one of these.
                    $title = trim($title);
                    $title = preg_replace('/[\.,\/]+$/', '', $title);
                }
    
                unset($title_remainder);
                // Note: subfield $b is undefined for datafields 130 and 730.
                foreach($datafield->subfield('b') as $title_remainder) { // Should be only one of these, & only if we have a subfield 'a'.
                    $title_remainder = trim($title_remainder);
                    $title_remainder = preg_replace('/[\.,\/]+$/', '', $title_remainder);
                }
    
                if (isset($title)) {
                    if (isset($title_remainder)) {
                        $title .= " $title_remainder";
                    }
                    $title = preg_replace('/\s+([:;,])\s+/', '$1 ', $title);
                    $title = preg_replace('/  /', ' ', $title);
                    if (!in_array($title, $titles)) {
                        $titles[] = $title;
                    }
                } 
            }
        }
        return $titles;
    }

    public function get_edition( $marc_xml )
    {
        $editions = array(); // There *may* be multiple editions, but there should be only one.
        foreach ($marc_xml->datafield('250') as $datafield) {
            unset($edition);
            foreach($datafield->subfield('a') as $edition) { // Should be only one of these.
                $edition = trim($edition);
                //$edition = preg_replace('/[\.,\/]+$/', '', $edition);
            }

            unset($edition_remainder);
            foreach($datafield->subfield('b') as $edition_remainder) { // Should be only one of these, & only if we have a subfield 'a'.
                $edition_remainder = trim($edition_remainder);
                //$edition_remainder = preg_replace('/[\.,\/]+$/', '', $edition_remainder);
            }

            if (isset($edition)) {
                if (isset($edition_remainder)) {
                    $edition .= " $edition_remainder";
                }
                $edition = preg_replace('/\s+([:;,])\s+/', '$1 ', $edition);
                $edition = preg_replace('/  /', ' ', $edition);
                $editions[] = $edition;
            } 
        }
        // Even if there is more than one edition statement, just use the first one:
        return $editions[0];
    }

    public function get_publication_data( $marc_xml )
    {
        $pub_fields = $marc_xml->datafield('260');
        if (count($pub_fields) > 1) {
            error_log("Warning: more than 1 datafield 260 found: OCLC ID " . $marc_xml->primary_id());
        }
        if (count($pub_fields) == 0) {
            // TODO: Lots of WorldCat records rejected for this reason. Find a fix!!!
            //throw new Exception("No publisher information found.");
            return array();
        }
        $pub_field = $pub_fields[0];

        $locations = array();
        foreach ($pub_field->subfield('a') as $location) {
            $location = preg_replace('/\s*[,:;]*\s*$/', '', $location);
            $locations[] = $location;
        }
        $pub['location'] = join('; ', $locations);

        $publishers = array();
        foreach ($pub_field->subfield('b') as $publisher) {
            $publisher = preg_replace('/\s*[,:;]*\s*$/', '', $publisher);
            $publishers[] = $publisher;
        }
        if (count($publishers) > 0) {
            $pub['publisher'] = join('; ', $publishers);
        } else {
            // TODO: Lots of WorldCat records rejected for this reason. Find a fix!!!
            //throw new Exception("No publisher information found.");
            return array();
        }

        return $pub;
    }

    public function get_publication_dates( $marc_xml )
    {
        // output
        $dates = array(
            'year_published' => null,
            'datetime_published' => null,
            'end_datetime_published' => null,
            'datetime_copyrighted' => null,
        );

        // TODO: Cover more of the cases in the spec: http://www.loc.gov/marc/bibliographic/bd008a.html
        $control_field = $marc_xml->controlfield('008');
        //echo "control_field = $control_field\n";

        // Initialize $year to the MARC undefined value:
        $year = 'uuuu';

        // Most dates won't have a month and day, except for type 'e', so we
        // set them to MySQL's undefined value:
        $month = '00';
        $day = '00';

        // Date types:
        $single_date_year_only = array(
            'c', // Continuing resource currently published
            's', // Single known date/probable date
            'u', // Continuing resource status unknown
        );

        $date_ranges = array(
            'd', // Continuing resource ceased publication
            'i', // Inclusive dates of collection
            'k', // Range of years of bulk of collection
            'm', // Multiple dates
            'q', // Questionable date
        );


        $reissue_and_original_dates = array(
            'n', // Dates unknown // TODO: Should we just ignore this? Included here since the handling will be similar.
            'p', // Date of distribution/release/issue and production/recording session when different
            'r', // Reprint/reissue date and original date
        );

        $publication_and_copyright = 't'; // Publication date and copyright date
        $detailed_date = 'e'; // Detailed date

        $missing_dates = array(
            'b', // No dates given; B.C. date involved
            '|', // No attempt to code
        );

        $date_type = substr($control_field, 6, 1);

        if (in_array($date_type, $single_date_year_only)) {
            $year = substr($control_field, 7, 4);
        } else if (in_array($date_type, $date_ranges)) {
            // For these, the first date will be the earlier date.
            $start_year = substr($control_field, 7, 4);
            $end_year = substr($control_field, 11, 4);

            if (preg_match('/^[12]\d{3}$/', $start_year) && preg_match('/^[12]\d{3}$/', $end_year)) {
                $year = $start_year; // Main date.
                $dates['end_datetime_published'] = $end_year . '-00-00 00:00:00';
            } else if (preg_match('/^[12]\d{3}$/', $end_year)) {
                // If we don't have two complete years, use the later one, because presumably
                // the cited work contains material produced around that time:
                $year = $end_year; // Main date.
            } else {
                // If we get this far, we may not have a usable date at all.
                $year = $start_year; // Main date.
            }
        } else if (in_array($date_type, $reissue_and_original_dates)) {
            // For these, the first date will be the earlier date.
            $reissue_year = substr($control_field, 7, 4);
            $original_year = substr($control_field, 11, 4);
            // Prefer the original year:
            $year = preg_match('/^[12]\d{3}$/', $original_year) ? $original_year : $reissue_year;
        } else if ($date_type == $publication_and_copyright) {
            $publication_year = substr($control_field, 7, 4);
            $copyright_year = substr($control_field, 11, 4);
            if (preg_match('/^[12]\d{3}$/', $publication_year) && preg_match('/^[12]\d{3}$/', $copyright_year)) {
                $year = $publication_year; // Main date.
                $dates['datetime_copyrighted'] = $copyright_year . '-00-00 00:00:00';
            } else if (preg_match('/^[12]\d{3}$/', $end_year)) {
                // If we don't have two complete years, use the publication year:
                $year = $publication_year; // Main date.
            } else {
                // If we get this far, we may not have a usable date at all.
                $year = $copyright_year; // Main date.
            }
        } else if ($date_type == $detailed_date) { // Detailed date: Also contains a month, maybe also a day.
            $year = substr($control_field, 7, 4);
            $proto_month = substr($control_field, 11, 2);
            $proto_day = substr($control_field, 13, 2);

            // Some records violate the spec, putting a year in slots 11-14 instead
            // of a month and day. Try to handle that as best we can:
            if (preg_match('/^\d{2}$/', $proto_month) && $proto_month <= 12) {
                $month = $proto_month;
            }
            if (preg_match('/^\d{2}$/', $proto_day) && $proto_day <= 31) {
                $day = $proto_day;
            }
        }

        // Handle missing or B.C. dates, and bad years generated above:
        if (in_array($date_type, array('b','|')) || !preg_match('/^[12]\d{3}$/', $year)) {
            throw new Exception(
                "Unusable publication date in control field 008: '$control_field'"
            );
        }
        $dates['datetime_published'] = "$year-$month-$day 00:00:00";
        $dates['year_published'] = $year;
        return $dates;
    }

    public function get_language( $marc_xml )
    {
        $control_field = $marc_xml->controlfield('008');
        $language = strtolower(substr($control_field, 35, 3));
        return $language;
    }

    public function get_languages( $marc_xml )
    {
        // Get the primary language:

        $control_field = $marc_xml->controlfield('008');
        $primary = strtolower(substr($control_field, 35, 3));

        // See: http://www.loc.gov/marc/bibliographic/bd008a.html
        $non_language_code_values = array(
            'zxx', // No linguistic content
            'mul', // Multiple languages
            'sgn', // Sign languages
            'und', // Undetermined
        );

        if (in_array($primary, $non_language_code_values)) {
            $primary = NULL;
        }

        // Get any other languages:

        $subfield_code_texts = array(
            'a' => 'Text/sound track or separate title',
            'b' => 'Summary/abstract',
            'd' => 'Sung or spoken text',
            'e' => 'Libretto',
            'f' => 'Table of contents',
            'g' => 'Accompanying material',
            'h' => 'Original and/or intermediate translation',
            'j' => 'Subtitles/captions',
        ); 

        $subfield_codes_to_skip = array(
            '2', // Source of code (NR)
            '6', // Linkage (NR)
            '8', // Field link and sequence number (R)
        );

        $others = array();
        foreach ($marc_xml->datafield('041') as $datafield) { // Should be only one of these.
            foreach($datafield->subfields() as $subfield) { // May be many of these.
                $code = array_shift(array_keys($subfield)); // Will be only one of these: See XML_Record_WorldCat_MARC_Datafield API.
                if (in_array($code, $subfield_codes_to_skip)) continue;
                $language_code = array_shift(array_values($subfield)); // Will be only one of these: See XML_Record_WorldCat_MARC_Datafield API.
                $others[] = array(
                    'value' => trim($language_code), // TODO: Validate the language code?
                    'attribute' => $subfield_code_texts[$code],
                );
                if (NULL == $primary && in_array($code, array('a','d'))) {
                    $primary = $language_code;
                }
            }
        }
        
        // If we still don't have a primary language code, just use the first of any other languages provided:
        if (NULL == $primary && 0 < count($others)) {
            $primary = $others[0]['value'];
        }

        return array('primary' => $primary, 'others' => $others);
    }

    public function get_size( $marc_xml )
    {
        $sizes = array(); // To check for multiple sizes.
        foreach ($marc_xml->datafield('300') as $datafield) { // Should be only one of these.
            foreach($datafield->subfield('a') as $size) { // Should be only one of these.
                $size = trim($size);
                $size = preg_replace('/[\s*:;,]+$/', '', $size);
                $sizes[] = $size;
            }
        }
        return implode('; ', $sizes);
    }

    public function get_subjects( $marc_xml )
    {
        $desired_tags = array(
            '600', // Personal Name
            '610', // Corporate Name
            '630', // Uniform Title
            '648', // Chronological Term
            '650', // Topical Term
            '651', // Geographical Name
            '656', // Occupation
        );

        $subjects = array(); // To check for duplicates.
        foreach ($desired_tags as $tag) {
            foreach ($marc_xml->datafield($tag) as $datafield) {
                $subject_parts = array();
                foreach($datafield->subfields() as $subfield) {
                    $code = array_shift(array_keys($subfield)); // Will be only one of these: See XML_Record_WorldCat_MARC_Datafield API.
                    if (preg_match('/^\d+$/', $code)) continue; // Numberic codes indicate "control subfieds".
                    $value = array_shift(array_values($subfield)); // Will be only one of these: See XML_Record_WorldCat_MARC_Datafield API.
                    $subject_parts[] = trim($value);
                }
                if (0 == count($subject_parts)) continue;
                // Ignore any subdivisions; use only the top-level subject:
                $subject = preg_replace('/\s*[-.,:;]+$/', '', $subject_parts[0]);
                if (!in_array($subject, $subjects)) {
                    $subjects[] = $subject;
                }
            }
        }
        return $subjects;
    }

    public function get_summary( $marc_xml )
    {
        $summaries = array(); // To check for multiple summaries.
        foreach ($marc_xml->datafield('520') as $datafield) { // May be more than one of these.
            foreach($datafield->subfield('a') as $summary) { // Should be only one of these.
                $summaries[] = trim($summary);
            }
            foreach($datafield->subfield('b') as $summary) { // Should be only one of these.
                $summaries[] = trim($summary);
            }
        }
        return join("\n\n", $summaries);
    }

    public function get_contents( $marc_xml )
    {
        $contents = array();
        foreach ($marc_xml->datafield('505') as $datafield) { // Should be only one of these.
            foreach($datafield->subfields() as $subfield) { // May be many of these.
                $code = array_shift(array_keys($subfield)); // Will be only one of these: See XML_Record_WorldCat_MARC_Datafield API.
                if (preg_match('/^\d+$/', $code)) continue; // Numberic codes indicate "control subfieds".
                $value = array_shift(array_values($subfield)); // Will be only one of these: See XML_Record_WorldCat_MARC_Datafield API.
                $contents[] = trim($value);
            }
        }
        // TODO: Usually, separate contents elements are separated by '--' in MARC. Format these on separate lines?
        return join(' ', $contents);
    }

    public function get_target_audience_notes( $marc_xml )
    {
        $notes = array();
        foreach ($marc_xml->datafield('521') as $datafield) { // Should be only one of these.
            foreach($datafield->subfield('a') as $note) { // Should be only one of these.
                $notes[] = trim($note);
            }
        }
        return join('; ', $notes);
    }

    public function get_isbns( $ids )
    {
        $isbns = array();
        foreach ($ids as $id) {
            if ($id['type'] != 'isbn') continue;
            $isbn = trim($id['value']);
            preg_match('/^([0-9X]+)/', $isbn, $matches);
            $isbn_obj = ISBN_Factory::create( $matches[1] );
            $isbn13 = $isbn_obj->as_isbn13()->isbn();
            if (in_array($isbn13, $isbns)) continue;
            $isbns[] = $isbn13;
        }

        /* Many OCLC records do not have ISBNs.
        if (count($isbns) == 0) {
            throw new Exception("No ISBN found.");
        }
        */

        return $isbns;
    }

    public function get_sudoc_numbers( $ids )
    {
        $sudoc_numbers = array();
        foreach ($ids as $id) {
            if ($id['type'] != 'sudoc') continue;
            $sudoc_number = trim($id['value']);
            if (in_array($sudoc_number, $sudoc_numbers)) continue;
            $sudoc_numbers[] = $sudoc_number;
        }
        return $sudoc_numbers;
    }

    public function get_doi( $ids )
    {
        $doi = NULL;
        foreach ($ids as $id) {
            if ('doi' != $id['type']) continue;
            $doi = trim($id['value']);
            // Should be only one of these:
            break;
        }
        return $doi;
    }

    // TODO: Should probably use this as an ID for XML_Record_WorldCat.
    public function get_lccn( $marc_xml )
    {
        unset($lccn); 
        foreach ($marc_xml->datafield('010') as $datafield) { //  Should be only one of these.
            foreach($datafield->subfield('a') as $lccn) { //  Should be only one of these.
                // Just assume there will be only one:
                $lccn = trim($lccn);
                break;
            }
        }
        return $lccn;
    }

    // TODO: Use this as an ID for XML_Record_WorldCat?
    public function get_isrc( $marc_xml )
    {
        unset($isrc); 
        foreach ($marc_xml->datafield('024') as $datafield) { //  Should be only one of these.
            if (0 != $datafield->ind1()) continue;
            foreach($datafield->subfield('a') as $isrc) { //  Should be only one of these.
                // Just assume there will be only one:
                $isrc = trim($isrc);
                break;
            }
        }
        return $isrc;
    }

    public function get_access_rights( $marc_xml )
    {
        $rights = array(); // There may be many subfields that we need to concatenate.
        foreach ($marc_xml->datafield('506') as $datafield) { // Should be only one of these.
            foreach($datafield->subfields() as $subfield) { // May be many of these.
                $value = array_shift(array_values($subfield)); // Should be only one of these.
                $rights[] = trim($value);
            }
        }
        return join(" ", $rights);
    }

    public function get_physical_description( $marc_xml )
    {
        $descriptions = array(); // There may be many subfields that we need to concatenate.
        foreach ($marc_xml->datafield('300') as $datafield) { // Should be only one of these.
            foreach($datafield->subfields() as $subfield) { // May be many of these.
                $value = array_shift(array_values($subfield)); // Should be only one of these.
                $descriptions[] = trim($value);
            }
        }
        $description = join(' ', $descriptions);
        // Fix formatting:
        $description = preg_replace('/\s+([:;,])\s+/', '$1 ', $description);
        $description = preg_replace('/  /', ' ', $description);
        return $description;
    }

    public function get_physical_location( $marc_xml )
    {
        $locations = array(); // There may be many subfields that we need to concatenate.
        foreach ($marc_xml->datafield('851') as $datafield) { // Should be only one of these.
            foreach($datafield->subfields() as $subfield) { // May be many of these.
                $value = array_shift(array_values($subfield)); // Should be only one of these.
                $locations[] = trim($value);
            }
        }
        $location = join(', ', $locations);
        // Multiple location fields may contain comma separators already,
        // which will result in doubled commas.
        $location = preg_replace('/,,/', ',', $location);
        return $location;
    }

    public function get_provenance( $marc_xml )
    {
        $provenances = array(); // There may be many subfields that we need to concatenate.
        foreach ($marc_xml->datafield('561') as $datafield) { // Should be only one of these.
            foreach($datafield->subfields() as $subfield) { // May be many of these.
                $value = array_shift(array_values($subfield)); // Should be only one of these.
                $provenances[] = trim($value);
            }
        }
        return join(" ", $provenances);
    }

    public function get_note( $marc_xml )
    {
        $desired_tags = array(
            '500', // General Note
            '502', // Dissertation Note
        );

        // TODO: Add 502 for Theses/Dissertations.
        $notes = array(); // There may be many subfields that we need to concatenate.
        foreach ($desired_tags as $tag) {
            foreach ($marc_xml->datafield($tag) as $datafield) { // Should be only one of these.
                $note_parts = array();
                foreach($datafield->subfields() as $subfield) { // May be many of these.
                    $code = array_shift(array_keys($subfield)); // Will be only one of these: See XML_Record_WorldCat_MARC_Datafield API.
                    if (preg_match('/^\d+$/', $code)) continue; // Numberic codes indicate "control subfieds".
                    $value = array_shift(array_values($subfield)); // Will be only one of these: See XML_Record_WorldCat_MARC_Datafield API.
                    $note_parts[] = trim($value);
                }
                if (0 == count($note_parts)) continue;
                $note = implode('; ', $note_parts);
                $note = preg_replace('/\s+([:;,])\s+/', '$1 ', $note);
                if (!in_array($note, $notes)) {
                    $notes[] = $note;
                }
            }
        }
        return join("\n\n", $notes);
    }

    public function get_funding_info( $marc_xml )
    {
        $funding_infos = array(); 
        foreach ($marc_xml->datafield('536') as $datafield) { // There may be more than one of these, but probably not.
            foreach($datafield->subfield('a') as $funding_info) { // Should be only one of these.
                $funding_infos[] = trim($funding_info);
            }
        }
        return join('; ', $funding_infos);
    }

    public function get_event_info( $marc_xml )
    {
        $event_info = array(
            'event' => NULL,
            'location' => NULL,
        );
        $fields = array(); // There may be many subfields that we need to concatenate.
        foreach ($marc_xml->datafield('111') as $datafield) { // Should be only one of these.
            foreach($datafield->subfields() as $subfield) { // May be many of these.
                $code = array_shift(array_keys($subfield)); // Should be only one of these.
                $value = array_shift(array_values($subfield)); // Should be only one of these.
                $fields[] = trim($value);
                if ('c' == $code) {
                    $event_info['location'] = trim($value);
                }
            }
        }
        $event_info['event'] = join(" ", $fields);
        return $event_info;
    }

    public function get_part_of( $marc_xml )
    {
        // TODO: Better handling of Record Control Numbers, ISSNs, etc?
        // TODO: Use datafield 773?

        $desired_tags = array(
            '440', // Series Statement (Obsolete, but sometimes still used.)
            '490', // Series Statement
            '830', // Series Added Entry-Uniform Title
        );

        $part_of = NULL;
        foreach ($desired_tags as $tag) {
            foreach ($marc_xml->datafield($tag) as $datafield) {
                $part_of_parts = array();
                foreach($datafield->subfields() as $subfield) {
                    $code = array_shift(array_keys($subfield)); // Will be only one of these: See XML_Record_WorldCat_MARC_Datafield API.
                    if (preg_match('/^\d+$/', $code)) continue; // Numberic codes indicate "control subfieds".
                    $value = array_shift(array_values($subfield)); // Will be only one of these: See XML_Record_WorldCat_MARC_Datafield API.
                    $part_of_parts[] = trim($value);
                }
                if (0 == count($part_of_parts)) continue;
                $part_of = implode(' ', $part_of_parts);
                $part_of = preg_replace('/\s+([:;,])\s+/', '$1 ', $part_of);
                break;
            }
        }
        return $part_of;
    }

    public function get_urls( $marc_xml )
    {
        $urls = array(
            'full_text' => NULL,
            'others' => array(),
        );

        // Determine whether or not any of these URLs is a full-text URL, for the resource itself:
        $found_resource = FALSE; // ind2 == 0
        $found_resource_version = FALSE; // ind2 == 1

        foreach ($marc_xml->datafield('856') as $datafield) { // Should be only one of these.
            if ('4' != $datafield->ind1()) continue; // HTTP
            $subfields = array();
            foreach($datafield->subfields() as $subfield) { // May be many of these.
                $code = array_shift(array_keys($subfield));
                $value = array_shift(array_values($subfield));
                $subfields[$code] = trim($value); // For URLs, there should be only one instance of each code.
            }
            if (!array_key_exists('u', $subfields)) continue; // Uniform Resource Identifier
            $title = NULL;
            foreach (array('y','z','3') as $code) {  // Link text, Public note, Materials specified
                if (array_key_exists($code, $subfields)) {
                    $title = $subfields[$code];
                    break;
                }
            }
            $url = array('title' => $title, 'url' => $subfields['u'],);
          
            $ind2 = $datafield->ind2();
            if (0 == $ind2 && !$found_resource) {
                $urls['full_text'] = $url;
                $found_resource = TRUE;
            } else if (1 == $ind2 && !$found_resource && !$found_resource_version) {
                $urls['full_text'] = $url;
                $found_resource_version = TRUE;
            } else {
                $urls['others'][] = $url;
            }
        }
        return $urls;
    }

    public function get_type( $marc_xml )
    {
        $type = 'book';

        $leader = $marc_xml->leader();
        $l6 = substr($leader, 6, 1);
        $l7 = substr($leader, 7, 1);
        $l8 = substr($leader, 8, 1);

        $controlfield_006 = $marc_xml->controlfield('006');
        $c_006_0 = (NULL == $controlfield_006 ? NULL : substr($controlfield_006, 0, 1));
        $c_006_11 = (NULL == $controlfield_006 ? NULL : substr($controlfield_006, 11, 1));
        $c_006_12 = (NULL == $controlfield_006 ? NULL : substr($controlfield_006, 12, 1));
        $c_006_16 = (NULL == $controlfield_006 ? NULL : substr($controlfield_006, 16, 1));
        $controlfield_007 = $marc_xml->controlfield('007');
        $c_007_0 = (NULL == $controlfield_007 ? NULL : substr($controlfield_007, 0, 1));
        // controlfield 008 should be always defined:
        $controlfield_008 = $marc_xml->controlfield('008');
        $c_008_0 = substr($controlfield_008, 0, 1);
        $c_008_23 = substr($controlfield_008, 23, 1);
        $c_008_28 = substr($controlfield_008, 28, 1);
        $c_008_29 = substr($controlfield_008, 29, 1);
        $c_008_33 = substr($controlfield_008, 33, 1);

        $d_856_u_exists_with_ind2_0 = false;
        $d_856_u_exists_with_ind2_blank = false;
        foreach ($marc_xml->datafield('856') as $d_856) { // Electronic Location and Access
            $ind2 = $d_856->ind2();
            foreach ($d_856->subfield('u') as $subfield) {
                if ('0' == $ind2) {
                    $d_856_u_exists_with_ind2_0 = true;
                }
                if (' ' == $ind2) {
                    $d_856_u_exists_with_ind2_blank = true;
                }
            }
        }

        if (0 < count($marc_xml->datafield('502'))) { // Material type 'deg': Thesis/dissertation
            $type = 'thesis_dissertation';
        
        } else if ( // Material type 'gpb': Government publication
            // Added these datafield conditions, because the WorldCat spec wasn't working.
            /*
            (0 < count($marc_xml->datafield('027'))) || // Standard Technical Report Number
            (0 < count($marc_xml->datafield('074'))) || // GPO Item Number
            (0 < count($marc_xml->datafield('086'))) || // Government Document Classification Number
            (0 < count($marc_xml->datafield('088'))) || // Report Number
            */
            // This WorldCat spec doesn't seem to work to identify government documents:
            /*
            (in_array($c_008_28, array('o','z')) && in_array($l6, array('a','e','f','g','k','m','o','r','t'))) ||
            (in_array($c_006_11, array('o','z')) && in_array($c_006_0, array('a','e','f','g','k','m','o','r','t')))
            */
            // According to the MARC spec, this should be all we need:
            (!in_array($c_008_28, array(' ','u','|')))
          ) {
            $type = 'report';

        } else if ( // Material type 'cnp': Conference publicaton
            (1 == $c_008_29 && in_array($l6, array('a','t'))) ||
            (1 == $c_006_12 && in_array($c_006_0, array('a','t')))
          ) {
            $type = 'proceedings';

        // For archival types, the commented-out conditions below more closely follow te spec, but seem unnecessary,
        // and could lead to false positives, in practice.
        //} else if ('a' == $l8 || in_array($l6, array('p','t','d','f')) || in_array($c_006_0, array('p','t','d','f'))) {
        } else if ('a' == $l8) { // Material type 'arc': Archival material
            $type = 'archival_collection';

        } else if (in_array($l6, array('i','j')) || in_array($c_006_0, array('i','j'))) { // Document type 'rec': Sound recordings
            $type = 'audio';

        } else if ( // Material type 'vid': Videorecording
            'v' == $c_007_0 ||
            ('g' == $l6 && 'v' == $c_008_33) ||
            ('g' == $c_006_0 && 'v' == $c_006_16)
          ) {
            $type = 'film';

        // WorldCat classifies lots of books also as "Internet resource (url)". "book" seems more
        // specific, so we check for it and assign it first:
        } else if ( // Document type 'bks': Book
            (in_array($l6, array('a','t')) && in_array($l7, array('a','c','d','m'))) ||
            in_array($c_006_0, array('a','t'))
          ) {
            $type = 'book';

        } else if ( // Document type 'url': Internet resource
            ('m' == $l6 && $d_856_u_exists_with_ind2_0) ||
            (
             in_array($l6, array('a','c','d','i','j','p','t')) &&
             in_array($c_008_23, array('s','o')) &&
             ($d_856_u_exists_with_ind2_0 || $d_856_u_exists_with_ind2_blank)
            ) ||
            (
             in_array($l6, array('e','f','g','k','o','r')) &&
             in_array($c_008_29, array('s','o')) &&
             ($d_856_u_exists_with_ind2_0 || $d_856_u_exists_with_ind2_blank)
            )
          ) {
            $type = 'website';
        } else {
            // Based on the material and document types we exclude, everything else
            // should fall in this category:
            $type = 'other_multimedia';
        }

        return $type;
    }
}
