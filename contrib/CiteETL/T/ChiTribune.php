<?php

//require PEAR::* ???

require_once 'CiteETL/T/Transformer.php';
require_once 'CiteETL/T/Search/ScienceHealthNews.php';

class CiteETL_T_ChiTribune extends CiteETL_T_Transformer
{
    protected $issn = '1085-6706';
    protected $journal_name = 'Chicago Tribune';
    protected $publisher = 'Tribune Co.';
    protected $filter_pattern;
    
    function __construct()
    {
        $search = new CiteETL_T_Search_ScienceHealthNews();
        $this->filter_pattern = $search->pattern();
    }
    
    function transform( $record )
    {
        // output
        $citation = array();
        $citation['values'] = array();
        $citation_values = &$citation['values'];
    
        $simplepie_item = $record->as_simplepie_item();
    
        // May need to do something with this...
        //'callbacks' => array('required','verify_scalar','limit_title_length:128'),
        $citation_values['title'] = $record->title();
    
        $description = $record->description();
        $description = preg_replace('/\n/', '', $description);
        $description = preg_replace('/\s{2,}/', ' ', $description);
        $citation_values['field_abstract'][0]['value'] = $description;
    
        // No categories for LAT
    
        $citation_values['field_journal_name'][0]['value'] = $this->journal_name;
    
        // No authors for LAT
    
        $citation_values['field_issn'][0]['value'] = $this->issn;
    
        $url = $record->primary_id();
        $citation_values['field_url_0'][0]['value'] = $url;
    
        $year = $simplepie_item->get_date('Y');
        $datetime = $simplepie_item->get_date('Y-m-d H:i:s');
        // Sometimes Chicago Tribune doesn't supply a pubDate value, so the above will fail. Grrr....
        if (!isset($year) || !preg_match('/\d{4}/', $year)) {
            $url_parts = explode(',', $url);
            // Usually, the publication date is at the end of the first part of the URL:
            preg_match('/\d+$/', $url_parts[0], $matches);
            $date = $matches[0];

            unset($year, $month, $day);

            // Years may be 2 or 4 digits, and may be at the beginning or end of the string.
            // The month seems to always come before the day.
            if (8 == strlen($date)) {
                $first_four = substr($date, 0, 4);
                $last_four = substr($date, 4, 4);
                // Since the month always comes before the day, we should never have a month/day >= 1232.
                if ($first_four >= 1232) {
                    $year = $first_four;
                    $month = substr($last_four, 0, 2);
                    $day = substr($last_four, 2, 2);
                } else {
                    $year = $last_four;
                    $month = substr($first_four, 0, 2);
                    $day = substr($first_four, 2, 2);
                }
            } else if (6 == strlen($date)) {
                // I only found one URL that matches this pattern, and it was mmddyy, so we'll
                // assume that's the way these are always formatted. If the date fails validation later, too bad.
                $month = substr($date, 0, 2);
                $day = substr($date, 2, 2);
                // This will break after 2099, but if we're still using this code by then, we have much bigger problems.
                $year = substr($date, 4, 2) + 2000;
            }
            // The above date extraction is inherently unreliable, so do some good-enough sanity checks:
            if (!isset($year) || !isset($month) || $month <= 0 || $month >= 13 || !isset($day) || $day <= 0 || $day >= 32) {
                throw new Exception("No pubDate value provided, and cannot reliably extract a date from the string '$date', from url: $url");
            }
            $datetime = "$year-$month-$day 00:00:00";
        }
        $citation_values['field_year_published'][0]['value'] = $year;
        $citation_values['field_datetime_published'][0]['value'] = $datetime;
    
        $citation_values['timestamp'] = $simplepie_item->get_date('U');
    /* Attempting to import this field seems to cause errors...
        $citation_values['field_full_date_published'][0]['value']['mday'] =
            $simplepie_item->get_date('d'); 
        $citation_values['field_full_date_published'][0]['value']['mon'] =
            $simplepie_item->get_date('m'); 
        $citation_values['field_full_date_published'][0]['value']['year'] = $year; 
    */
    
        $citation_values['field_type'][0]['value'] = $this->type;
    
        $citation_values['field_format'][0]['value'] = $this->format;
    
        $citation_values['field_language'][0]['value'] = $this->language;
    
        // TODO: How do I prevent this from getting set to "true"? Just not even try to set it?
        //$citation_values['field_scholarly']['value'] = 0;
    
        $citation_values['field_source'][0]['value'] = 'Chicago Tribune';

        // Collect the sources and source IDs:
        $sources = array();
        $sources[] = array(
            'source' => $this->journal_name,
            'source_id' => $url,
        );
        $citation['sources'] = $sources;
    
        $this->filter( $citation );
    
        return $citation;
    }

} // end class CiteETL_T_ChiTribune 
