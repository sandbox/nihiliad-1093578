<?php

class CiteETL_T_Search_ScienceHealthNews
{
    protected $pattern;

    function __construct()
    {
    
        $keywords = array
        (
         'abortion',
         'advance directives',
         'animal experimentation',
         'animal industry',
         'animal research',
         'animal rights',
         'animal welfare',
         'assisted death',
         'assisted suicide',
         'beginning of life',
         'biodiversity',
         'bioethic',
         'biological warfare',
         'biomedical',
         'biomedical engineering',
         'biomedical enhancement',
         'biopolitics',
         'biotech',
         'brain death',
         'business ethics',
         'capital punishment',
         'chimera',
         'civil rights',
         'clon',
         'communicable disease',
         'confidentiality',
         'consent',
         'cryopreservation',
         'dehumanization',
         'dignity',
         'disab',
         'disaster planning',
         'disease outbreaks',
         'disease transmission',
         'drug industry',
         'duty to recontact',
         'dying',
         'embryo',
         'embryo creation',
         'embryonic',
         'environmental',
         'environmental health',
         'epidemic',
         'ethic',
         'eugenics',
         'euthanasia',
         'evolutionary ethics',
         'fetal research',
         'fraud',
         'gene therapy',
         'genetic engineering',
         'genetic',
         'genocide',
         'green movement',
         'health care',
         'health facilities',
         'health policy',
         'healthcare',
         'holocaust',
         'human cloning',
         'human experimentation',
         'human genome',
         'human reproductive technolog',
         'human rights',
         'in vitro',
         'industry ethics',
         'informed consent',
         'laboratory animals',
         'life support',
         'living donors',
         'mandatory programs',
         'mandatory reporting',
         'medical care',
         'medical ethics',
         'medical futility',
         'medical genetics',
         'medical innovations',
         'medical laws and legislation',
         'medical philosophy',
         'medical policy',
         'medical technology',
         'mentally ill',
         'neuroscience',
         'nursing ethics',
         'organ donation',
         'organ donor',
         'pandemic',
         'parental consent',
         'parental notification',
         'parity',
         'patient advocacy',
         'patient rights',
         'pharmaceutical',
         'posthumous conception',
         'preimplantation diagnosis',
         'prenatal',
         'privacy',
         'professional ethics',
         'public health',
         'public policy',
         'publication bias',
         'quackery',
         'quality of life',
         'quarantine',
         'refusal',
         'relativism',
         'reproductive health',
         'reproductive rights',
         'reproductive techniques',
         'research integrity',
         'resuscitation orders',
         'right to die',
         'right to life',
         'sale of organs',
         'sale of tissues',
         'scientific integrity',
         'sex determination',
         'sex preselection',
         'stem cell',
         'sterilization',
         'supreme court',
         'surroga',
         'terminal care',
         'terminally ill',
         'tissue',
         'tissue donors',
         'transplant',
         'truth disclosure',
         'uncompensated care',
         'vaccines',
         'value of life',
         'whistleblow',
         'withholding treatment',
         'women',
         'women\'s health',
        );
        $this->pattern = '/(' . join('|', $keywords) . ')/i';
    }

    public function pattern()
    {
        return $this->pattern;
    }

} // end class CiteETL_T_Search_ScienceHealthNews 
