<?php

//require PEAR::* ???

require_once 'CiteETL/T/Transformer.php';
require_once 'CiteETL/T/Search/ScienceHealthNews.php';

class CiteETL_T_Time extends CiteETL_T_Transformer
{
    // No ISSN for a website. What to use instead???
    // Look at: http://firstsearch.oclc.org/WebZ/FSFETCH?fetchtype=fullrecord:sessionid=fsapp9-58055-fo0la3e2-wgyspe:entitypagenum=25:0:recno=2:resultset=9:format=FI:next=html/record.html:bad=error/badfetch.html:entitytoprecno=2:entitycurrecno=2:numrecs=1
    //protected $issn = '';
    protected $journal_name = 'Time';
    protected $publisher = 'Time Inc.';
    protected $issn = '0040-781X';
    protected $filter_pattern;
    
    function __construct() 
    {
        $search = new CiteETL_T_Search_ScienceHealthNews();
        $this->filter_pattern = $search->pattern();
    }
    
    function transform( $record ) {
    
        // output
        $citation = array();
        $citation['values'] = array();
        $citation_values = &$citation['values'];
    
        $simplepie_item = $record->as_simplepie_item();
    
        // May need to do something with this...
        //'callbacks' => array('required','verify_scalar','limit_title_length:128'),
        $citation_values['title'] = $record->title();
    
        $citation_values['field_abstract'][0]['value'] =
            $record->description();
    
        $citation_values['field_journal_name'][0]['value'] = $this->journal_name;
        $citation_values['field_publisher'][0]['value'] = $this->publisher;
    
        /* Time doesn't seem to include an author/creator... */
        $year = $simplepie_item->get_date('Y');
    
        $citation_values['field_year_published'][0]['value'] = $year;
        $citation_values['field_datetime_published'][0]['value'] =
            $simplepie_item->get_date('Y-m-d H:i:s');
    
        /* TODO: Don't think I need this anymore... */
        $citation_values['timestamp'] = $simplepie_item->get_date('U');
    
        $citation_values['field_type'][0]['value'] = $this->type;
        $citation_values['field_format'][0]['value'] = $this->format;
        $citation_values['field_language'][0]['value'] = $this->language;
        $citation_values['field_issn'][0]['value'] = $this->issn;
    
        // TODO: How do I prevent this from getting set to "true"? Just not even try to set it?
        //$citation_values['field_scholarly']['value'] = 0;
    
        //$url = $simplepie_item->get_id();
        $url = $record->primary_id();
        $citation_values['field_url_0'][0]['value'] = $url;
    
        $citation_values['field_source'][0]['value'] = 'Time';

        // Collect the sources and source IDs:
        $sources = array();
        $sources[] = array(
            'source' => $this->journal_name,
            'source_id' => $url,
        );
        $citation['sources'] = $sources;
    
        $this->filter( $citation );
    
        return $citation;
    }

} // end class CiteETL_T_Time 
