<?php

require_once 'ISBN/Factory.php';

class CiteETL_T_WorldCat
{
    // TODO: This won't always be true. Come up with better handling for types.
    protected $type = 'Book';
    
    // Will these always be true????
    protected $format = 'Print';
    
    function __construct()
    {
    }
    
    function transform( $marc_xml ) {

        $citation = array();
        $citation['values'] = array();
        $citation_values = &$citation['values'];

        // Collect the sources and source IDs:
        $sources = array();
        $oclc_id = $marc_xml->primary_id();
        $citation_values['field_oclc_id'][0]['value'] = $oclc_id;
        $sources[] = array(
            'source' => 'OCLC',
            'source_id' => $oclc_id,
        );
    
        $citation_values['field_source'][0]['value'] = 'OCLC';

        $ids = $marc_xml->ids();

        // IDs continued: SuDoc Numbers
        $sudoc_numbers = $this->get_sudoc_numbers( $ids );
        // TODO: After the rollover to Drupal 6, allow the main citation record to have multiple SuDoc Numbers.
        if (count($sudoc_numbers) > 0) {
            //echo "sudoc_numbers = "; print_r( $sudoc_numbers );
            $citation_values['field_sudoc_number'][0]['value'] = $sudoc_numbers[0];
            $citation_values['field_type'][0]['value'] = 'Report/Government/Legal';
        } else {
            $citation_values['field_type'][0]['value'] = $this->type;
        }
        foreach ($sudoc_numbers as $sudoc_number) {
            $sources[] = array(
                'source' => 'SuDoc',
                'source_id' => $sudoc_number,
            );
        }

        // IDs continued: ISBNs
        try {
            $isbns = $this->get_isbns( $ids );
        } catch (Exception $e) {
            throw new Exception("OCLC record $oclc_id: " . $e->getMessage());
        }
        // TODO: After the rollover to Drupal 6, allow the main citation record to have multiple ISBNs.
        if (count($isbns) > 0) {
            $citation_values['field_isbn'][0]['value'] = $isbns[0];
        }
        foreach ($isbns as $isbn) {
            $sources[] = array(
                'source' => 'ISBN',
                'source_id' => $isbn,
            );
        }

        // TODO: Add URLs as IDs? Will be tricky, since, in WorldCat data, URLs are not always unique IDs.

        // IDs continued: Assign sources and source IDs for de-duping:
        $citation['sources'] = $sources;

        try {
            $title = $this->get_title( $marc_xml );
        } catch (Exception $e) {
            throw new Exception("OCLC record $oclc_id: " . $e->getMessage());
        }
        if (strlen($title) > 128) {
            $citation_values['field_alternative_title'][0]['value'] = $title;
            $title = substr($title, 0, 125) . '...';
        }
        $citation_values['title'] = $title;
    
        $authors = $this->get_authors( $marc_xml );
/* TODO: Eliminate this: 
WD php: Invalid argument supplied for foreach() in                                                                                  [error]
/opt/www/ethics01.oit.umn.edu/htdocs/prod/sites/all/modules/cite/contrib/CiteETL/T/WorldCat.php on line 85.
*/
        if (is_array($authors) && count($authors) > 0) {
            foreach ($authors as $index => $author) {
                $citation_values['field_author'][$index]['value'] = $author;
            }
        }
    
        $subjects = $this->get_subjects( $marc_xml );
        if (is_array($subjects) && count($subjects) > 0) {
            foreach ($subjects as $index => $subject) {
                $citation_values['field_subject'][$index]['value'] = $subject;
            }
        }

        $citation_values['field_abstract'][0]['value'] = $this->get_summary( $marc_xml );

        try { 
            $pub_data = $this->get_publication_data( $marc_xml );
        } catch (Exception $e) {
            throw new Exception("OCLC record $oclc_id: " . $e->getMessage());
        }
        $citation_values['field_publisher'][0]['value'] = $pub_data['publisher'];
        $citation_values['field_publisher_location'][0]['value'] = $pub_data['location'];

        try {
            list($datetime, $year) = $this->get_publication_dates( $marc_xml );
        } catch (Exception $e) {
            throw new Exception("OCLC record $oclc_id: " . $e->getMessage());
        }
        $citation_values['field_datetime_published'][0]['value'] = $datetime;
        $citation_values['field_year_published'][0]['value'] = $year;
    
        $citation_values['field_size'][0]['value'] = $this->get_size( $marc_xml );
    
        $citation_values['field_format'][0]['value'] = $this->format;
    
        $citation_values['field_language'][0]['value'] = $this->get_language( $marc_xml );

        // TODO: How do I prevent this from getting set to "true"? Just not even try to set it?
        //$citation_values['field_scholarly'][0]['value'] = 'Non-scholarly';

        return $citation;
    }
    
    public function get_authors( $marc_xml )
    {
        $authors = array();

        // Only one of these should be defined:
        $author_datafields = array();
        foreach (array('100','110','111','130') as $tag) {
            $datafields = $marc_xml->datafield($tag);
            if (count($datafields) > 0) {
                $author_datafields[] = $datafields;
            }
        }

        // TODO: This echo reveals that some author data deserves more complex treatment.
        //echo "author_datafields = \n"; print_r( $author_datafields );

        if (count($author_datafields) > 1) {
            error_log("Warning: multiple, unexpected author datafields found: OCLC ID " . $marc_xml->primary_id());
        }
        if (count($author_datafields) > 0) {
            $author_datafield = $author_datafields[0];
            foreach ($author_datafield as $datafield) {
                foreach ($datafield->subfield('a') as $author) {
                    $author = trim($author);
                    $author = preg_replace('/[\.,]+$/', '', $author);
                    if (!in_array($author, $authors)) {
                        $authors[] = $author;
                    }
                }
            }
        }
        if (count($authors) > 0) return join('; ', $authors);

        // If we don't have an author yet, try to get one from the title tag:
        foreach ($marc_xml->datafield('245') as $datafield) {
            foreach($datafield->subfield('c') as $author) {
                $author = trim($author);
                $author = preg_replace('/[\.,\/]+$/', '', $author);
                if (!in_array($author, $authors)) {
                    $authors[] = $author;
                }
            }
        }
        return $authors;

        /* Some OCLC/WorldCat records have no authors.
        throw new Exception("No author information found.");
        */
    }

    public function get_title( $marc_xml )
    {
        $titles = array(); // To check for multiple titles.
        foreach ($marc_xml->datafield('245') as $datafield) { // Should be only one of these.
            unset($title);
            foreach($datafield->subfield('a') as $title) { // Should be only one of these.
                $title = trim($title);
                $title = preg_replace('/[\.,\/]+$/', '', $title);
            }

            unset($title_remainder);
            foreach($datafield->subfield('b') as $title_remainder) { // Should be only one of these, & only if we have a subfield 'a'.
                $title_remainder = trim($title_remainder);
                $title_remainder = preg_replace('/[\.,\/]+$/', '', $title_remainder);
            }

            if (isset($title)) {
                if (isset($title_remainder)) {
                    $title .= " $title_remainder";
                }
                $titles[] = $title;
            } 
        }
        if (count($titles) == 0) {
            throw new Exception("No title information found.");
        }
        if (count($titles) > 1) {
            error_log("Warning: more than 1 title found: OCLC ID " . $marc_xml->primary_id());
        }
        return $titles[0];
    }

    public function get_publication_data( $marc_xml )
    {
        $pub_fields = $marc_xml->datafield('260');
        if (count($pub_fields) > 1) {
            error_log("Warning: more than 1 datafield 260 found: OCLC ID " . $marc_xml->primary_id());
        }
        if (count($pub_fields) == 0) {
            throw new Exception("No publisher information found.");
        }
        $pub_field = $pub_fields[0];

        $locations = array();
        foreach ($pub_field->subfield('a') as $location) {
            $location = preg_replace('/\s*[,:;]*\s*$/', '', $location);
            $locations[] = $location;
        }
        $pub['location'] = join('; ', $locations);

        $publishers = array();
        foreach ($pub_field->subfield('b') as $publisher) {
            $publisher = preg_replace('/\s*[,:;]*\s*$/', '', $publisher);
            $publishers[] = $publisher;
        }
        if (count($publishers) > 0) {
            $pub['publisher'] = join('; ', $publishers);
        } else {
            throw new Exception("No publisher information found.");
        }

        return $pub;
    }

    public function get_publication_dates( $marc_xml )
    {
        // TODO: Cover more of the cases in the spec: http://www.loc.gov/marc/bibliographic/bd008a.html
        $control_field = $marc_xml->controlfield('008');
        //echo "control_field = $control_field\n";

        // Initialize $year to the MARC undefined value:
        $year = 'uuuu';

        // Most dates won't have a month and day, except for type 'e', so we
        // set them to MySQL's undefined value:
        $month = '00';
        $day = '00';

        $date_type = substr($control_field, 6, 1);
        if (in_array($date_type, array('c','s','u',))) {
            $year = substr($control_field, 7, 4);
        }
        if (in_array($date_type, array('d','i','k','m','n','p','q','r',))) {
            $year1 = substr($control_field, 7, 4);
            $year2 = substr($control_field, 11, 4);
            $year = preg_match('/^[12]\d{3}$/', $year2) ? $year2 : $year1;
        }
        if (in_array($date_type, array('t',))) {
            $year1 = substr($control_field, 7, 4);
            $year2 = substr($control_field, 11, 4);
            $year = preg_match('/^[12]\d{3}$/', $year1) ? $year1 : $year2;
        }
        if (in_array($date_type, array('e',))) {
            $year = substr($control_field, 7, 4);
            $proto_month = substr($control_field, 11, 2);
            $proto_day = substr($control_field, 13, 2);

            // Some records violate the spec, putting a year in slots 11-14 instead
            // of a month and day. Try to handle that as best we can:
            if (preg_match('/^\d{2}$/', $proto_month) && $proto_month <= 12) {
                $month = $proto_month;
            }
            if (preg_match('/^\d{2}$/', $proto_day) && $proto_day <= 31) {
                $day = $proto_day;
            }
        }

        // Handle missing or B.C. dates, and bad years generated above:
        if (in_array($date_type, array('b','|')) || !preg_match('/^[12]\d{3}$/', $year)) {
            throw new Exception(
                "Unusable publication date in control field 008: '$control_field'"
            );
        }
        $datetime = "$year-$month-$day 00:00:00";
        return array($datetime, $year);
    }

    public function get_language( $marc_xml )
    {
        $control_field = $marc_xml->controlfield('008');
        $language = strtolower(substr($control_field, 35, 3));
        return $language;
    }

    public function get_size( $marc_xml )
    {
        $sizes = array(); // To check for multiple sizes.
        foreach ($marc_xml->datafield('300') as $datafield) { // Should be only one of these.
            foreach($datafield->subfield('a') as $size) { // Should be only one of these.
                $size = trim($size);
                $size = preg_replace('/[\s*:;,]+$/', '', $size);
                $sizes[] = $size;
            }
        }
        if (count($sizes) > 1) {
            error_log("Warning: more than 1 size found: OCLC ID " . $marc_xml->primary_id());
        }
        return $sizes[0];
    }

    public function get_subjects( $marc_xml )
    {
        $subjects = array(); // To check for multiple titles.
        foreach ($marc_xml->datafield('650') as $datafield) { // Should be only one of these.
            foreach($datafield->subfield('a') as $subject) { // Should be only one of these.
                $subject = trim($subject);
                $subject = preg_replace('/[\.,]+$/', '', $subject);
                if (!in_array($subject, $subjects)) {
                    $subjects[] = $subject;
                }
            }
        }
        return $subjects;
    }

    public function get_summary( $marc_xml )
    {
        $summaries = array(); // To check for multiple summaries.
        foreach ($marc_xml->datafield('520') as $datafield) { // Should be only one of these.
            foreach($datafield->subfield('a') as $summary) { // Should be only one of these.
                $summaries[] = trim($summary);
            }
            foreach($datafield->subfield('b') as $summary) { // Should be only one of these.
                $summaries[] = trim($summary);
            }
        }
        return join("\n\n", $summaries);
    }

    public function get_isbns( $ids )
    {
        $isbns = array();
        foreach ($ids as $id) {
            if ($id['type'] != 'isbn') continue;
            $isbn = trim($id['value']);
            preg_match('/^([0-9X]+)/', $isbn, $matches);
            $isbn_obj = ISBN_Factory::create( $matches[1] );
            $isbn13 = $isbn_obj->as_isbn13()->isbn();
            if (in_array($isbn13, $isbns)) continue;
            $isbns[] = $isbn13;
        }

        /* Many OCLC records do not have ISBNs.
        if (count($isbns) == 0) {
            throw new Exception("No ISBN found.");
        }
        */

        return $isbns;
    }

    public function get_sudoc_numbers( $ids )
    {
        $sudoc_numbers = array();
        foreach ($ids as $id) {
            if ($id['type'] != 'sudoc') continue;
            $sudoc_number = trim($id['value']);
            if (in_array($sudoc_number, $sudoc_numbers)) continue;
            $sudoc_numbers[] = $sudoc_number;
        }
        return $sudoc_numbers;
    }

} // end class CiteETL_T_WorldCat 
