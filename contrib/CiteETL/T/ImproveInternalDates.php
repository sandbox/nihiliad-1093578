<?php

// One off class to replace datetime_published values with datetime_epublished values
// where the latter is more precise, i.e. contains a month and day.

class CiteETL_T_ImproveInternalDates
{
    function transform($row)
    {
        //echo "row = " . var_dump($row) . "\n";

        // Output:
        $citation = array();
        $citation['nid'] = $row['nid'];
        $citation['values'] = array();
        $citation_values = &$citation['values'];
    
        // Required fields:
        $citation_values['field_source'][0]['value'] = $row['field_source_value'];
        // TODO: Work-aournd for bad values in the dev database. Remove in production!
        //$citation_values['field_source'][0]['value'] = 'PubMed';
        $citation_values['field_type'][0]['value'] = $row['field_type_value'];
        // TODO: Work-aournd for bad values in the dev database. Remove in production!
        //$citation_values['field_type'][0]['value'] = 'Journal Article';

        $citation_values['field_datetime_published'][0]['value'] = $row['field_datetime_epublished_value'];

        return $citation;
    }
} // end class CiteETL_T_ImproveInternalDates
