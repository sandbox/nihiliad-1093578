<?php

//require PEAR::* ???

require_once 'CiteETL/T/Transformer.php';
require_once 'CiteETL/T/Search/ScienceHealthNews.php';

class CiteETL_T_WashingtonPostHealth extends CiteETL_T_Transformer
{
    protected $issn = '0190-8286';
    protected $journal_name = 'Washington Post';
    protected $filter_pattern;
    
    function __construct()
    {
        $search = new CiteETL_T_Search_ScienceHealthNews();
        $this->filter_pattern = $search->pattern();
    }
    
    function transform( $record )
    {
        // output
        $citation = array();
        $citation['values'] = array();
        $citation_values = &$citation['values'];

        $simplepie_item = $record->as_simplepie_item();
    
        $citation_values['field_abstract'][0]['value'] =
            $record->content();
            //$simplepie_item->get_description();
    
        // May need to do something with this...
        //'callbacks' => array('required','verify_scalar','limit_title_length:128'),
        $citation_values['title'] = $record->title();
        // We need last_title_word for cleanup of the categories below.
        preg_match('/\s+(\S+)$/', $citation_values['title'], $title_matches);
        $last_title_word = $title_matches[1];
    
        // The Wa. Post puts every word of the title each as a separate
        // category at the beginning of the categories. We strip those out:
        $found_last_title_word = false;
        $index = 0;
        foreach ($record->categories() as $category) {
            if (!$found_last_title_word) {
                if ($category == $last_title_word) {
                    $found_last_title_word = true;
                }
                continue;
            }
            $citation_values['field_subject'][$index]['value'] = $category;
            $index++;
        }
    
        $citation_values['field_journal_name'][0]['value'] = $this->journal_name;
    
        $citation_values['field_author'][0]['value'] = $record->creator();
    
        $citation_values['field_issn'][0]['value'] = $this->issn;
    
    /*
     'volume' => array(
      'to' => 'field_volume/0/value',
      'from' => 'MedlineCitation/Article/Journal/JournalIssue/Volume',
      'callbacks' => array('verify_scalar'),
     ),
    
     'issue' => array(
      'to' => 'field_issue/0/value',
      'from' => 'MedlineCitation/Article/Journal/JournalIssue/Issue',
      'callbacks' => array('verify_scalar'),
     ),
    */
    
        $year = $simplepie_item->get_date('Y');
    
        $citation_values['field_year_published'][0]['value'] = $year;
        $citation_values['field_datetime_published'][0]['value'] =
            $simplepie_item->get_date('Y-m-d H:i:s');
    
        $citation_values['timestamp'] = $simplepie_item->get_date('U');
    /* Attempting to import this field seems to cause errors...
        $citation_values['field_full_date_published'][0]['value']['mday'] =
            $simplepie_item->get_date('d'); 
        $citation_values['field_full_date_published'][0]['value']['mon'] =
            $simplepie_item->get_date('m'); 
        $citation_values['field_full_date_published'][0]['value']['year'] = $year; 
    */
    
        $citation_values['field_type'][0]['value'] = $this->type;
    
        $citation_values['field_format'][0]['value'] = $this->format;
    
        $citation_values['field_language'][0]['value'] = $this->language;
    
        // TODO: How do I prevent this from getting set to "true"? Just not even try to set it?
        //$citation_values['field_scholarly']['value'] = 0;
    
        // We want to make sure to have a common, unique link to the article,
        // in case it shows up in other Wa. Post feeds, so we strip off the
        // feed-specific part of the url:
        //$url = preg_replace('/\?(nav|wprss)=rss_health/', '', $simplepie_item->get_id());
        $url = $record->primary_id();
        $citation_values['field_url_0'][0]['value'] = $url;
    
        $citation_values['field_source'][0]['value'] = 'Washington Post';

        // Collect the sources and source IDs:
        $sources = array();
        $sources[] = array(
            'source' => $this->journal_name,
            'source_id' => $url,
        );
        $citation['sources'] = $sources;
    
        $this->filter( $citation );
    
        return $citation;
    }

} // end class CiteETL_T_WashingtonPostHealth 
