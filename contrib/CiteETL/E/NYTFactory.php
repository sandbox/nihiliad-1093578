<?php

require_once 'CiteETL/E/ExtractorFactory.php';

class CiteETL_E_NYTFactory extends CiteETL_E_ExtractorFactory
{
    function __construct()
    {
        parent::__construct(array(
            'record_class'   => 'XML_Record_FeedItem_NYT',
            'record_file_class'   => 'XML_Record_File_Feed',
        ));
    }
} // end class CiteETL_E_NYTFactory
