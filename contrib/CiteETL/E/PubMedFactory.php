<?php

require_once 'CiteETL/E/ExtractorFactory.php';

class CiteETL_E_PubMedFactory extends CiteETL_E_ExtractorFactory
{
    function __construct()
    {
        parent::__construct(array(
            'record_class' => 'XML_Record_PubMed',
            'record_file_class' => 'XML_Record_File_PubMed',
        ));
    }

} // end class CiteETL_E_PubMedFactory
