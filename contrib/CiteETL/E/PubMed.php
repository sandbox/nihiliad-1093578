<?php

require_once 'XML/Record/Iterator.php';
require_once 'XML/Record/PubMed.php';

class CiteETL_E_PubMed extends XML_Record_Iterator
{

    function __construct()
    {
        parent::__construct(array(
            'directory' => '/opt/var/ethicshare/etl/pubmed/deduped/',
            'xml_record_class'   => 'XML_Record_PubMed',
        ));
    }

} // end class CiteETL_E_PubMed

?>
