<?php

require_once 'CiteETL/E/SimplePie.php';

class CiteETL_E_FeministBioethics extends CiteETL_E_SimplePie {

function __construct() {
    parent::__construct(array(
     'feed_url' => 'http://inscribe.iupress.org/action/showFeed?ui=0&mi=0&ai=za&jc=fab&type=etoc&feed=rss',
     'strip_html_tags' => array('br')
    ));
}

} // end class CiteETL_E_FeministBioethics
