<?php

require_once 'CiteETL/E/ExtractorFactory.php';

class CiteETL_E_WorldCatFactory extends CiteETL_E_ExtractorFactory
{
    function __construct()
    {
        parent::__construct(array(
            'record_class' => 'XML_Record_WorldCat_MARC',
            'record_file_class' => 'XML_Record_File_WorldCat_MARC',
        ));
    }

} // end class CiteETL_E_WorldCatFactory
