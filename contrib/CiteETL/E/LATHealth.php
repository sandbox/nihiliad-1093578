<?php

require_once 'LocalFeed/Item/Iterator.php';

class CiteETL_E_LATHealth extends LocalFeed_Item_Iterator
{
    function __construct( $params )
    {
        $input_directory = $params['input_directory'];
        if (!isset($input_directory)) {
            throw new Exception("Missing required param 'input_directory'");
        }
    
        parent::__construct(array(
         'input_directory' => $input_directory,
         'strip_html_tags' => array('br','a','img'),
        ));
    }
} # end class CiteETL_E_LATHealth
