<?php

require_once 'XML/Record/Iterator.php';
require_once 'XML/Record/WorldCat/MARC.php';

class CiteETL_E_WorldCat extends XML_Record_Iterator
{

    function __construct()
    {
        parent::__construct(array(
            'directory' => '/opt/var/ethicshare/etl/worldcat/deduped/',
            'xml_record_class'   => 'XML_Record_WorldCat_MARC',
        ));
    }

} // end class CiteETL_E_WorldCat

?>
