<?php

// TODO: Use an autoloader and remove this line:
require_once 'Cite/Model.php';

class CiteETL_L_Updater
{
    protected $mysqli;
    public function mysqli()
    {
        return $this->mysqli;
    }
    protected function set_mysqli( $mysqli )
    {
        $this->mysqli = $mysqli;
    }

    protected $citeModel;
    public function citeModel()
    {
        return $this->citeModel;
    }
    protected function setCiteModel( $citeModel )
    {
        $this->citeModel = $citeModel;
    }

    protected $log_message; // A common log message to set for all loaded citations.  public function log_message()
    public function log_message()
    {
        return $this->log_message;
    }
    protected function set_log_message( $log_message )
    {
        $this->log_message = $log_message;
    }

    public function __construct( $params )
    {
        $mysqli = $params['mysqli'];
        if (!isset($mysqli)) {
            throw new Exception("Missing required param 'mysqli'");
        }
        $this->set_mysqli( $mysqli );

        $citeModel = new Cite_Model($mysqli);
        $this->setCiteModel($citeModel);

        $log_message = $params['log_message'];
        if (!isset($log_message)) {
            throw new Exception("Missing required param 'log_message'");
        }
        $this->set_log_message( $log_message );
    }

    function __destruct()
    {
    }

    public function update( $citation )
    {
        $citeModel = $this->citeModel();

        // Get the already-existing node for this citation, if it exists:

        if (isset($citation['nid'])) {
            // This is probably an internal update, based on a query of the EthicShare
            // database itself for known-existing records:
            $nid = $citation['nid'];
        } else {
            $sources = $citation['sources'];
            $nids_found = array();
            $identifiers = array();
            foreach ($sources as $source_array) {
                $source_id = $source_array['source_id'];
                $source = $source_array['source'];
                $identifier_string = "$source => $source_id";
                $identifiers[] = $identifier_string; // In case we need it for an error message.
    
                // Should be only one nid for each source_id/source combination:
                foreach ($citeModel->selectCiteSourceNids($source_id, $source) as $nid) {
                    if (!array_key_exists($nid, $nids_found)) {
                        $nids_found[$nid] = array();
                    }
                    $nids_found[$nid][] = $identifier_string;
                }
            }
    
            $nids = array_keys($nids_found);
    
            if (0 == count($nids)) {
                $identifiers_string = join('; ', $identifiers);
                throw new Exception("No nid found for citation with identifiers '$identifiers_string'");
            } else if (1 < count($nids)) {
                $identifiers_string = '';
                foreach ($nids as $nid) {
                    $nid_identifiers_string = join('; ', $nids_found[$nid]);
                    $identifiers_string .= "nid => $nid: $nid_identifiers_string; ";
                }
                throw new Exception("Multiple nid's found for citations with these identifiers: '$identifiers_string'");
            } else {
                $nid = array_shift($nids);
            }
        }

        $node = node_load($nid);

        if (false == $node) {
            throw new Exception("Failed to load node with nid '$nid'");
        }

        if (!$this->records_differ($citation, $node)) {
            return; // Don't update the record if it hasn't changed.
        }
        //echo "node $nid is revised\n";

        $old_vid = $node->vid;

        global $user;

        // TODO: Use a value from a config file! However, usually any record that we're updating
        // will be a published record, so this should be okay for now.
        $name = 'admin';
        $uid = 1;
        $citation['values']['name'] = $name;
        $citation['values']['uid'] = $uid; // 'admin': Doesn't work!
        $citation['values']['op'] = t('Save'); // required value

        // Create a new revision and put a message in the log:
        $citation['values']['revision'] = 1;
        //$citation['values']['revision_uid'] = 1; // TODO: This doesn't seem to work. See below.
        $citation['values']['log'] = $this->log_message();

        // Get some values here, for later use (see below), before we call
        // drupal_execute(), because it has side effects that change $citation. Boo!        
        $type = $citation['values']['field_type'][0]['value'];
        $source = $citation['values']['field_source'][0]['value'];
        // New schema problematic fields (with select lists):
        $types = $citation['values']['field_types'][0]['value'];
        $primary_language = $citation['values']['field_primary_language'][0]['value'];

        // If the new tags differ from the current tags, be sure to preserve the current tags:
        if (is_array($citation['values']['taxonomy']['tags']) && is_array($node->tags)) {
            $citation['values']['taxonomy']['tags'] =
                $this->preserve_tags($citation, $node); 
            //echo "new citation tags = "; var_dump($citation['values']['taxonomy']['tags']); echo "\n";
        }

        // Eliminates the error:
        // "call_user_func_array(): First argument is expected to be a valid callback, 'node_form' was given in $root/includes/form.inc on line 372."
        module_load_include('pages.inc', 'node');

        $node_path = drupal_execute('cite_node_form', $citation, (object)$node);
        // Was hoping this function in the form_batch module would fix some of the bugs associated with drupal_execute,
        // but it didn't work.
        //$node_path = form_batch_drupal_execute('cite_node_form', $citation, (object)$node);

        // drupal_execute errors.
        $errors = form_get_errors();
        if (count($errors)) {
            $message = join('; ', $errors);
    
            // May be nice to include a dump of the citation, maybe
            // also log levels, also:
            throw new Exception( $message );
        }

        // Work around some strange drupal_execute bugs by fixing some data
        // with mysqli. First we need the vid of the revision we
        // just created...

        $new_vid = $citeModel->selectMaxVid($nid);

        //...then update the problematic columns in the appropriate tables:

        $citeModel->updateCite($type, $source, $nid, $new_vid);
        $citeModel->updateCiteNewSchema($types, $primary_language, $nid, $new_vid);
        $citeModel->updateNode($uid, $nid, $new_vid);
        $citeModel->updateNodeRevisions($uid, $nid, $new_vid);
    }

    public function records_differ($citation, $node) {
        // Testing comparison of new citaiton to already-existing node:
        $records_differ = false;
        foreach ($citation['values'] as $field => $value) {
            if (!isset($node->$field)) {
                //echo "$field not set in already-existing node.\n";
                $records_differ = true;
                break;
            }

            // Known problematic fields: We never get the correct values from the node
            // for these fields, for some unknown reason:
            if ($field == 'field_type' || $field == 'field_source') {
                continue;
            }

            // Title is the only field whose value will not be an array.
            if ($field == 'title') {
                if ($node->title != $value) {
                    //echo "node title differs from new title.\n";
                    $records_differ = true;
                    break;
                }
            } else {
                $node_value = $node->$field;
                foreach ($value as $index => $array) {
                    $node_array = $node_value[$index];

                    // We never set a 'format' for the 'abstract'. Should we?
                    // For now, we just don't compare it.
                    if ($field == 'field_abstract') {
                        unset($node_array['format']);
                    }
                    
                    if ($array != $node_array) {
                        //echo "node differs from new values for field $field, index $index.\n";
                        //echo "node: $field: $index => "; var_dump( $node_array ); echo "\n";
                        //echo "citation: $field: $index => "; var_dump( $array ); echo "\n";
                        $records_differ = true;
                        break;
                    }
                } 
            }
            //echo "'$field' => "; var_dump($value); echo "\n";
        }
        //echo "node = "; var_dump( $node ); echo "\n";
        return $records_differ;
    }

    public function preserve_tags($citation, $node)
    {
        $new_tags = array();
        foreach ($citation['values']['taxonomy']['tags'] as $vocabulary => $tags) {
            $new_tags[$vocabulary] = split(',', $tags);
        }
        //echo "new tags = "; var_dump($new_tags); echo "\n";
        //echo "current tags = "; var_dump($node->tags); echo "\n";
        foreach ($node->tags as $vocabulary => $tags) {
            if (!isset($new_tags[$vocabulary])) {
                $new_tags[$vocabulary] = array();
            }
            foreach ($tags as $obj) {
                $tag = $obj->name;
                if (!in_array($tag, $new_tags[$vocabulary])) {
                    $new_tags[$vocabulary][] = $tag;
                }
            }
        }
        //echo "new tags = "; var_dump($new_tags); echo "\n";
        $new_tags_formatted = array();
        foreach ($new_tags as $vocabulary => $tags) {
            $new_tags_formatted[$vocabulary] = join(',', $tags);
        }
        return $new_tags_formatted;
    }

} # end class CiteETL_L_Updater 
