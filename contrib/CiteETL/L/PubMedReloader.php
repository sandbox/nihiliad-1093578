<?php

class CiteETL_L_PubMedReloader
{
    function load( $citation )
    {
        unset($sources, $pmid, $nid);
        $source_primary = 'PubMed';
        
        $sources = $citation['sources'];
        foreach ($sources as $source_array) {
            $source = $source_array['source'];
            $source_id = $source_array['source_id'];
            if ($source == $source_primary) {
                $pmid = $source_id;
                break;
            }
        }
        // TODO: Make this generic, so we don't need separate reloaders for each source.
        // However, this should never happen, anyway.
        if (!isset($pmid)) {
            throw new Exception("No pmid found for PubMed record");
        }
    
        $node_exists = false;
    
        $pre_existing_search = db_query(
            "select nid
             from cite_source
             where source_id = '%s'
               and source = '%s'",
             array($source_id, $source_primary) 
        );
        while ($pre_existing = db_fetch_object($pre_existing_search)) {
            $nid = $pre_existing->nid;
        }
        if (!isset($nid)) {
            error_log("Skipping new, unloaded record");
        }
    
        $node = node_load($nid);

        $citation['status'] = 1;
        // TODO: Use the name specified via drush or the caller!!!
        $citation['name'] = 'admin';
        //$es_citation['field_full_date_published']['timezone'] = '';
        //print_r($citation);
    
        $node_path = drupal_execute('cite_node_form', $citation, $node);
    
        $errors = form_get_errors();
        if (count($errors)) {
            $message = join('; ', $errors);
    
            // May be nice to include a dump of the citation, maybe
            // also log levels, also:
            throw new Exception( $message );
        }
    
        // Workaround for broken importing of: CCK Date fields, citation "type",
        // and citation "scholarly":
        preg_match('/\d+$/', $node_path, $matches);
        db_query(
            "update content_type_cite
             set field_full_date_published_value = %d,
                 field_type_value = '%s',
                 field_scholarly_value = '%s'
             where nid = %d",
             array(
                 $citation['timestamp'],
                 $citation['field_type'][0]['value'],
                 $citation['field_scholarly'][0]['value'],
                 $nid
             )
        );
    
        // TODO: Add code to insert any missing sources and source_ids.
        // TODO: Add exception handling!!!!
    }

} # end class CiteETL_L_PubMedReloader 
