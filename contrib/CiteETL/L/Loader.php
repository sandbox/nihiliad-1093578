<?php

// TODO: Use an autoloader and remove this line:
require_once 'Cite/Model.php';

class CiteETL_L_Loader
{
    // TODO: Use Cite_ModelRegistry and eliminate $mysqli:
    protected $mysqli;
    public function mysqli()
    {
        return $this->mysqli;
    }
    protected function set_mysqli( $mysqli )
    {
        $this->mysqli = $mysqli;
    }

    protected $citeModel;
    public function citeModel()
    {
        return $this->citeModel;
    }
    protected function setCiteModel( $citeModel )
    {
        $this->citeModel = $citeModel;
    }

    protected $log_message; // A common log message to set for all loaded citations.
    public function log_message()
    {
        return $this->log_message;
    }
    protected function set_log_message( $log_message )
    {
        $this->log_message = $log_message;
    }

    public function __construct( $params )
    {
        $mysqli = $params['mysqli'];
        if (!isset($mysqli)) {
            throw new Exception("Missing required param 'mysqli'");
        }
        $this->set_mysqli( $mysqli );

        $citeModel = new Cite_Model($mysqli);
        // Prevent MySQL from re-indexing the cite_source table after every insert:
        $citeModel->dropCiteSourceIndex();
        $this->setCiteModel($citeModel);

        $log_message = $params['log_message'];
        if (!isset($log_message)) {
            throw new Exception("Missing required param 'log_message'");
        }
        $this->set_log_message( $log_message );
    }

    function __destruct()
    {
        $this->citeModel()->addCiteSourceIndex();
    }

    public function load( $citation )
    {
        $node = array('type' => 'cite');
        $citation['values']['status'] = 1;

        // TODO: Get these from a config file or something!
        $name = 'admin';
        $uid = 1;
        $citation['values']['name'] = $name;
        $citation['values']['uid'] = $uid; // 'admin': Doesn't work!

        $citation['values']['op'] = t('Save'); // required value

        // Create a new revision and put a message in the log:
        $citation['values']['revision'] = 1;
        //$citation['values']['revision_uid'] = 1; // TODO: This doesn't seem to work. See below.
        $citation['values']['log'] = $this->log_message();

        // Get some values here, for later use (see below), before we call
        // drupal_execute(), because it has side effects that change $citation. Boo!        
        $type = $citation['values']['field_type'][0]['value'];
        $source = $citation['values']['field_source'][0]['value'];
        // New schema problematic fields (with select lists):
        $types = $citation['values']['field_types'][0]['value'];
        $primary_language = $citation['values']['field_primary_language'][0]['value'];

        // Eliminates the error:
        // "call_user_func_array(): First argument is expected to be a valid callback, 'node_form' was given in $root/includes/form.inc on line 372."
        module_load_include('pages.inc', 'node');

        $node_path = drupal_execute('cite_node_form', $citation, (object)$node);
        // Was hoping this function in the form_batch module would fix some of the bugs associated with drupal_execute,
        // but it didn't work.
        //$node_path = form_batch_drupal_execute('cite_node_form', $citation, (object)$node);

        //echo "citation = "; var_dump( $citation );
        //echo "node = "; var_dump( $node );

        unset($nid);
        $nid = $citation['nid'];
        if (!$nid) {
            // Would be nice if Drupal provided better error-checking for this.
            throw new Exception("Failed to load citation: nid is undefined."); 
        }

        $errors = form_get_errors();
        if (count($errors)) {
            $message = join('; ', $errors);
            //echo "form error message = $message\n";
    
            // May be nice to include a dump of the citation, maybe
            // also log levels, also:
            throw new Exception( $message );
        }
    
        // Work around some strange drupal_execute bugs by fixing some data 
        // with mysqli. First we need the vid of the revision we
        // just created...

        $citeModel = $this->citeModel();
        $new_vid = $citeModel->selectMaxVid($nid);

        //...then update the problematic columns in the appropriate tables:

        // The use of form_batch_drupal_execute made the following line of code unnecessary. Yay!
        $citeModel->updateCite($type, $source, $nid, $new_vid);
        $citeModel->updateCiteNewSchema($types, $primary_language, $nid, $new_vid);
        $citeModel->updateNode($uid, $nid, $new_vid);
        $citeModel->updateNodeRevisions($uid, $nid, $new_vid);

        // cite_source is not really a Drupal table, so we update it directly:
        foreach ($citation['sources'] as $source_array) {
            $source = $source_array['source'];
            $source_id = $source_array['source_id'];
            // TODO: Roll back the creation of the CCK cite node??? Might need PDO, and therefore Drupal 7, for that.
            $citeModel->insertCiteSource($nid, $source_id, $source);
        }
    }

}
