<?php



/*
 * Implementation of hook_theme()
 */
// function cite_styles_export_theme($existing, $type, $theme, $path) {
//   return array(
//     'cite_ris' => array(
//       'arguments' => array('node' => NULL),
//       ),
//     'cite_refworks' => array(
//       'arguments' => array('node' => NULL),
//       ),
//     'cite_bibtex' => array(
//       'arguments' => array('node' => NULL),
//       ),
//     'cite_openurl_2000' => array(
//       'arguments' => array('node' => NULL),
//       ),    
//     'cite_openurl_2004' => array(
//       'arguments' => array('node' => NULL),
//       ),        
//   );
// }


// function cite_styles_export_registry() {
//   return module_invoke_all('cite_export');
// }

/*
* Implementation of hook cite_export
*/
// function cite_styles_export_cite_export() {
//   return array(
//     'ris' => array(
//       'name' => t('RIS/EndNote'),
//       'theme_hook' => 'cite_ris',        
//       'description' => 'Outputs Nodes to the RIS citation format'
//     ),
//     'refworks' => array(
//       'name' => t('RefWorks'),
//       'theme_hook' => 'cite_refworks',        
//       'description' => 'Outputs Nodes to the RefWorks',
//     ),    
//   ); 
// }

/*
* Implementation of hook_cite_styles
* Call the cite_styles hook for all export themes
*/
// function cite_styles_export_cite_styles() {
//   $styles = cite_styles_export_registry();
//   foreach ($styles as $style) {
//       $cite_styles[] = $style['theme_hook'];
//   }
//   
//   return $cite_styles;
// }

/**
* Accept a node id and return the node as RIS
* 
* @return string
*/
function theme_cite_ris($node) {
  $ris[] = 'ID  - [nid]'. "\r";
  $ris[] = 'T1  - [title]'. "\r";
  $ris[] = 'JO  - [field_journal_name-formatted]'. "\r";
  $ris[] = 'VL  - [field_volume-formatted]'. "\r";
  $ris[] = 'IS  - [field_issue-formatted]'. "\r";
  $ris[] = 'SP  - [field_start_page-formatted]'. "\r";
  $ris[] = 'EP  - [field_end_page-formatted]'. "\r";
  $ris[] = 'SN  - [field_issn-formatted][field_isbn-formatted]'. "\r";
  $ris[] = 'N2  - [field_abstract-formatted]'. "\r";
  
  
  $datetime = token_replace('[field_datetime_published-formatted]', $type = 'node', $node);
  $year   = substr($datetime , 0, 4);
  $month  = ((int) substr($datetime, 5, 2) != 0) ? substr($datetime, 5, 2) : NULL;
  $day    = ((int) substr($datetime, 8, 2) != 0) ? substr($datetime, 8, 2) : NULL;
  $ris[] = sprintf('PY  - %s/%s/%s/', $year, $month, $day) ."\r";

  
  //icky authors and subjects are delimited by semicolon
  $author = token_replace('[field_author-formatted]', $type = 'node', $node);
  $auth = cite_styles_exploder($author, 'AU  - ', NULL);
  $subject = token_replace('[field_subject-formatted]', $type = 'node', $node);
  $sub = cite_styles_exploder($subject, 'K1  - ', NULL);

  $ris = array_merge($ris, $auth, $sub);
  
  $replaced = token_replace($ris, $type = 'node', $node);
  //only output stuff we found
  foreach ($replaced as $item) {
    $entry = trim(substr($item, 5));
      $ris_item .= $item;
  }

  //we need "raw" b/c this is not a field set to display, but values set by admins, so raw is ok
  $cck_type = token_replace('[field_type-raw]', $type = 'node', $node); 
  $ty = _ris_types($cck_type);
  if ($ty) {
    $output =  'TY  - '. $ty . "\r" . $output;
    $output .= $ris_item;
    $output .= 'ER  -' . "\r\r";
    return $output;
  }
  else {
    watchdog('Cite Export Styles - RIS', 'Unable to process request for this publication type: '. $cck_type, WATCHDOG_NOTICE);
  }
  
  // var_dump($output);
  // die();
  return $output;
}

/**
* Map publications types to RIS types
* 
* @return string
*/
function _ris_types($type) {
  switch($type) {
    case 'article';
      return 'JOUR';
    case 'News Article';
      return 'JOUR';

    case 'Book';
      return 'BOOK';      
    case 'Journal Article';
      return 'JOUR';      
    case 'News/Magazine';             //really, there are two possible codes here: MGZN and NEWS
      return 'NEWS';
    case 'Report/Government/Legal'; 
      return 'GEN';                   //"Generic" for now
    case 'Archival/Media/Teaching Material';
      return 'GEN';                   //"Generic" for now
  }
}

/**
* Accept a node id and return the node as RIS
* 
* @return string
*/
function theme_cite_refworks($node) {
  $ref[] = 'ID [nid]'. "\r";
  $ref[] = 'T1 [title]'. "\r";
  $ref[] = 'JF [field_journal_name-formatted]'. "\r";
  $ref[] = 'VO [field_volume-formatted]'. "\r";
  $ref[] = 'IS [field_issue-formatted]'. "\r";
  $ref[] = 'SP [field_start_page-formatted]'. "\r";
  $ref[] = 'EP [field_end_page-formatted]'. "\r";
  $ref[] = 'SN [field_issn-formatted]'. "\r";
  $ref[] = 'SN [field_isbn-formatted]'. "\r";
  $ref[] = 'AB [field_abstract-formatted]'. "\r";
  
  
  $datetime = token_replace('[field_datetime_published-formatted]', $type = 'node', $node);
  $year   = substr($datetime , 0, 4);
  $ref[] = sprintf('YR %s', $year) ."\r";
  
    
  //icky authors and subjects are delimited by semicolon
  $author = token_replace('[field_author-formatted]', $type = 'node', $node);
  $auth = cite_styles_exploder($author, 'A1 ', NULL);
  $subject = token_replace('[field_subject-formatted]', $type = 'node', $node);
  $sub = cite_styles_exploder($subject, 'K1 ', NULL);
  
  $replaced = token_replace(array_merge($ref, $auth, $sub), $type = 'node', $node);
  //only output stuff we found
  foreach ($replaced as $item) {  
    $entry = trim(substr($item, 2));
    if ($entry != '') {
      $ref_item .= $item;
    }
  }

  $cck_type = token_replace('[field_type-raw]', $type = 'node', $node);
  $RT = _refworks_types($cck_type);
  if ($RT) {
    $output = 'RT '. $RT ."\r";
    $output .= $ref_item;
    return $output;
  }
  else {
    watchdog('Cite Export Styles - RefWorks', 'Unable to process request for this publication type: '. $cck_type, WATCHDOG_ERROR);  
  }
}


/**
* Map publications types to RIS types
* 
* @return string
*/
function _refworks_types($type) {
  switch($type) {
    case 'article';
      return 'Journal Article';
    case 'News Article';
      return 'Newspaper Article';
      

    case 'Book';
      return 'Book, Whole';    
    case 'Journal Article';
      return 'Journal Article';          
    case 'News/Magazine';                     //Two possible codes here: Magazine Article and Newspaper Article
      return 'Newspaper Article';
    case 'Report/Government/Legal'; 
      return 'Generic';                       //"Generic" for now Possibilities: Bills/Resolutions, Case/Court Decisions, Report, Patent
    case 'Archival/Media/Teaching Material';
      return 'Generic';                       //"Generic" for now - Unpublished Material, Video/DVD, Sound Recording, Motion Picture
  }
}



function theme_cite_bibtex($node) {

  $bib[] = '  title = "[title]"'. ",\r";
  $bib[] = '  journal = "[field_journal_name-formatted]"'. ",\r";
  $bib[] = '  volume = "[field_volume-formatted]"'. ",\r";
  $bib[] = '  number = "[field_issue-formatted]"'. ",\r";
  $bib[] = '  pages = "[field_start_page-formatted]--[field_end_page-formatted]"'. "\r";
  $bib[] = '  issn = "[field_issn-formatted]"'. ",\r";
  $bib[] = '  isbn = "[field_isbn-formatted]"'. ",\r";
  $bib[] = '  abstract = "[field_abstract-formatted]"'. ",\r";
  
  $datetime = token_replace('[field_datetime_published-formatted]', $type = 'node', $node);
  $year   = substr($datetime , 0, 4);
  $bib[] = sprintf('  year = "%s"', $year) .",\r";
  
  //subject semicolon to comma reach-around
  $subject = str_replace(';', '--[commahere]--', token_replace('[field_subject-formatted]', $type = 'node', $node));
  $subject = str_replace(',', ' -', $subject);
  $subject = str_replace('--[commahere]--', ',', $subject);
  $bib[] = sprintf('  keywords = "%s"', $subject). ",\r";
  $author = token_replace('[field_author-formatted]', $type = 'node', $node);
  $author = str_replace(';', ' and ', $author);
  $bib[] = sprintf('  author = "%s"', $author) .",";

  $replaced = token_replace($bib, $type = 'node', $node);
  
  //only output stuff we found
  foreach ($replaced as $item) {  
    $entry = explode('= "', $item);
    if (trim($entry[0], "\x22\x27") != '') {
      $ref_item .= $item;
    }
  }

  $nid = token_replace('[nid]', $type = 'node', $node);  
  $cck_type = token_replace('[field_type-raw]', $type = 'node', $node);
  $pub_type = _bibtex_types($cck_type);
  if ($pub_type) {
    $output  = '@'. $pub_type . '{'. $nid .", \r";
    $output .= $ref_item;
    $output .= "\r} \r ";    
    return $output;
  }
  else {
    watchdog('Cite Export Styles - RefWorks', 'Unable to process request for this publication type: '. $type_error, WATCHDOG_ERROR);
  }
}


/**
* Map publications types to RIS types
* 
* @return string
*/
function _bibtex_types($type) {
  switch($type) {
    case 'article';
      return 'article';
    case 'News Article';
      return 'article';
      
    case 'Book';
      return 'book';      
    case 'Journal Article';
      return 'article';
    case 'News/Magazine';                     
      return 'article';
    case 'Report/Government/Legal'; 
      return 'misc';
    case 'Archival/Media/Teaching Material';
      return 'misc';
  }
}

/**
* Accept a node id and return the node as a OpenURL Z39.88 2004 string
* 
* @return string
*/
function theme_cite_openurl_2004($node) {  
  //we need "raw" b/c this is not a field set to display, but values set by admins, so raw is ok
  $cck_type = token_replace('[field_type-raw]', $type = 'node', $node); 
  $type = _openurl_types($cck_type);
  //open url groups what we call types into two primary types with secondary types set as genres
  if ($type == 'book') {
    $token  = '&rft_val_fmt=info:ofi/fmt:kev:mtx:book';
    $token .= '&rft.genre=book';    
    $token .= '&rft.btitle='.  urlencode(token_replace('[title]', $type = 'node', $node));   
    $token .= '&rft.isbn='.    urlencode(token_replace('[field_isbn-formatted]', $type = 'node', $node));    
    
    $datetime = token_replace('[field_datetime_published-formatted]', $type = 'node', $node);
    $year   = substr($datetime , 0, 4);
    $token .= '&rft.date='.    urlencode($year);    
    
  }
  elseif ($type == 'article') {
    $token  = '&rft_val_fmt=info:ofi/fmt:kev:mtx:journal';
    $token .= '&rft.genre=article';
    $token .= '&rft.jtitle='.  urlencode(token_replace('[field_journal_name-formatted]', $type = 'node', $node));
    $token .= '&rft.atitle='.  urlencode(token_replace('[title]', $type = 'node', $node));    
    $token .= '&rft.issn='.    urlencode(token_replace('[field_issn-formatted]', $type = 'node', $node));
    
    $datetime = token_replace('[field_datetime_published-formatted]', $type = 'node', $node);
    $year   = substr($datetime , 0, 4);
    $token .= '&rft.date='.    urlencode($year);
    
    $token .= '&rft.volume='.  urlencode(token_replace('[field_volume-formatted]', $type = 'node', $node));
    $token .= '&rft.issue='.   urlencode(token_replace('[field_issue-formatted]', $type = 'node', $node));
    $token .= '&rft.spage='.   urlencode(token_replace('[field_start_page-formatted]', $type = 'node', $node)); //spage is causing an error on UVA resolver for some reason
    $token .= '&rft.epage='.   urlencode(token_replace('[field_end_page-formatted]', $type = 'node', $node));    
  }                                                                                                                   
  else { //default is article                                                                                         
    $token  = '&rft_val_fmt=info:ofi/fmt:kev:mtx:journal';                                                  
    $token .= '&rft.jtitle='.  urlencode(token_replace('[field_journal_name-formatted]', $type = 'node', $node));
    $token .= '&rft.genre=article'; 
    $token .= '&rft.issn='.    urlencode(token_replace('[field_issn-formatted]', $type = 'node', $node));
    
    $datetime = token_replace('[field_datetime_published-formatted]', $type = 'node', $node);
    $year   = substr($datetime , 0, 4);
    $token .= '&rft.date='.    urlencode($year);
    
    $token .= '&rft.volume='.  urlencode(token_replace('[field_volume-formatted]', $type = 'node', $node));
    $token .= '&rft.issue='.   urlencode(token_replace('[field_issue-formatted]', $type = 'node', $node));
    $token .= '&rft.spage='.   urlencode(token_replace('[field_start_page-formatted]', $type = 'node', $node));
    $token .= '&rft.epage='.   urlencode(token_replace('[field_end_page-formatted]', $type = 'node', $node));    
  }

  $authors = token_replace('[field_author-formatted]', $type = 'node', $node);
  $authors = explode(";", $authors);
  $author = explode(',', $authors[0]); //grab the first author
  $token .= '&rft.aulast='. urlencode(trim($author[0]));
  if ($author[1]) {
    $token .= '&rft.aufirst='. urlencode(trim($author[1]));
  }

  return 'url_ver=Z39.88-2004'. $token;
}


/**
* Map publications types to openurl types
* 
* @return string
*/
function _openurl_types($type) {
  switch($type) {
    case 'News Article';
      return 'article'; 
      
      case 'Book';
        return 'book';      
      case 'Journal Article';
        return 'article';
      case 'News/Magazine';                     
        return 'article';                         //a quick check of Academic Search Premier - Ebsco seems to use "article" for just about everything that isn't a book
      case 'Report/Government/Legal'; 
        return 'article';
      case 'Archival/Media/Teaching Material';
        return 'article';                              
  }
}


// /**
// * Accept a node id and return the node as a OpenURL Z39.88 2004 string
// * 
// * @return string
// */
// function theme_cite_openurl_2000($node) {  
// 
//   $pmid = token_replace('[field_pmid-formatted]', $type = 'node', $node);
//   $token .= ($pmid) ? sprintf('&id=pmid:%d', urlencode($pmid)) : NULL;
//   //Add DOI when we get them id=doi:123
//   
//   //we need "raw" b/c this is not a field set to display, but values set by admins, so raw is ok
//   $cck_type = token_replace('[field_type-raw]', $type = 'node', $node); 
//   $type = _openurl_types($cck_type);
//   //open url groups what we call types into two primary types with secondary types set as genres  
//   $token .= '&genre='. $type; 
//   //title = The title of a bundle (journal, book, conference)
//   //atitle = The title of an individual item (article, preprint, conference proceeding, part of a book )
//   if ($type != 'book') {
//     $token .= sprintf('&atitle=%s', urlencode(token_replace('[title]', $type = 'node', $node)));
//     $token .= sprintf('&title=%s', urlencode(token_replace('[field_journal_name-formatted]', $type = 'node', $node)));
//   }
//   else {
//     $token .= sprintf('&title=%s', urlencode(token_replace('[title]', $type = 'node', $node)));
//   }
//   
//   //TODO: make the following into a loop
//   $volume = token_replace('[field_volume-formatted]', $type = 'node', $node);
//   $token .= ($volume) ? sprintf('&volume=%s', urlencode($volume)) : NULL;
//   
//   $issue = token_replace('[field_issue-formatted]', $type = 'node', $node);
//   $token .= ($issue) ? sprintf('&issue=%s', urlencode($issue)) : NULL;
//   
//   $spage = token_replace('[field_start_page-formatted]', $type = 'node', $node);
//   $token .= ($spage) ? sprintf('&spage=%s', urlencode($spage)) : NULL;  
//   
//   $epage = token_replace('[field_end_page-formatted]', $type = 'node', $node);
//   $token .= ($epage) ? sprintf('&spage=%s', urlencode($epage)) : NULL;
//   
//   $issn = token_replace('[field_issn-formatted]', $type = 'node', $node);
//   $token .= ($issn) ? sprintf('&issn=%s', urlencode($issn)) : NULL;
//   
//   $isbn = token_replace('[field_issn-formatted]', $type = 'node', $node); 
//   $token .= ($isbn) ? sprintf('&issn=%s', urlencode($isbn)) : NULL;  
// 
// 
//   $datetime = token_replace('[field_datetime_published-formatted]', $type = 'node', $node);
//   $year   = substr($datetime , 0, 4);
//   $token .= ($year) ? sprintf('&date=%d', $year) : NULL;  
//   
//   $authors = token_replace('[field_author-formatted]', $type = 'node', $node);
//   $authors = explode(";", $authors);
//   $author  = explode(',', $authors[0]); //grab the first author
//   $token  .= ($author[0]) ? sprintf('&aulast=%s', urlencode(trim($author[0])) ): NULL;
//   $token  .= ($author[1]) ? sprintf('&aufirst=%s', urlencode(trim($author[1]))) : NULL;
//   
//   return 'sid=ethicshare'. $token;
// }


function cite_styles_export_link_resolver_openurl($nid) {
  $node = node_load($nid);
  return theme('cite_openurl_2000', $node);
}
