<?php
$output .= ($title)     ? sprintf('title  ="%s"', $title) ."\r" : NULL;
$output .= ($journal)   ? sprintf('journal ="%s"', $journal) ."\r" : NULL;
$output .= ($volume)    ? sprintf('volume ="%s"', $volume) ."\r" : NULL;
$output .= ($number)    ? sprintf('number ="%s"', $number) ."\r" : NULL;
$output .= ($pages)     ? sprintf('pages ="%s"', $pages) ."\r" : NULL;
$output .= ($issn)      ? sprintf('issn ="%s"', $issn) ."\r" : NULL;
$output .= ($isbn)      ? sprintf('isbn ="%s"', $isbn) ."\r" : NULL;
$output .= ($abstract)  ? sprintf('abstract ="%s"', $abstract) ."\r" : NULL;
if (count($authors)) {
  foreach ($authors as $author) {
    $refworks_authors .= sprintf('%s and ', $author);
  }
  $output .= sprintf('author ="%s"', substr($refworks_authors, 0, -5))  ." \r"; 
}
if (count($keywords)) {
  foreach ($keywords as $keyword) {
    $refworks_keywords .= sprintf('%s,', $keyword);  
  }
  $output .= sprintf('keywords ="%s"', substr($refworks_keywords, 0, -1)); 
}
printf("@%s{%d,\r%s\r} \r", $pub_type , $nid, $output) ." \r"; 
