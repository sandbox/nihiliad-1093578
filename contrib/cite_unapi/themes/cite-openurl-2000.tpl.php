<?php
$output .= ($pmid)    ? sprintf('&id=pmid:%d', $pmid): NULL;
$output .= ($genre)   ? sprintf('&genre=%s', $genre): NULL;
$output .= ($atitle)  ? sprintf('&atitle=%s', $atitle): NULL;
$output .= ($title)   ? sprintf('&title=%s', $title): NULL;
$output .= ($volume)  ? sprintf('&volume=%s', $volume): NULL;
$output .= ($issue)   ? sprintf('&issue=%s', $issue): NULL;
$output .= ($spage)   ? sprintf('&spage=%s', $spage): NULL;
$output .= ($epage)   ? sprintf('&epage=%s', $epage): NULL;
$output .= ($issn)    ? sprintf('&issn=%s', $issn): NULL;
$output .= ($isbn)    ? sprintf('&isbn=%s', $isbn): NULL;
$output .= ($date)    ? sprintf('&date=%s', $date): NULL;
$output .= ($aufirst) ? sprintf('&aufirst=%s', $aufirst): NULL;
$output .= ($aulast)  ? sprintf('&aulast=%s', $aulast): NULL;
print 'sid=ethicshare'. $output;