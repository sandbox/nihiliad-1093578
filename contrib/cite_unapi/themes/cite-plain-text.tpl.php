<?php
$output .= $title ."\n";
if ($authors) {
  foreach ($authors as $author) {
    $output .= $author .'; ';
  }
  $output = substr($output, 0, -2) ."\n";
}

$output .= sprintf("%s %s%s%s%s", $journal, $date, $volume, $issue, $pages);

print $output;