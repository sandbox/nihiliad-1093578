<?php
//see cite_ris.inc for variable definitions
$output .= ($TY) ? sprintf('TY  - %s', $TY) ."\r" : NULL;
$output .= ($ID) ? sprintf('ID  - %d', $ID) ."\r" : NULL;
$output .= ($T1) ? sprintf('T1  - %s', $T1) ."\r" : NULL;
$output .= ($JO) ? sprintf('JO  - %s', $JO) ."\r" : NULL;
$output .= ($VL) ? sprintf('VL  - %s', $VL) ."\r" : NULL;
$output .= ($IS) ? sprintf('IS  - %s', $IS) ."\r" : NULL;
$output .= ($SP) ? sprintf('SP  - %s', $SP) ."\r" : NULL;
$output .= ($EP) ? sprintf('EP  - %s', $EP) ."\r" : NULL;
$output .= ($SN) ? sprintf('SN  - %s', $SN) ."\r" : NULL;
$output .= ($N2) ? sprintf('N2  - %s', $N2) ."\r" : NULL;
$output .= ($PY) ? sprintf('PY  - %s', $PY) ."\r" : NULL;
if (count($A1s)) {
  foreach ($A1s as $A1) {
    $output .= sprintf('A1  - %s', $A1) ."\r";  
  }
}
if (count($K1s)) {
  foreach ($K1s as $K1) {
    $output .= sprintf('K1  - %s', $K1) ."\r";  
  }
}
$output .= 'ER  -';
print $output;