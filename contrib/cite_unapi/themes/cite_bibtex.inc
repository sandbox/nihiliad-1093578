<?php
function template_preprocess_cite_bibtex(&$variables) {
  $variables['nid']       = check_plain($variables['node']->nid);  
  $variables['title']     = check_plain($variables['node']->title);
  $variables['journal']   = check_plain($variables['node']->field_journal_name[0]['value']);
  $variables['volume']    = check_plain($variables['node']->field_volume[0]['value']);
  $variables['number']    = check_plain($variables['node']->field_issue[0]['value']);
  $variables['pages']     = sprintf('%s--%s', check_plain($variables['node']->field_start_page[0]['value']), check_plain($variables['node']->field_end_page[0]['value']));
  $variables['issn']      = check_plain($variables['node']->field_issn[0]['value']);
  $variables['isbn']      = check_plain($variables['node']->field_isbn[0]['value']);
  $variables['abstract']  = check_plain($variables['node']->field_abstract[0]['value']);
  $datetime = explode('-', $variables['node']->field_datetime_published[0]['value']);
  $variables['year'] = sprintf('%d', $datetime[0]);
  foreach ($variables['node']->field_author as $author) {
    $variables['authors'][] = trim(check_plain($author['value']));
  }
  foreach ($variables['node']->field_subject as $subject) {
    $variables['keywords'][] = trim(check_plain($subject['value']));
  }
  if ($ty = _cite_unapi_bibtex_types($variables['node']->field_type[0]['value'])) {
    $variables['TY'] = $ty;
  }
  else {
    watchdog('Cite unAPI', 'Unable to process request for this publication type: '. $variables['node']->field_type[0]['value'], WATCHDOG_NOTICE);
  }
}

/**
* Map publications types to BibTex types
* 
* @return string
*/
function _cite_unapi_bibtex_types($type) {
  switch($type) {
    case 'article';
      return 'article';
    case 'News Article';
      return 'article';
      
    case 'Book';
      return 'book';      
    case 'Journal Article';
      return 'article';
    case 'News/Magazine';                     
      return 'article';
    case 'Report/Government/Legal'; 
      return 'misc';
    case 'Archival/Media/Teaching Material';
      return 'misc';
  }
}