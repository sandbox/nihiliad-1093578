<?php

function template_preprocess_cite_ris(&$variables) {
  $variables['ID'] = $variables['node']->nid;
  $variables['T1'] = check_plain($variables['node']->title);
  $variables['JO'] = check_plain($variables['node']->field_journal_name[0]['value']);
  $variables['VL'] = check_plain($variables['node']->field_volume[0]['value']);
  $variables['IS'] = check_plain($variables['node']->field_issue[0]['value']);
  $variables['SP'] = check_plain($variables['node']->field_start_page[0]['value']);
  $variables['EP'] = check_plain($variables['node']->field_end_page[0]['value']);
  $variables['SN'] = ($variables['node']->field_issn[0]['value']) ? check_plain($variables['node']->field_issn[0]['value']) : check_plain($variables['node']->field_issn[0]['value']);
  $variables['N2'] = check_plain($variables['node']->field_abstract[0]['value']);
  $datetime = explode('-', $variables['node']->field_datetime_published[0]['value']);
  $day = substr($datetime[2], 0, 2);
  $variables['PY'] = sprintf('%d/%02d/%02d/', $datetime[0], $datetime[1], $day);
  foreach ($variables['node']->field_author as $author) {
    $variables['A1s'][] = trim(check_plain($author['value']));
  }
  foreach ($variables['node']->field_subject as $subject) {
    $variables['K1s'][] = trim(check_plain($subject['value']));
  }
  if ($ty = _cite_ris_types($variables['node']->field_type[0]['value'])) {
    $variables['TY'] = $ty;
  }
  else {
    watchdog('Cite unAPI', 'Unable to process request for this publication type: '. $variables['node']->field_type[0]['value'], WATCHDOG_NOTICE);
  }
}

/**
* Map publications types to RIS types
* 
* @return string
*/
function _cite_ris_types($type) {
  switch($type) {
    case 'article';
      return 'JOUR';
    case 'News Article';
      return 'JOUR';
    case 'Book';
      return 'BOOK';      
    case 'Journal Article';
      return 'JOUR';      
    case 'News/Magazine';             //really, there are two possible codes here: MGZN and NEWS
      return 'NEWS';
    case 'Report/Government/Legal'; 
      return 'GEN';                   //"Generic" for now
    case 'Archival/Media/Teaching Material';
      return 'GEN';                   //"Generic" for now
  }
}