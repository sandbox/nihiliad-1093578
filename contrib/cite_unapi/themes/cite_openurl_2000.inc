<?php

function template_preprocess_cite_openurl_2000(&$variables) {
  $variables['pmid'] = check_plain($variables['node']->field_pmid[0]['value']);
  //we need "raw" b/c this is not a field set to display, but values set by admins, so raw is ok
  $cck_type = check_plain($variables['node']->field_type[0]['value']); 
  $variables['genre'] = _cite_unapi_openurl_types($cck_type);
  //title = The title of a bundle (journal, book, conference)
  //atitle = The title of an individual item (article, preprint, conference proceeding, part of a book )
  if ($variables['genre'] != 'book') {
    $variables['atitle'] = urlencode(strip_tags($variables['node']->title));
    $variables['title']  = urlencode(strip_tags($variables['node']->field_journal_name[0]['value']));     
  }
  else {
    $variables['title'] = check_plain($variables['node']->title);         
  }
  //TODO: make the following into a loop
  $variables['volume'] = check_plain($variables['node']->field_volume[0]['value']);
  $variables['issue']  = check_plain($variables['node']->field_issue[0]['value']);
  $variables['spage']  = check_plain($variables['node']->field_start_page[0]['value']);
  $variables['epage']  = check_plain($variables['node']->field_end_page[0]['value']);
  $variables['issn']   = check_plain($variables['node']->field_issn[0]['value']);
  $variables['isbn']   = check_plain($variables['node']->field_issn[0]['value']); 
  $datetime = token_replace('[field_datetime_published-formatted]', $type = 'node', $node);
  $year   = substr($datetime , 0, 4);
  $variables['date'] = $year;
  $author  = explode(',', urlencode(strip_tags($variables['node']->field_author[0]['value']))); //grab the first author
  $variables['aulast']  = $author[0];
  $variables['aufirst'] = $author[1];  
}

/**
* Map publications types to openurl types
* 
* @return string
*/
function _cite_unapi_openurl_types($type) {
  switch($type) {
    case 'News Article';
      return 'article'; 
      
      case 'Book';
        return 'book';      
      case 'Journal Article';
        return 'article';
      case 'News/Magazine';                     
        return 'article';                         //a quick check of Academic Search Premier - Ebsco seems to use "article" for just about everything that isn't a book
      case 'Report/Government/Legal'; 
        return 'article';
      case 'Archival/Media/Teaching Material';
        return 'article';                              
  }
}