<?php

function template_preprocess_cite_plain_text(&$variables) {
  $variables['title'] = check_plain($variables['node']->title);
  $variables['journal'] = check_plain($variables['node']->field_journal_name[0]['value']);
  $variables['volume'] = check_plain($variables['node']->field_volume[0]['value']);
  $variables['issue'] = ($variables['node']->field_issue[0]['value']) ? sprintf('(%s)', check_plain($variables['node']->field_issue[0]['value'])) : NULL;
  $start_page = check_plain($variables['node']->field_start_page[0]['value']);
  $end_page = check_plain($variables['node']->field_end_page[0]['value']);
  $variables['issn'] = ($variables['node']->field_issn[0]['value']) ? check_plain($variables['node']->field_issn[0]['value']) : check_plain($variables['node']->field_issn[0]['value']);
  // $variables['N2'] = check_plain($variables['node']->field_abstract[0]['value']);
  $datetime = explode('-', $variables['node']->field_datetime_published[0]['value']);
  $date = sprintf('%d/%02d/%02d/', $datetime[0], $datetime[1], $datetime[2], 0, 2);
  
  $timestamp = strtotime($date);
  $year = $datetime[0];
  $month = date("M", $timestamp);
  $day = (int) substr($datetime[2], 0, 2);
  $day = ($day > 0) ? $day .';' : NULL;
  $variables['date'] = sprintf('%d %s %s', $datetime[0], $month, $day);

  if (($variables['issue'] || $variables['volume']) && $start_page) {
    $start_page = ':'. $start_page;
  }
  
  if (($variables['issue'] || $variables['volume']) && (!$start_page && $end_page)) {
    $end_page = ':'. $end_page;
  }
  
  $variables['pages'] = ($start_page && $end_page) ? sprintf('%s-%s', $start_page, $end_page) : sprintf('%s %s', $start_page, $end_page);
  
  
  if ($variables['node']->field_author) {
    foreach ($variables['node']->field_author as $author) {
      $variables['authors'][] = trim(check_plain($author['value']));
    }
  }
  if ($variables['node']->field_subject) {
    foreach ($variables['node']->field_subject as $subject) {
      $variables['subjects'][] = trim(check_plain($subject['value']));
    }
  }

}