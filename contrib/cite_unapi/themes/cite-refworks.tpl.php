<?php
//see cite_refworks.inc for variable definitions
$output .= ($RT)    ? sprintf('RT %s', $RT) ."\r" : NULL;
$output .= ($ID)    ? sprintf('ID %d', $ID) ."\r" : NULL;
$output .= ($T1)    ? sprintf('T1 %s', $T1) ."\r" : NULL;
$output .= ($JF)    ? sprintf('JF %s', $JF) ."\r" : NULL;
$output .= ($VO)    ? sprintf('VO %s', $VO) ."\r" : NULL;
$output .= ($IS)    ? sprintf('IS %s', $IS) ."\r" : NULL;
$output .= ($SP)    ? sprintf('SP %s', $SP) ."\r" : NULL;
$output .= ($OP)    ? sprintf('OP %s', $OP) ."\r" : NULL;
$output .= ($SN_sn) ? sprintf('SN %s', $SN_sn) ."\r" : NULL;
$output .= ($SN_bn) ? sprintf('SN %s', $SN_bn) ."\r" : NULL;
$output .= ($AB)    ? sprintf('AB %s', $AB) ."\r" : NULL;
if (count($A1s)) {
  foreach ($A1s as $A1) {
    $output .= sprintf('A1 %s', $A1) ."\r";  
  }
}
if (count($K1s)) {
  foreach ($K1s as $K1) {
    $output .= sprintf('K1 %s', $K1) ."\r";  
  }
}
print $output;