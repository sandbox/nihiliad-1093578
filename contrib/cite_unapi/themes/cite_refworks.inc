<?php
function template_preprocess_cite_refworks(&$variables) {
  $variables['ID'] = $variables['node']->nid;
  $variables['T1'] = check_plain($variables['node']->title);
  $variables['JF'] = check_plain($variables['node']->field_journal_name[0]['value']);
  $variables['VO'] = check_plain($variables['node']->field_volume[0]['value']);
  $variables['IS'] = check_plain($variables['node']->field_issue[0]['value']);
  $variables['SP'] = check_plain($variables['node']->field_start_page[0]['value']);
  $variables['OP'] = check_plain($variables['node']->field_end_page[0]['value']);
  $variables['SN_sn'] = check_plain($variables['node']->field_issn[0]['value']);
  $variables['SN_bn'] = check_plain($variables['node']->field_isbn[0]['value']);
  $variables['AB'] = check_plain($variables['node']->field_abstract[0]['value']);
  $datetime = explode('-', $variables['node']->field_datetime_published[0]['value']);
  $variables['YR'] = sprintf('%d', $datetime[0]);
  foreach ($variables['node']->field_author as $author) {
    $variables['A1s'][] = check_plain($author['value']);
  }
  foreach ($variables['node']->field_subject as $subject) {
    $variables['K1s'][] = check_plain($subject['value']);
  }
  if ($RT = _cite_unapi_refworks_types($variables['node']->field_type[0]['value'])) {
    $variables['RT'] = $RT;
  }
  else {
    watchdog('Cite unAPI', 'Unable to process request for this publication type: '. $variables['node']->field_type[0]['value'], WATCHDOG_NOTICE);
  }
}

/**
* Map publications types to RIS types
* 
* @return string
*/
function _cite_unapi_refworks_types($type) {
  switch($type) {
    case 'article';
      return 'Journal Article';
    case 'News Article';
      return 'Newspaper Article';
      

    case 'Book';
      return 'Book, Whole';    
    case 'Journal Article';
      return 'Journal Article';          
    case 'News/Magazine';                     //Two possible codes here: Magazine Article and Newspaper Article
      return 'Newspaper Article';
    case 'Report/Government/Legal'; 
      return 'Generic';                       //"Generic" for now Possibilities: Bills/Resolutions, Case/Court Decisions, Report, Patent
    case 'Archival/Media/Teaching Material';
      return 'Generic';                       //"Generic" for now - Unpublished Material, Video/DVD, Sound Recording, Motion Picture
  }
}