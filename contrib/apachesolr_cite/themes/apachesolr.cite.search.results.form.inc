<?php
/**
 * Process variables for apachesolr-cite-search-results-form.tpl.php.
 *
 * The $variables array contains the following arguments:
 * - $results
 * - $type
 *
 * @see apachesolr-cite-search-results-form.tpl.php
 */
function template_preprocess_apachesolr_cite_search_results_form(&$variables) {
  $response = apachesolr_static_response_cache();
  $query = apachesolr_current_query();
  
  if (apachesolr_has_searched() && ($response)) {
   if ($suggestions = get_object_vars($response->spellcheck->suggestions)) {
     //Get the original query and replace words.
     foreach ($suggestions as $word => $value) {
       $replacements[$word] = $value->suggestion[0];
     }
     $new_query = clone $query;
     $new_keyword = strtr($query->get_query_basic(), $replacements);
     $variables['suggestion'] = sprintf('<div class="spelling-suggestions">%s "%s"?</div>', t('Did you mean'), l($new_keyword, 'publications/' . $new_keyword));
   }

    $variables['filter_breadcrumb'] = apachesolr_cite_breadcrumb($query, $_GET);
  }
  

  if (apachesolr_has_searched()) {
    
    $keys  = apachesolr_cite_keys($_REQUEST);    
    $keys_extra = apachesolr_pretty_advanced_params('pretty', $_GET);
    $keys = ($keys_extra && $keys) ? $keys .' AND '. $keys_extra : $keys;
    $keys = ($keys_extra && !$keys) ? $keys .' '. $keys_extra : $keys;
    $searched_on = $keys;

    $variables['count_text']    = format_plural($response->response->numFound, '1 Item Found', '@count Items Found');
    $rows_to_show               = theme('apachesolr_cite_rows_to_show', $query->get_query_basic(), $query, $response) . '<div class="clear"></div>';
    $variables['to_show']       = ($response->response->numFound > 10) ? sprintf('<div class="to-show">view per page %s</div>', $rows_to_show) : '';
    $variables['sort']          = ($response->response->numFound) ? apachesolr_cite_get_sort() : NULL;
    $variables['found_results'] = ($response->response->numFound) ? TRUE : FALSE;
    
    $searched_on = str_replace('*:*', 'All Records (*:*)', $searched_on);    
    if ($variables['found_results'])  {
      drupal_set_title('Results for: '.  $searched_on);
    }
    else {
      $variables['searched_on'] = $searched_on;
    }
    
    
    global $user;
    if ($user->uid > 0) {
      $target_users_roles = array(
        ACTIVITY_ALL => 'all',
        $user->uid => 'author',
      );
      if ($_SESSION['apachesolr_cite_last']['keys']) {
        $data['search-string-link'] = l($searched_on, 'publications/'. $_SESSION['apachesolr_cite_last']['keys'], array('query' => $_SESSION['apachesolr_cite_last']['query']));
        activity_insert($user->uid, 'apachesolr_cite', 'search', 'insert', $data, $target_users_roles);
      }
    }
    
    // $variables['output']      .= sprintf('<div id="search-header">%s <div class="clear"></div>%s <div class="search-sort">%s</div></div><div class="clear"></div>', $count_text, $to_show, );
  }


  // $variables['search_header']      = theme('apachesolr_cite_search_header');
  $variables['form_submit']        = drupal_render($variables['form']['submit']);
  $variables['form_op']            = drupal_render($variables['form']['op']);
  $variables['form_destination']   = drupal_render($variables['form']['destination']);
  $variables['form_checkall']      = drupal_render($variables['form']['checkall']);
  $variables['folder']             = drupal_render($variables['form']['folder_nid']);
  $variables['export_dropdown']    = drupal_render($variables['form']['export_dropdown']);
  $variables['export_submit']      = drupal_render($variables['form']['export_submit']);
  $variables['form']               = drupal_render($variables['form']);
}

function apachesolr_cite_breadcrumb(&$query, $get_arr) {
  //add facet breadcrumb
  $filters = array();
  $facet_fields = array('Subject' => 'sm_cck_field_subject', 'Author' => 'sm_cck_field_author', 'Publication Type' => 'ss_cck_field_type', 
  'Publication Subtype' => 'sm_cck_field_subtype', 'Year Published' => 'ss_cck_field_year_published', 'Journal Name' => 'ss_cck_field_journal_name',
  'User Keyword' => 'tid', 'Language' => 'ss_cck_field_language'
  );
  $filter = array();
  $crumbs = array();
  foreach ($facet_fields as $label => $facet_field) {
    $filter = $query->filter_extract($get_arr['filters'], $facet_field);
    if (!empty($filter)) {
      $filter[0]['#name'] = $facet_field;
      $filter_queries[$query->make_filter($filter[0])] = $query->make_filter($filter[0]);
      if ($facet_field == 'tid') {
        $term = taxonomy_get_term($filter[0]['#value']);
        $filter_text = $term->name;
      }
      else {
        $filter_text = html_entity_decode($filter[0]['#value']);
      }
      //shouldn't this use apachesolr's display_callback?
      if ($facet_field == 'ss_cck_field_language') {
        $filter_text = cite_language_map($filter_text);
      }
      
      
      $crumbs[] = array('text' => $filter_text .' ('. $label .')', 'filter' => $query->make_filter($filter[0]));        
    }
  }
  
  if (!empty($crumbs)) {
    foreach ($crumbs as $crumb) {
      //link to all filters exept this breadcrumb item's own filter
      $queries = array_diff($filter_queries, array($crumb['filter'] => $crumb['filter']));
      $queries += array_diff($get_arr, array('rows' => $rows, 'q' => $get_arr['q'] ,'filters' => $get_arr['filters']));      
      $options = array('query' => 'filters=' . implode(' ', $queries) .'&'. apachesolr_cite_get_querystring($get_arr, array('filters', 'q')));
      $filters[] = l($crumb['text'], $query->get_path(), $options);
    }
  }
  
  return (!empty($filters)) ? sprintf('<div id="filter-breadcrumb">%s</div>', theme('item_list', $filters)) : NULL;
}


function apachesolr_cite_get_sort() {
  // Get the query and response. Without these no blocks make sense.
  $response = apachesolr_static_response_cache();
  if (empty($response) || ($response->response->numFound < 2)) {
    return;
  }

  $query = apachesolr_current_query();
  $sorts = $query->get_available_sorts();

  // Get the current sort as an array.
  $solrsort = $query->get_solrsort();
  $sort_links = array();
  $path = $query->get_path();
  $new_query = clone $query;
  $toggle = array('asc' => 'desc', 'desc' => 'asc');
  
  foreach ($sorts as $name => $sort) {
    $active = $solrsort['#name'] == $name;
    $direction = '';
    $new_direction = $sort['default'];
    if ($name == 'score') {
      // We only sort by ascending score.
      $new_direction = 'asc';
    }
    elseif ($active) {
      $direction = $solrsort['#direction'];
      $new_direction = $toggle[$solrsort['#direction']];
    }
    $new_query->set_solrsort($name, $new_direction);
    $sort_links[$name] = array(
      'title' => $sort['title'],
      'path' => $path,
      'options' => array('query' => $new_query->get_url_queryvalues()),
      'active' => $active,
      'direction' => $direction,
    );
  }
  

  // Allow other modules to add or remove sorts.
  drupal_alter('apachesolr_sort_links', $sort_links);
  
  //hack: another step we need to take to get around
  //the fact that the sort array is keyed on field name
  $active_sort = explode(' ', $_GET['solrsort']);
  foreach ($sort_links as $name => $link) {
    if ($active_sort) {
      $link['active'] = ($name == $active_sort[0]  .'_'.  $active_sort[1]) ? TRUE : FALSE;
    }
    
    //and now we have to remove the _asc and _desc suffixes that make the array keys unique
    $link['options']['query']['solrsort'] = str_replace(array('_asc', '_desc'), '',  $link['options']['query']['solrsort']);
    $themed_links .= theme('apachesolr_sort_link', $link['title'], $link['path'], $link['options'], $link['active'], $link['direction']);
  }
  
return '
  <div class="sort">
  <select name="sortitselect" class="sort-dropdown">'
  . $themed_links .
  '</select>
  <label for="sortit">Sort by:</label>   
  </div>';
}
