<?php
// $Id:Exp $

/**
 * @file apachesolr-cite-basic-search-box.tpl.php
 *
 * Available variables:
 *
 *
 * @see template_preprocess_apachesolr_cite_search_box_basic()
 */
  // $include_path = drupal_get_path('module', 'apachesolr_cite');
  // drupal_add_js($include_path .'/apachesolr_cite_tabs.js');
  //search form toggle
  if ($_GET['sft'] == 'keyword') {
    $default_style = '
      <style type="text/css">
      <!--
      .advanced-search-box {
      	display: none;
      }
      .keyword-search-box, div.keyword-link {
      	display: none;
    	}
    	
    	#header-bottom-last {
    	  display: none;
    	}
    	
    	-->
      </style>';
  }
  elseif ($_GET['sc'] == 'advanced') {
    $default_style = '
      <style type="text/css">
      <!--    
      #search-box-basic, div.advanced-link {
      	display: none;
    	}
    	 
    	#header-bottom-last {
    	  display: block;
    	}
    	
    	-->
      </style>';
  }
  else {
    $default_style = '
      <style type="text/css">
      <!--    
      .advanced-search-box {
      	display: none;
      }  
      .keyword-search-box, div.keyword-link {
      	display: none;
    	}
    	-->
      </style>';
  }
  
  $search_box = $default_style;
  // $sources_link = l(t('Sources'), 'about/collection', array('attributes' => array('class' => 'sources-link'), 'title' => 'Learn more about what sources are contained in the EthicShare collection.'));
  $link = sprintf('%s <div class="advanced-link"><a class="advanced-link" href="#advanced"><span>Open Advanced Search</span></a></div>',
    $sources_link);
  $toggle_link_basic    = sprintf('<div class="search-toggle-basic"><div>%s</div></div>', $link);

  $link = sprintf('<div class="keyword-link"><a class="keyword-link" href="#advanced"><span>Close Advanced Search</span></a><div class="advanced-search-divider"></div> </div>',
    $sources_link);  
  $toggle_link_advanced = sprintf('<div class="search-toggle-advanced"><div>%s</div></div>', $link);
  
  $basic_search_form = sprintf('<div class="basic-search-box"><div>%s</div></div>', drupal_get_form('apachesolr_cite_search_keyword_form', $keys, $values));
  
  $search_box .= sprintf('<div class="search-hydra-container-basic"><div id="search-box-basic">%s</div></div>%s %s', 
  $basic_search_form , $toggle_link_basic, $toggle_link_advanced);
  
  print $search_box;
