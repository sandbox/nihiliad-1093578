<?php

print $inline_style_toggle; //used to set default to display advanced search form or not

$output .= '<div id="advanced-form">';
$output .= '<div id="fields-container">';
$output .= '<div class="fields-left">
          <table border="0"><tbody><tr><td>';
$output .= drupal_render($form['citation']['keys']);
$output .= '</td></tr><tr><td>';
$output .= drupal_render($form['citation']['title']);
$output .= '</td></tr><tr><td>';
$output .= '<label for="edit-author" class="overlabel author-advanced">Last Name, First Name</label>';	
$output .= drupal_render($form['citation']['author']);
$output .= '</td></tr><tr><td>';
$output .= '</td></tr></tbody></table>';
$output .= '</div><!-- /fields-left -->';
$output .= '<div class="fields-right">';

$output .= '<table border="0"><tbody><tr><td>';
$output .= '<label for="edit-subject" class="overlabel subject-advanced">ex: Stem Cell AND Humans AND United States</label>';	  
$output .= drupal_render($form['citation']['subject']);
$output .= '</td></tr><tr><td>';
$output .= drupal_render($form['citation']['journal']);
$output .= '</td></tr><tr><td>';
	$output .= '<table class="cite_numbers"><tr><td>';
	$output .= drupal_render($form['citation']['volume']);
	$output .= '&nbsp;</td><td>';
	$output .= drupal_render($form['citation']['issue']);
	$output .= '&nbsp;</td><td>';	
	$output .= '<label for="edit-year" class="overlabel year-advanced">YYYY-YYYY</label>';		
	$output .= drupal_render($form['citation']['year']);	
	$output .= '</td></tr></table>';
$output .= '</td></tr></tbody></table></div>';
$output .= '</div><!-- /fields-right -->';
$output .=  drupal_render($form['citation']['submit']);  
$output .= '</div><!-- /fields-container -->';
//search form toggle
$output .= drupal_render($form['sft']);

$output = $form['citation']['#children'] = $output;
print drupal_render($form);