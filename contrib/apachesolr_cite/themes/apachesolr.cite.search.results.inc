<?php

/**
 * Process variables for apachesolr-cite-search-results.tpl.php.
 *
 * The $variables array contains the following arguments:
 * - $results
 * - $type
 *
 * @see apachesolr-cite-search-results.tpl.php
 */
function template_preprocess_apachesolr_cite_search_results(&$variables) {
  modalframe_parent_js();

  $variables['search_results'] = drupal_get_form('apachesolr_cite_search_results_form', $variables['results']);
  $variables['pager'] = theme('pager', NULL, 10, 0);
  // To Provide alternate search results template, do something like:
  // $variables['template_files'][] = 'apachesolr-cite-search-results-'. $variables['type'];
}

function apachesolr_cite_search_results_form($form, $results) {
  drupal_add_js(drupal_get_path('module', 'og_folders') .'/includes/og_folders.js', $type = 'module', $scope = 'header', $defer = TRUE);  
  $form = array();
  if (count($results) >= 1) {
    $form += og_folders_save_to_folder_form();
    $form['submit']['#attributes'] = array('title' => 'Save checked citations from this page to a folder.', 'class' => 'folder-search-submit');
    $form += og_folders_export_form();
    $form['checkall'] = array(
      '#type' => 'checkbox',
      '#value' => 0,
      '#weight' => -10,
      );
  }

  $rows = ($_GET['rows']) ? $_GET['rows'] : 10;
  $page = ($_GET['page']) ? $_GET['page'] : 0;
  $item_number = ($rows * $page) + 1;
  foreach ($results as $result) {
    solr_to_cck_field($result);
    $nid = $result['node']->nid;

    //google books preview
    if ($result['node']->field_type == 'Book') {
      $cite_isbn = preg_replace('/([^0-9\s])/', '', $result['node']->field_isbn); //remove all non-number characters and take only the first ten
      if ($cite_isbn) {
        $isbns[] = 'ISBN:'. $cite_isbn;
      }
    }
    if (!$options[$nid]) {
      $options[$nid] = sprintf('<div class="citation-right">%s</div>', theme('apachesolr_cite_search_result_'. $result['node']->type, $result, $item_number));
      $item_number++;
    }

  }
    
  //look for all isbns in google book search
  // google_book_preview_js($isbns);
  
  $form['citation_checkboxes'] = array(
    '#type'     => 'checkboxes',
    '#options'  => $options,
    '#theme'    => 'apachesolr_cite_search_checkboxes'
    );
  
  $form['og_folders_cache_namespace'] = array('#value' => 'apachesolr_cite_search_cache', '#type' => 'hidden');
  return $form;
}


/**
 * Theme checkboxes such that input and title are in separate columns
 * See: http://drupal.org/node/197578
 *
 *
 * @ingroup themable 
 */
 
function theme_apachesolr_cite_search_checkboxes($form) {
  $options = $form['#options'];
  // if (is_array($options)) {
    $nids = array_keys($options);
    if ($nids) {
      foreach ($nids as $nid) {
        $citations .= theme('apachesolr_cite_search_checkbox', $form[$nid]);
      }
    }
    return '<ul id="search-results-list">'. $citations .'</ul>';
  // }
}

function theme_apachesolr_cite_search_checkbox($form) {
  static $first_themed;
  $input_form .= '<input type="checkbox" name="nids[]" class="search-result-checkbox checkall" id="edit-objects-'.$form['#id'].'" value="'.$form['#return_value'].'" ';
  // $folders_cache = og_folders_factory('OgFolderCache');  
  // $input_form .= ($folders_cache->item_cached($form['#array_parents'][1], 'apachesolr_cite_search_cache')) ? ' checked="checked" ' : ' ';
  $input_form .= drupal_attributes($form['#attributes']). ' />';
  $checkbox = sprintf('<td valign="top" width="5px"><div class="cite-checkbox">%s</div></td>', $input_form);
  if (!is_null($form['#title'])) {
      $checkbox .= '<td><label for="'.$form['#id'].'">'.$form['#title'].'</label></td>';
  }
  unset($form['#title']);
  $first = ($first_themed != TRUE) ? 'first' : FALSE;
  $first_themed = TRUE;
  return sprintf('<li class="search-result-list-item"><div class="%s citation"><table><tr>%s</tr></table></div></li>', $first, $checkbox) . "\n";
}


/**
 * Helper function to remove prefixes used in solr (ex "ss_cck_")
 *
 */
function solr_to_cck_field(&$result) {
  drupal_alter('apachesolr_cck_fields', $mappings);
  foreach ($mappings['per-field'] as $field => $field_params) {
    $check_solr = str_replace('field_', 'cck_field_', apachesolr_index_key($field_params));
    if ($result['node']->$check_solr) {
      $cck_field_name = substr($check_solr, 7);
      $result['node']->$cck_field_name = $result['node']->$check_solr;
      $result['node']->$check_solr = '';
      unset($result['node']->$check_solr);
    }
  } 
}


function theme_apachesolr_cite_search_result_cite($result, $item_number = NULL) {
  global $base_url, $user;
  //Solr barfs when we try index the slash in News/Magazine, so we index that value in a solr safe way.
  $pub_type = ($result['node']->field_type == 'News---Magazine') ? 'News/Magazine' : $result['node']->field_type;
  $title = 'Access the full text from your libraries at your institution.  You will be navigating away from EthicShare.';
  $alt = 'Access the full text from your libraries at your institution.  You will be navigating away from EthicShare.';  
  $link_attr =  array('class' => 'findit-link', 'target' => '_new');
  if ($pub_type != 'News Article') {
    $findit = theme('link_resolver', $result['node']->nid, $alt, $title, NULL, $link_attr);    
  }
  else {
    if ($result['node']->field_url_0) {  
      $findit_image = theme('image', '/sites/all/modules/link_resolver/images/find_in_a_library.png', t('Find In a Library'));
      $findit =  l($findit_image, $result['node']->field_url_0,  
        array(
          'attributes' => 
            array(
              'class' => 'findit-link', 
              'target' => '_blank', 
              'title' => 'Access the full text from your libraries at your institution.  You will be navigating away from EthicShare.'
              ), 
          'html' => TRUE)
          );
      }
  } 

  $keys = str_replace(SEARCH_CONTEXT .'/', '', $_GET['q']);

  //google books search
  $cite_isbn = preg_replace('/([^0-9\s])/', '', $result['node']->field_isbn); //remove all non-number characters and take only the first ten
  if ($cite_isbn) {
    $isbns = explode(' ', $cite_isbn);
    $isbn = trim(substr($isbns[0], 13));
    $findit .= '<div class="google-book-preview" id="ISBN-'. $isbns[0] .'"></div>';
  }     
  
  if ($result['node']->field_author) {
    if (is_array($result['node']->field_author)) {
      foreach ($result['node']->field_author as $author) {
        $authors .= l($author,  'publications', array('query' => '&author='. $author .'&sc=advanced')) .'; ';    
      }
    }
    else{
      $authors_arr = explode(';', $result['node']->field_author);
      foreach ($authors_arr as $author) {
        $authors .= l($author,  'publications', array('query' => 'author='. $author .'&sc=advanced#advanced')) .'; ';
      }
    }
    $cite .= '<div class="cite-authors">';
    $cite .= substr($authors, 0, -2);
    $cite .= '</div><div class="clear"></div>';
  }
  elseif ($result['node']->field_source_status == 'In Process' && $result['node']->field_pmid) {
    $cite .= '<div class="cite-authors">[No authors listed]</div><div class="clear"></div>';
  }


  // if ($result['node']->field_type) {
  //   $cite .= sprintf('<div class="cite-type">(%s)</div> ', str_replace('---', '/', $result['node']->field_type));
  // }      
// 
  $first_item = true;

  if ($result['node']->field_datetime_published && $pub_type == 'News/Magazine') {
    $year   = substr($result['node']->field_datetime_published, 0, 4);
    $month  = (int) substr($result['node']->field_datetime_published, 5, 2);
    $month  = ($month != 0) ? $month .'/': NULL;
    $day    = (int)  substr($result['node']->field_datetime_published, 8, 2);
    $day    = ($day != 0) ? $day .'/': NULL;    
    $cite  .= sprintf('<div class="cite-date-published">%s%s%s</div><div class="clear"></div>', $month, $day, $year);
  }

  if ($result['node']->field_journal_name) {
    $cite .= sprintf(' <div class="cite-journal">%s.</div>', $result['node']->field_journal_name);
  }

  if ($result['node']->field_publisher_location) {  
    $cite .= sprintf(' <div class="cite-publisher-location">%s:</div>', $result['node']->field_publisher_location);
  }

  if ($result['node']->field_publisher) {  
    $cite .= sprintf(' <div class="cite-publisher">%s</div>', $result['node']->field_publisher);
  }

  if ($result['node']->field_issue_chronology) {
    $cite .= sprintf('<div class="cite-datetime">%s&nbsp;</div>', $result['node']->field_issue_chronology);    
  }

  if ($result['node']->field_volume && !$result['node']->field_issue) {
    $cite_end .= sprintf('<div class="cite-volume">%s:</div>',  $result['node']->field_volume .' ');
  }
  elseif ($result['node']->field_volume) {
    $cite_end .= sprintf('<div class="cite-volume">%s</div>',  $result['node']->field_volume .' ');
  }

  if ($result['node']->field_issue) {
    $cite_end .= sprintf('<div class="cite-issue">(%s):</div>', $result['node']->field_issue);
  }

  //$pagination
  if ($result["node"]->field_pagination) {
    $cite_end .= sprintf('<div class="cite-pagination">%s;</div>', check_plain($result["node"]->field_pagination));
  }
  
  
  //well, yeah, this conditional is kind of out of hand.
  if ($result['node']->field_datetime_published && !$result['node']->field_issue_chronology && $pub_type != 'News/Magazine') {
    $year   = substr($result['node']->field_datetime_published, 0, 4);
    $month  = (int) substr($result['node']->field_datetime_published, 5, 2);
    $day    = (int)  substr($result['node']->field_datetime_published, 8, 2);
    if ($month == 0 || $result['node']->field_type == 'Book') {
      $cite_end .= sprintf('<div class="cite-year">&nbsp;%s</div>', $year);
    }
    else {
      $months = array(' ', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec');

      //for solr, we can't have zero months or days, so we record a precision
      //level at the time of indexing
      $precision = $result['node']->ss_derived_field_datetime_precision;
      if ($result['node']->nid == 713827) {
       var_dump($result['node']->ss_derived_field_datetime_precision);        
      }
      if ($precision) {
        if ($precision == 'year') {
          $cite_end .= sprintf('<div class="cite-datetime">&nbsp;%d.</div>', $year);              
        }
        elseif ($precision == 'month') {
          $cite_end .= sprintf('<div class="cite-datetime">&nbsp;%d %s.</div>', $year, $months[$month]);              
        }
        else {
          $cite_end .= sprintf('<div class="cite-datetime">&nbsp;%d %s %d.</div>', $year, $months[$month], $day);
        }
      }
      elseif ($day == 0 && $month == 0) {
        $cite_end .= sprintf('<div class="cite-datetime">&nbsp;%d.</div>', $year);              
      }
      elseif ($day == 0 && $month != 0){
        $cite_end .= sprintf('<div class="cite-datetime">&nbsp;%d %s.</div>', $year, $months[$month]);        
      }
      else {
        $cite_end .= sprintf('<div class="cite-datetime">&nbsp;%d %s %d.</div>', $year, $months[$month], $day);
      }
    }
  }

  if ($result['node']->field_language) {
    $lang = cite_language_map($result['node']->field_language);
    if ($lang) {
      $cite_end .= sprintf(' <div class="cite-language"> (%s)</div>', $lang);
    }
  }

  $source = cite_source_get_link($result['node']->nid);    
  if ($source) {
    $cite_end .= sprintf(' <div class="cite-source"> [Record Source: %s]</div>', $source);
  }    


  if ($cite_end) {
    $cite = sprintf('%s %s', $cite, $cite_end);
  }
  else {
    $cite = sprintf('%s', $cite);
  }

  if ($result['node']->field_source_status && $result['node']->field_source_status != 'Completed') {
    $cite .= sprintf(' <div class="cite-source-status"> [%s]</div>', $result['node']->field_source_status);
  }

  $cite .= '<div class="clear"></div>';


  //show the whole she-bang for news feeds pages.
  if (arg(0) == 'feeds') {
    $cite .=  '<div class="clear"></div>';
    $cite .= $result['node']->field_abstract .'.';
  }


  if ($result['node']->added) {
    $cite .= sprintf('<div class="list-node-added">Added to list: %s</div>', $result['node']->added); //datestamp of when item was added to a list
  }

  
  
  if (is_array($result['node']->field_subject)) {
    $subjects = '';
    foreach ($result['node']->field_subject as $subject) {
      $subject = l(check_plain($subject), $base_url. '/publications/?subject='. check_plain($subject) .'&sc=advanced');
      $subjects .= sprintf('<li>%s;</li>', $subject);
    }
    $cite .= sprintf('
      <div class="cite-database-keywords-box">
        <label>Database Keywords:</label>
        <div class="cite-database-keywords"><ul>%s</ul></div>
        <div class="cite-database-keywords-expanded"><ul>%s</ul></div>
        <div class="clear"></div>
      </div>', 
      $subjects, $subjects);
    $subjects ='';      
  }
  elseif ($result['node']->field_subject != null) {
    $subject_text = check_plain($result['node']->field_subject);
    $subject = l(check_plain($subject_text), $base_url. '/publications/?subject='. $subject_text .'&sc=advanced');
    $cite .= sprintf('
      <div class="cite-database-keywords-box">
        <label>Database Keyword:</label>
        <div class="cite-database-keywords"><ul>%s</ul></div>
        <div class="clear"></div>
      </div>', 
      $subject);
    $subjects ='';
  }
  
  $item_number = ($item_number) ? $item_number .'.' : NULL;
  $row = sprintf('<tr>%s<td valign="top"><div class="citation-left"><div class="result-title">%s %s</div>%s</div></td><td valign="top" align="right">%s</td></tr>',
    ($data['record_number']) ? '<td valign="top" width="15px"><div class="result-number"><span>'. $data['record_number'] .'.</span></div></td>' : '', 
    $item_number, l($result["title"], 'node/'. $result['node']->nid, $attributes = array(), $query = NULL, $fragment = NULL, FALSE, TRUE),
    $cite,
    $findit
  );


  $output .= sprintf('<table class="citation-inner">%s</table>', $row);
  $unapi_url = url('unapi/'. $result['node']->nid, array('absolute' => TRUE));
  $output .= sprintf('<abbr class="unapi-id" title="%s"> </abbr>', $result['node']->nid);
  
  //DISABLED - not getting much use and take up lots of space, leaving here for posterity
  // Tags/keywords -- should anything appear when user isn't logged in?
  // if ($user->uid >= 1) {
  //   $community_tags = str_replace('taxonomy/term', 'tagged/everyone', community_tags_node_view($result['node'], TRUE));      
  //   $community_tags = ($node->delta == 0) ? sprintf('</form><form id="community-tags-form" style="margin: 10px 0 10px;">%s</form>', $community_tags) : $community_tags;      
  //   $output .= '<div class="clear"></div>'. $community_tags .'<div class="clear"></div>';
  // }
  // else {
  //   $message .= t('You must be !logged_in to contribute and use user supplied keywords.', array('!logged_in' => l('logged in', 'user', array('query' => 'destination=publications'))));
  //   $output .= sprintf('<div class="clear"></div><div class="cite-subjects"><h3>User Supplied Keywords:</h3>%s</div>', $message);
  // }
  
  return sprintf('<div class="citation-data">%s</div>', $output);
}