<?php

class ApachesolrCite {
  public static $search_paths = array();
  public static $keys = '';
  
  public function set_base_search_path() {    
    if(empty(self::$search_paths)) {
      $path = variable_get('apachesolr_cite_base_search_path', 'cite/search');
      self::$search_paths = array($path);
    }

    return self::$search_paths;
  }  
  
  public function get_base_search_paths() {
    return self::set_base_search_path();
  }
}

?>