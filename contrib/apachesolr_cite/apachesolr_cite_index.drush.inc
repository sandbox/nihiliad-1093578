<?php

/**
 * Implementation of hook_drush_command().
 */
function apachesolr_cite_index_drush_command() {
  $items['cite_index'] = array(
    'callback' => '_apachesolr_cite_index_citations',
    'description' => 'Indexes all nodes in the apachesolr_search_nodess table',
    'options' => apachesolr_cite_index_get_options()
  );
  
  $items['cite_index_remove_noncite'] = array(
    'callback' => '_cite_index_remove_noncite',
    'description' => 'Removes non-citations from the indexing cue',
    'options' => apachesolr_cite_index_get_options()
  );  
  
  return $items;
}

function apachesolr_cite_index_drush_help($section) {
  switch ($section) {
    case 'drush:cite_index':
      return dt("Index Citation Nodes Command.");
    case 'drush:cite_index_remove_noncite':
      return dt("Removes non-citations from the indexing cue.");      
  }
}

/**
 * Implementation of hook_get_options() 
 *
 * Get the available options for the "cite_index" drush command.
 *
 * @return
 *    An associative array containing the option definition as the key, and the
 *    description as the value, for each of the available options.
 */
function apachesolr_cite_index_get_options() {
  $options = array(
   '--DELETE=<commit buffer>'                 => dt('Toggles delete operation for query.'),
   '--LIMIT=<SQL LIMIT>'                      => dt('ex: 1, 1000'),
   '--FROM_EXTRA=<EXTRA SQL FROM>'            => dt('Additional FROM tables'),   
   '--WHERE_EXTRA=<EXTRA SQL WHERE>'          => dt('Additional WHERE args, must include booleans (ex: " AND WHERE nid=12345)"'),
   '--FULL_INDEXER'                           => dt('Uses Core ApacheSolr Module document generator (Slow but complete)'),
   '--ORDER_BY'                               => dt('Overrides default ORDER BY n.nid ASC SQL clause'),
  );
  return $options;
}

/**
 * Allows you to issue a drush command to check if nodes are in the index
 * - that is, verify that they were indexed.
 */
function _apachesolr_cite_index_check_index() {
  global $args; 
  $results = db_query("SELECT nid, status FROM {apachesolr_search_nodes}");
  $solr =& apachesolr_get_solr($args['options']['SOLR_HOST'], $args['options']['SOLR_PORT'], $args['options']['SOLR_PATH']);
  while ($row = db_fetch_object($results)) {
    $keys = sprintf('nid:%d', $row->nid);
    $query =& apachesolr_drupal_query($keys);
    $response = $solr->search($query->get_query(), 1, 1, array());
    if ($response->numFound < 1 && $node->status == 1) {
      $output[$row->nid] = 'Failed to Index Node: '. $row->nid;
    }
    if ($response->numFound >= 1 && $node->status === 0) {
      $output[$row->nid] = 'Failed to Remove Node from Index: '. $row->nid;
    }    
  }
  return $output;
}