 // Function to process GBS info and update the DOM.
 function ProcessGBSBookInfo(booksInfo) {
   for (isbn in booksInfo) {
    var element = document.getElementById(isbn);
    var bookInfo = booksInfo[isbn];
        if (bookInfo) {
      element.href = bookInfo.preview_url;
      if (bookInfo.preview == "full" ||
          bookInfo.preview == "partial") {
        element.style.display = '';
     }
    }
  }
}
