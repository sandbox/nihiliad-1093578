<?php
function template_preprocess_cite_email_item(&$variables) {
  global $base_url;
  //if this is a solr result
  // if ($variables['node']->id) {
  //   $solr_var = clone $variables['node']['node'];
  //   unset($variables['node']);
  //   $variables['node']->title;
  //   $variables['node']->field_journal_name[0]['value'] = $solr_var->ss_cck_field_journal_name;
  //   $variables['node']->field_volume[0]['value'] = $solr_var->ss_cck_field_volume;
  //   $variables['node']->field_issue[0]['value'] = $solr_var->ss_cck_field_issue;
  //   $variables['node']->field_start_page[0]['value'] = $solr_var->ss_cck_field_start_page;
  //   $variables['node']->field_end_page[0]['value'] = $solr_var->ss_cck_field_end_page;
  //   $variables['node']->field_issn[0]['value'] = $solr_var->ss_cck_field_issn;
  //   $variables['node']->field_isbn[0]['value'] = $solr_var->ss_cck_field_isbn; 
  //   $variables['node']->field_datetime_published[0]['value'] = $solr_var->ss_cck_field_datetime_published;
  //   $variables['node']->field_author = $solr_var->sm_cck_field_author;
  //   $variables['node']->field_subject = $solr_var->sm_cck_field_subjects;
  //   $variables['node']->title = (string) $solr_var->title;    
  // }
  
  // $variables['title'] = l($variables['node']->title, $base_url .'/node/'. $variables['node']->nid, $attributes = array(), $query = NULL, $fragment = NULL, $absolute = TRUE);
  
  if (module_exists('click')) {
    $variables['title'] = sprintf('<a href="%s/click/%d/0">%s</a>', $base_url, $variables['node']->nid, check_plain($variables['node']->title));
  }
  else {
    $variables['title'] = sprintf('<a href="%s/node/%d">%s</a>', $base_url, $variables['node']->nid, check_plain($variables['node']->title));
  }
  
  $variables['journal'] = check_plain($variables['node']->field_journal_name[0]['value']);
  $variables['volume'] = check_plain($variables['node']->field_volume[0]['value']);
  $variables['issue'] = ($variables['node']->field_issue[0]['value']) ? sprintf('(%s)', check_plain($variables['node']->field_issue[0]['value'])) : NULL;
  $start_page = check_plain($variables['node']->field_start_page[0]['value']);
  $end_page   = check_plain($variables['node']->field_end_page[0]['value']);
  $variables['issn'] = ($variables['node']->field_issn[0]['value']) ? check_plain($variables['node']->field_issn[0]['value']) : NULL;
  $variables['isbn'] = ($variables['node']->field_isbn[0]['value']) ? check_plain($variables['node']->field_isbn[0]['value']) : NULL;  
  $datetime = explode('-', $variables['node']->field_datetime_published[0]['value']);
  $date = sprintf('%d/%02d/%02d/', $datetime[0], $datetime[1], $datetime[2], 0, 2);
  
  $timestamp = strtotime($date);
  $year = $datetime[0];
  $month = date("M", $timestamp);
  $day = (int) substr($datetime[2], 0, 2);
  $day = ($day > 0) ? $day .';' : NULL;
  $variables['date'] = sprintf('%d %s %s', $datetime[0], $month, $day);

  if (($variables['issue'] || $variables['volume']) && $start_page) {
    $start_page = ':'. $start_page;
  }
  
  if (($variables['issue'] || $variables['volume']) && (!$start_page && $end_page)) {
    $end_page = ':'. $end_page;
  }
  
  $variables['pages'] = ($start_page && $end_page) ? sprintf('%s-%s', $start_page, $end_page) : sprintf('%s %s', $start_page, $end_page);
  
  if ($variables['node']->field_author[0]['value']) {
    foreach ($variables['node']->field_author as $author) {
      $variables['authors'][] = trim(check_plain($author['value']));
    }
  }
  
  if ($variables['node']->field_subject[0]['value']) {
    foreach ($variables['node']->field_subject as $subject) {
      $variables['subjects'][] = trim(check_plain($subject['value']));
    }
  }

}