<?php

class Cite_ModelRegistry
{
    protected static $instances = array();

    public static function getInstance(MySQLi $mysqli = NULL)
    {
        if (NULL == $mysqli) {
            // We are likely running inside Drupal, so try to get Drupal's db handle:
            global $active_db;
            if (NULL == $active_db) {
                throw new Exception("No MySQLi object passed in, and cannot find a Drupal database handle.");
            }
            if (!($active_db instanceof MySQLi)) {
                // Note: This will fail for Drupal >= 7.x, which uses PDO.
                throw new Exception("The Drupal database handle must be an instance of MySQLi in order to use this class.");
            }
            $mysqli = $active_db;
        }

        // Get the Cite_Model instance associated with this db handle. Create one if it doesn't already exist. 
        $object_vars = get_object_vars($mysqli);
        if (!array_key_exists('__drupal_cite_mysqli_id', $object_vars)) {
            $mysqli->__drupal_cite_mysqli_id = uniqid();
        }
        $id = $mysqli->__drupal_cite_mysqli_id;
        $instances = self::$instances;
        if (!array_key_exists($id, $instances)) {
            $instances[$id] = new Cite_Model($mysqli);
        }
        return $instances[$id];
    }
} 
