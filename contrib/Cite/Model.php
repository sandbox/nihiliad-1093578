<?php

class Cite_Model
{
    protected $mysqli;
    public function mysqli()
    {
        return $this->mysqli;
    }
    protected function setMysqli( $mysqli )
    {
        $this->mysqli = $mysqli;
    }

    public function __construct(MySQLi $mysqli)
    {
        $this->setMysqli($mysqli);
    }

    // We don't bother to prepare the statements in these functions, because they should be
    // executed only once or twice per process:
    public function citeSourcePrimaryKeyExists()
    {
        $result = $this->mysqli()->query("SHOW INDEX FROM cite_source WHERE Key_name = 'PRIMARY'");
        return $result->num_rows > 0 ? true : false;
    }
    public function citeSourceSourceIdIndexExists()
    {
        $result = $this->mysqli()->query("SHOW INDEX FROM cite_source WHERE Key_name = 'source_id_idx'");
        return $result->num_rows > 0 ? true : false;
    }

    public function dropCiteSourceIndex()
    {
        $mysqli = $this->mysqli();

        if (!$this->citeSourcePrimaryKeyExists()) {
            error_log("Attempt to drop a non-existent primary key on cite_source.");
        } else {
            // Drop the primary key on the cite source table so that MySQL doesn't
            // re-index after every insert:
            if (FALSE === $mysqli->query('ALTER TABLE cite_source DROP PRIMARY KEY')) {
                throw new Exception(
                    "Failed to drop primary key index on cite_source: " . $mysqli->error
                );
            }
        }

        if (!$this->citeSourceSourceIdIndexExists()) {
            error_log("Attempt to drop a non-existent index on cite_source.");
        } else {
            if (FALSE === $mysqli->query('DROP INDEX source_id_idx ON cite_source')) {
                throw new Exception(
                    "Failed to drop source_id_idx index on cite_source: " . $mysqli->error
                );
            }
        }
    }

    public function addCiteSourceIndex()
    {
        $mysqli = $this->mysqli();

        if ($this->citeSourcePrimaryKeyExists()) {
            error_log("Attempt to add a primary key to cite_source, but a primary key already exists.");
        } else {
            $mysql_server_version = $mysqli->server_version;
            // TODO: Previously I needed this commented-out version of the query; now I don't. What's going on?
            //$query = 'ALTER TABLE cite_source DROP INDEX `PRIMARY`, ADD PRIMARY KEY (nid,source_id,source)';
            $query = 'ALTER TABLE cite_source ADD PRIMARY KEY (nid,source_id,source)';
            if (50100 <= (int)$mysql_server_version) {
                $query = 'ALTER TABLE cite_source ADD PRIMARY KEY (nid,source_id,source)';
            }
            if (FALSE === $mysqli->query($query)) {
                throw new Exception(
                    "Failed to add primary key index on cite_source: " . $mysqli->error
                );
            }
        }

        if ($this->citeSourceSourceIdIndexExists()) {
            error_log("Attempt to add an index to cite_source, but the index already exists.");
        } else {
            if (FALSE === $mysqli->query('CREATE INDEX source_id_idx ON cite_source (source_id, source)')) {
                throw new Exception(
                    "Failed to add source_id_idx index on cite_source: " . $mysqli->error
                );
            }
        }
    }

    protected $citeSourceInsertStmt;
    public function insertCiteSource($nid, $source_id, $source)
    {
        if (!isset($this->citeSourceInsertStmt)) {
            $this->citeSourceInsertStmt =
                $this->mysqli()->prepare('INSERT INTO cite_source(nid, source_id, source) VALUES (?, ?, ?)');
        }
        $stmt = $this->citeSourceInsertStmt;
        $stmt->bind_param('iss', $nid, $source_id, $source);
        $stmt->execute();
        if ($this->mysqli()->error) {
            throw new Exception("Failed to insert id '$source_id' from source '$source' for citation with nid '$nid': {$mysqli->errno}: {$mysqli->error}"); 
        }
    }

    protected $citeSourceNidSelectStmt;
    public function selectCiteSourceNids($source_id, $source)
    {
        if (!isset($this->citeSourceNidSelectStmt)) {
            $this->citeSourceNidSelectStmt =
                $this->mysqli()->prepare('SELECT DISTINCT nid FROM cite_source WHERE source_id = ? AND source = ?');
        }
        $stmt = $this->citeSourceNidSelectStmt;
        $stmt->bind_param('ss', $source_id, $source);
        $stmt->execute();
        if ($mysqli->error) { 
            throw new Exception("Failed to select nid from cite_source where source_id = '$source_id' and source = '$source': {$mysqli->errno}: {$mysqli->error}"); 
        } 
        unset($nid);
        // Must call this and free_result() below when using multiple select
        // statements on the same database handle!
        $stmt->store_result();
        $stmt->bind_result($nid);
        $nids = array(); // We may find more than one of these, even though we shouldn't.
        while ($stmt->fetch()) {
            $nids[] = $nid;
        }
        // Must call this and store_result() above when using multiple select
        // statements on the same database handle!
        $stmt->free_result();

        return $nids;
    }

    protected $maxVidSelectStmt;
    public function selectMaxVid($nid)
    {
        if (!isset($this->maxVidSelectStmt)) {
            $this->maxVidSelectStmt =
                $this->mysqli()->prepare('SELECT MAX(vid) FROM content_type_cite WHERE nid = ?');
        }
        $stmt = $this->maxVidSelectStmt;
        $stmt->bind_param('i', $nid);
        $stmt->execute();
        if ($mysqli->error) { 
            throw new Exception("Failed to select max vid from content_type_cite where nid = '$nid': {$mysqli->errno}: {$mysqli->error}"); 
        } 
        unset($vid);
        // Must call this and free_result() below when using multiple select
        // statements on the same database handle!
        $stmt->store_result();
        $stmt->bind_result($vid);
        $stmt->fetch();
        // Must call this and store_result() above when using multiple select
        // statements on the same database handle!
        $stmt->free_result();

        return $vid;
    }

    protected $citeColumnExists = array();
    public function citeColumnExists($column)
    {
        if (!array_key_exists($column, $this->citeColumnExists)) {
            // We don't bother to prepare this statement, because it should be executed
            // only a few times per process:
            $result = $this->mysqli()->query("SHOW COLUMNS FROM content_type_cite WHERE Field = '$column'");
            if ($mysqli->error) { 
                throw new Exception("Failed to show columns from content_type_cite where Field = '$column': {$mysqli->errno}: {$mysqli->error}"); 
            } 
            $this->citeColumnExists[$column] = $result->num_rows > 0 ? true : false;
        }
        return $this->citeColumnExists[$column];
    }

    protected $citeUpdateStmt;
    public function updateCite($type, $source, $nid, $vid)
    {
        // Due to EthicShare citation schema changes, these columns may or may not exist:
        if (!$this->citeColumnExists('field_type_value') || !$this->citeColumnExists('field_source_value')) {
            return;
        }
        if (!isset($this->citeUpdateStmt)) {
            $this->citeUpdateStmt =
                $this->mysqli()->prepare('UPDATE content_type_cite SET field_type_value = ?, field_source_value = ? WHERE nid = ? and vid = ?');
        }
        $stmt = $this->citeUpdateStmt;
        $stmt->bind_param('ssii', $type, $source, $nid, $vid);
        $stmt->execute();   
        if ($mysqli->error) { 
            throw new Exception("Failed to update citation with nid '$nid' and vid '$vid': {$mysqli->errno}: {$mysqli->error}"); 
        } 
    }

    protected $citeNewSchemaUpdateStmt;
    public function updateCiteNewSchema($types, $primary_language, $nid, $vid)
    {
        // Due to EthicShare citation schema changes, these columns may or may not exist:
        if (!$this->citeColumnExists('field_types_value') || !$this->citeColumnExists('field_primary_language_value')) {
            return;
        }
        if (!isset($this->citeNewSchemaUpdateStmt)) {
            $this->citeNewSchemaUpdateStmt =
                $this->mysqli()->prepare('UPDATE content_type_cite SET field_types_value = ?, field_primary_language_value = ? WHERE nid = ? and vid = ?');
        }
        $stmt = $this->citeNewSchemaUpdateStmt;
        $stmt->bind_param('ssii', $types, $primary_language, $nid, $vid);
        $stmt->execute();   
        if ($mysqli->error) { 
            throw new Exception("Failed to update citation (new schema) with nid '$nid' and vid '$vid': {$mysqli->errno}: {$mysqli->error}"); 
        } 
    }

    protected $nodeUpdateStmt;
    public function updateNode($uid, $nid, $vid)
    {
        if (!isset($this->nodeUpdateStmt)) {
            $this->nodeUpdateStmt =
                $this->mysqli()->prepare('UPDATE node SET uid = ? WHERE nid = ? and vid = ?');
        }
        $stmt = $this->nodeUpdateStmt;
        $stmt->bind_param('iii', $uid, $nid, $vid);
        $stmt->execute();   
        if ($mysqli->error) { 
            throw new Exception("Failed to update node with nid '$nid' and vid '$vid': {$mysqli->errno}: {$mysqli->error}"); 
        } 
    }

    protected $nodeRevisionsUpdateStmt;
    public function updateNodeRevisions($uid, $nid, $vid)
    {
        if (!isset($this->nodeRevisionsUpdateStmt)) {
            $this->nodeRevisionsUpdateStmt =
                $this->mysqli()->prepare('UPDATE node_revisions SET uid = ? WHERE nid = ? and vid = ?');
        }
        $stmt = $this->nodeRevisionsUpdateStmt;
        $stmt->bind_param('iii', $uid, $nid, $vid);
        $stmt->execute();   
        if ($mysqli->error) { 
            throw new Exception("Failed to update node revision with nid '$nid' and vid '$vid': {$mysqli->errno}: {$mysqli->error}"); 
        } 
    }
}
