<?php

require_once 'Console/CommandLine.php';

class Console_CommandLinePlus extends Console_CommandLine
{
    protected $required_params = array();
    public function required_params()
    {
        return $this->required_params;
    }

    // This constructor allows for a more terse declaration
    // of options and args, eliminating the need to call
    // addOption() & addArgument() for each one.
    public function __construct( $params )
    {
        if (isset($params['options'])) {
            $options = $params['options'];
            unset( $params['options'] );
        }
        if (isset($params['args'])) {
            $args = $params['args'];
            unset( $params['args'] );
        }

        parent::__construct( $params );

        if (isset($options)) {
            foreach ($options as $name => $params) {
                $this->addOption( $name, $params );
            }
        }
        if (isset($args)) {
            foreach ($args as $name => $params) {
                $this->addArgument( $name, $params );
            }
        }
    }

    public function addOption( $name, $params )
    {
        // For convenience. In most cases, should be no need to 
        // retype the name with a couple of dashes in front of it.
        if (!isset($params['long_name'])) {
            $params['long_name'] = "--$name";
        }

        // Keep track of which options are required, because the
        // parent class doesn't handle this correctly. See parse().
        if (isset($params['optional'])) {
            $optional = $params['optional'];
            unset( $params['optional'] );
            if (!$optional) {
                $this->required_params[$name] = $params;
            }
        }

        parent::addOption( $name, $params );
    }

    public function parse()
    {
        list($argc, $argv) = parent::getArgcArgv();
        $args = func_get_args();
        $my_argc = (isset($args[0]) ? $args[0] : $argc);
        $my_argv = (isset($args[1]) ? $args[1] : $argv);

        $cl = parent::parse($my_argc, $my_argv);
        $parsed_options = $cl->options;

        // If an option is missing from the command line, the parent
        // class fails to throw an exception. However, it will throw an
        // exception if the option is present but has no value. So here
        // we force the parent class to throw an exception in the case of 
        // missing required options, by adding any missing options, without
        // values, and parsing the command line again:
        $missing_required_params = false;
        foreach ($this->required_params as $name => $params) {
            if (isset($parsed_options[$name]) && $parsed_options[$name] != '') {
                continue;
            }
            $missing_required_params = true;
            
            // This seems to cause the parent class to assume that '=' is the value:
            //$my_argv[] = $params['long_name'] . '=';
            $my_argv[] = $params['long_name'];
        }
        if ($missing_required_params) {
            // This should now throw an exception:
            $cl = parent::parse(count($my_argv), $my_argv);
        }
        return $cl;
    }

} // end class Console_CommandLinePlus

?>
