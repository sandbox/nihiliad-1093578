#!/usr/bin/php -q
<?php

require_once 'simpletest/autorun.php';
SimpleTest :: prefer(new TextReporter());
set_include_path('../php' . PATH_SEPARATOR . get_include_path());
require_once 'Console/CommandLinePlus.php';

ini_set('memory_limit', '2G');

error_reporting( E_ALL );

class ConsoleCommandLinePlusTest extends UnitTestCase
{
    public function test_new()
    {
        $parser = new Console_CommandLinePlus(array(
            'description' => 'Test suite for Console_CommandLinePlus',
            'version'     => '0.0.1',
        ));
        $this->assertIsA($parser, 'Console_CommandLinePlus');
        $cl = $parser->parse();
         
        // This should give the name of this program itself, which we'll
        // use for later tests:
        $this->self_name = $parser->name;
    }

    public function test_opts()
    {
        unset( $parser );
        $parser = new Console_CommandLinePlus(array(
            'description' => 'Test suite for Console_CommandLinePlus',
            'version'     => '0.0.1',
            'options' => array(
                'foo' => array(
                    'description' => 'Are we foo-barred or not?',
                    'optional' => false,
                ),
                'bar' => array(
                    'long_name'   => '--barre',
                    'description' => 'long name different from option name',
                ),
                'baz' => array(
                    'short_name'  => '-b',
                    'description' => 'testing short names',
                ),
            ),
        ));
        $this->assertIsA($parser, 'Console_CommandLinePlus');

        // This triggers an exception:
        //$opts = array($this->self_name, '--foo=bar', '--barre', '-b', 'luhrmann',);

        $opts = array($this->self_name, '--foo=bar', '--barre', 'belle', '-bluhrmann',);
        $cl = $parser->parse(count($opts), $opts);
        
        $this->assertEqual($cl->options['foo'], 'bar');
        $this->assertEqual($cl->options['bar'], 'belle');
        $this->assertEqual($cl->options['baz'], 'luhrmann');
    }

    public function test_args()
    {
        unset( $parser );
        $parser = new Console_CommandLinePlus(array(
            'description' => 'Test suite for Console_CommandLinePlus',
            'version'     => '0.0.1',
            'args' => array(
                'output_file' => array('optional' => true),
                'input_files' => array(
                    'multiple' => true,
                    'optional' => true,
                ),
            ),
        ));
        $this->assertIsA($parser, 'Console_CommandLinePlus');

        $args = array(
            $this->self_name,
            'out',
            'in1',
            'in2',
        );
        $cl = $parser->parse(count($args), $args);

        $this->assertEqual($cl->args['output_file'], 'out');
        $this->assertEqual($cl->args['input_files'], array('in1','in2'));
    }

    public function test_opts_args()
    {
        unset( $parser );
        $parser = new Console_CommandLinePlus(array(
            'description' => 'Test suite for Console_CommandLinePlus',
            'version'     => '0.0.1',
            'options' => array(
                'foo' => array(
                    'description' => 'Are we foo-barred or not?',
                    'optional' => false,
                ),
                'bar' => array(
                    'long_name'   => '--barre',
                    'description' => 'long name different from option name',
                ),
                'baz' => array(
                    'short_name'  => '-b',
                    'description' => 'testing short names',
                ),
            ),
            'args' => array(
                'output_file' => array('optional' => true),
                'input_files' => array(
                    'multiple' => true,
                    'optional' => true,
                ),
            ),
        ));
        $this->assertIsA($parser, 'Console_CommandLinePlus');

        $opts_args = array(
            $this->self_name,
            '--foo=bar',
            '--barre', 'belle',
            '-bluhrmann',
            'out',
            'in1',
            'in2',
        );
        $cl = $parser->parse(count($opts_args), $opts_args);

        $this->assertEqual($cl->options['foo'], 'bar');
        $this->assertEqual($cl->options['bar'], 'belle');
        $this->assertEqual($cl->options['baz'], 'luhrmann');

        $this->assertEqual($cl->args['output_file'], 'out');
        $this->assertEqual($cl->args['input_files'], array('in1','in2'));

        $this->parser = $parser;
    }

    public function test_missing_required_options()
    {
        $this->expectException();
        $cl = $this->parser->parse();
    }

} // end ConsoleCommandLinePlusTest
