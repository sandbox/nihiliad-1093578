<?php
// $Id$

/**
 * @file
 * Traverse a directory tree and return an array of files that
 * match a set of rules.
 *
 * Text_Glob implements glob(3) style matching that can be used to match
 * against text, rather than fetching names from a filesystem.
 *
 * <h2>Synopsis</h2>
 *
 * @code
 *
 * require_once 'File/Find/Rule.php';
 * $f = new File_Find_Rule();
 *
 * // find all the subdirectories of a given directory
 * $subdirs = $f->directory()->in( $directory );
 *
 * // find all the .php files in /usr/share/php
 * $files = $f->file()->name( '*.php' )->in( '/usr/share/php' );
 *
 * @endcode
 *
 * <h1>Blame</h1>
 *
 * Based on the Perl File::Find::Rule module, by Richard Clamp, and available
 * on CPAN: http://search.cpan.org/dist/File-Find-Rule/lib/File/Find/Rule.pm
 *
 * Partially ported to PHP for the symfony project by Fabien Potencier, as part
 * of the sfFinder class. Port modified to be more similar to the Perl version,
 * brought closer to completion, and broken out into its own package separate 
 * from its dependencies by David Naughton. The Perl version uses some programming
 * language features lacking in PHP (e.g. code references), so many features of
 * the Perl module are not yet ported and will require significant effort to
 * implement.
 *
 * @package File_Find_Rule
 *
 * @author     David Naughton <nihiliad@gmail.com>
 * @author     Fabien Potencier <fabien.potencier@gmail.com>
 * @author     Richard Clamp <richardc@unixbeard.net>
 * @copyright  2009 David Naughton <nihiliad@gmail.com>
 * @copyright  2004-2005 Fabien Potencier <fabien.potencier@gmail.com>
 * @copyright  2002, 2003, 2006 Richard Clamp <richardc@unixbeard.net>
 * @version    0.1.0
 */

require_once 'Text/Glob.php';
require_once 'Number/Compare.php';

class File_Find_Rule {
    
    protected $type                   = 'file';
    protected $names                  = array();
    protected $prunes                 = array();
    protected $discards               = array();
    protected $execs                  = array();
    protected $mindepth               = 0;
    protected $sizes                  = array();
    protected $maxdepth               = 1000000;
    protected $relative               = false;
    protected $follow_link            = false;
    protected $sort                   = false;
    protected $ignore_version_control = true;
    protected static $version_control_patterns = 
        array('.svn', '_svn', 'CVS', '_darcs', '.arch-params', '.monotone', '.bzr', '.git', '.hg');

    protected $tg;

    function __construct()
    {
        $this->tg = new Text_Glob();
    }
    
    /**
     * Descend at most $level (a non-negative integer) levels of directories
     *  below the starting point.
     *
     * May be invoked many times per rule, but only the most recent value is used.
     *
     * @note
     * The PHP version interprets $level differently than the Perl version. In
     * the PHP version, $level 0 includes the leaves of the root directory of the
     * subtree, and $level 1 includes the first generation of sub-directories below
     * the root, plus the leaves of those sub-directories. In the Perl version,
     * $level 0 includes only the root directory of the subtree, and $level 1
     * includes the leaves of the root directory and the first generation of
     * sub-directories. The level semantics of the PHP version may change in
     * the future to match the Perl version.
     *
     * @see mindepth()
     *
     * @param int $level
     * @return object $this
     */
    public function maxdepth($level)
    {
        $this->maxdepth = $level;
        return $this;
    }
    
    /**
     * Do not apply any tests at levels less than $level (a non-negative integer).
     *
     * @note
     * The PHP version interprets $level differently than the Perl version.
     * 
     * @see maxdepth()
     *
     * @param  int $level
     * @return object $this
     */
    public function mindepth($level) {
        $this->mindepth = $level;
        return $this;
    }

    /**
     * Rule to match only files. This is the default.
     * 
     * @return object $this
     */
    public function file()
    {
        $this->type = 'file';
        return $this;
    }
    
    /**
     * Rule to match only directories.
     * 
     * @return object $this
     */
    public function directory()
    {
        $this->type = 'directory';
        return $this;
    }

    /**
     * Rule to match both files and directories.
     * 
     * @return object $this
     */
    public function any()
    {
        $this->type = 'any';
        return $this;
    }
    
    /**
     * Transforms a list of mixed globs, regexes, and string literals
     * into regexes.
     *
     * @param array $strings
     * @param boolean $not
     *   Indicates whether or not the rule should be negated.
     * @return array $regexes
     *   An array of ($not, $regex) pairs. 
     */
    protected function strings_to_regexes($strings, $not = false)
    {
        $regexes = array();
        foreach ($strings as $string) {
            if (is_array($string)) {
                $regexes += $this->strings_to_regexes($string, $not);
            //} else if ($string{0} == '/' && $string{strlen($string)-1} == '/') {
            } else if (preg_match('#^/.+/$#', $string)) {
                $regexes[] = array($not, $string);
            } else {
                $regexes[] = array($not, $this->tg->glob_to_regex($string));
            }
        }
        return $regexes;
    }
    
    /**
     * Specifies names that should match.
     *
     * @code
     * $f->name('*.php')
     * $f->name('/\.php$/') // same as above
     * $f->name('test.php')
     * @endcode
     *
     * @param array $strings
     *   A mixed list of globs, patterns, and string literals. Mixing arrays
     *   and scalars of these types of strings is also valid input. Patterns
     *   (regexes) must be delimited with the '/' character.
     * @return object $this
     */
    public function name()
    {
        $strings = func_get_args();
        $this->names += $this->strings_to_regexes($strings);
        return $this;
    }
    
    /**
     * Specifies names that should not match.
     *
     * @code
     * $f->not_name('*.php');
     * @endcode
     *
     * @see name()
     * @param array $strings
     *   A mixed list of globs, patterns, and string literals. Mixing arrays
     *   and scalars of these types of strings is also valid input. Patterns
     *   (regexes) must be delimited with the '/' character.
     * @return object $this
     */
    public function not_name()
    {
        $strings = func_get_args();
        $this->names += $this->strings_to_regexes($strings, true);
        return $this;
    }
    
    /**
     * Match based on file sizes.
     *
     * @code
     * $f->size('> 10K');
     * $f->size('<= 1Ki');
     * $f->size(4);
     * @endcode
     *
     * @see Number_Compare
     * @param list $targets
     *   A comma-separated list of target patterns that follow Number_Compare
     *    semantics.
     * @return object $this
     */
    public function size()
    {
        foreach (func_get_args() as $target) {
            $this->sizes[] = new Number_Compare($target);
        }
        return $this;
    }
    
    /**
     * Traverses no further. This rule always matches.
     *
     * @note
     * This seems to work very differently from the Perl version. Probably best
     * not to use it until it can be tested and investigated.
     *
     * @param  list $strings
     *   A list of patterns, globs, and/or literal strings to match.
     * @return object $this
     */
    public function prune()
    {
        $strings = func_get_args();
        $this->prunes += $this->strings_to_regexes($strings);
        return $this;
    }
    
    /**
     * Don't keep this file. This rule always matches.
     *
     * @note
     * This seems to work very differently from the Perl version. Probably best
     * not to use it until it can be tested and investigated.
     *
     * @param  list $strings
     *   A list of patterns, globs, and/or literal strings to match.
     * @return object $this
     */
    public function discard()
    {
        $strings = func_get_args();
        $this->discards += $this->strings_to_regexes($strings);
        return $this;
    }
    
    /**
     * Ignores version control directories.
     *
     * Currently supports Subversion, CVS, DARCS, Gnu Arch, Monotone, Bazaar-NG,
     * GIT, Mercurial.
     *
     * @note
     * This method is not part of the Perl version and may be deprecated. Trying
     * decide whether or not to keep it. It does currently appear in the test
     * suite, but that was only to get the Perl tests to pass. 
     *
     * @param boolean $ignore
     *   False when version control directories shall be included (default is true).
     * @return object $this
     */
    public function ignore_version_control($ignore = true)
    {
        $this->ignore_version_control = $ignore;
        return $this;
    }
    
    /**
     * Tells File_Find_Rule to sort the results by name.
     *
     * @note
     * This method is not part of the Perl version, does not appear in the test
     * suite of this PHP version, and may be deprecated. Sorting in Perl uses 
     * functional programming semantics in a way that currently is impossible in
     * PHP. Achieving the power of the Perl version in PHP will require
     * significant effort. Also, I really don't like that sorting is so limited.
     *
     * @return object $this
     */
    public function sort_by_name()
    {
        $this->sort = 'name';
        return $this;
    }
    
    /**
     * Returns files and directories ordered by type (directories before files),
     * then by name.
     *
     * @note
     * This method is not part of the Perl version, does not appear in the test
     * suite of this PHP version, and may be deprecated. Sorting in Perl uses 
     * functional programming semantics in a way that currently is impossible in
     * PHP. Achieving the power of the Perl version in PHP will require
     * significant effort. Also, I really don't like that sorting is so limited.
     *
     * @return object $this
     */
    public function sort_by_type()
    {
        $this->sort = 'type';
        return $this;
    }
    
    /**
     * Allows user-defined rules.
     * 
     * Executes function or method for each element.
     * Element match if function or method returns true.
     *
     * @code
     * $f->exec('myfunction');
     * $f->exec(array($object, 'mymethod'));
     * @endcode
     *
     * @param mixed function or method to call
     *   The arguments to the function or method will be $path and $basename.
     *   Your function or method must return a boolean to indicate match
     *   success or failure.
     * @return object $this
     */
    public function exec()
    {
        foreach (func_get_args() as $exec) {
            if (is_array($exec)) {
                list($object, $method) = $exec;
                throw new Exception("method '$method' does not exist for object '$object'.");
            } else if (!function_exists($exec)) {
                throw new Exception("function '$exec' does not exist.");
            }
            $this->execs[] = $exec;
        }
        return $this;
    }
    
    /**
     * Returns relative paths for all files and directories.
     *
     * @note
     * The Perl version docs describe this as: Trim the leading portion of any
     * path found.
     *
     * @return object $this
     */
    public function relative()
    {
        $this->relative = true;
        return $this;
    }
    
    /**
     * Symlink following.
     *
     * @note
     * The Perl version provides this feature via the "extras" method. Currently
     * untested in the PHP version.
     *
     * @return object $this
     */
    public function follow_link()
    {
        $this->follow_link = true;
        return $this;
    }
    
    /**
     * Evaluates the rule, returns a list of paths to matching files and directories.
     *
     * @code
     * $matches = $f->in( $directories );
     * @endcode
     *
     * @param array list of directories to search within
     *   TODO: Check exactly what types this method accepts. Make it more
     *   flexible if necessary.
     * @return array list of files and directories
     */
    public function in()
    {
        $f = clone $this;
        
        if ($this->ignore_version_control) {
            $f->discard( self::$version_control_patterns )
              ->prune( self::$version_control_patterns );
        }
        
        // Merge the args into a single array:
        $orig_dirs = array();
        foreach (func_get_args() as $orig_dir) {
            $orig_dirs += (array)$orig_dir;
        }

        $files = array();
        foreach ($orig_dirs as $orig_dir) {
            $dir = realpath($orig_dir);
            if (!is_dir($dir)) continue;
            
            // TODO: I strongly doubt this is portable. Needs testing. - naughton
            // Also seems to make some of the complex conditions in
            // is_path_absolute() unnecessary.
            $dir = str_replace('\\', '/', $dir);
            
            // absolute path?
            if (!self::is_path_absolute($dir)) {
                $dir = getcwd() . '/' . $dir;
            }
            
            // TODO: Again, what's with the replacement of '\\' with '/'? How
            // is this portable?
            $new_files = str_replace(
                '\\', '/',
                $f->search_in(array('dir' => $dir, 'orig_dir' => $orig_dir))
            );
            
            if ($this->relative) {
                $new_files = str_replace(rtrim($dir, '/') . '/', '', $new_files);
            }
            
            // TODO: Investigate the differences between '+' & 'array_merge'.
            // '+' preserves indexes and is shorter. What else?
            $files += $new_files;
        }
        
        if ($this->sort == 'name') {
            sort($files);
        }

        return array_unique($files);
    }
    
    /**
     * Traverses a directory tree, applies the rules, and returns the matching
     * elements. This method does most of the hard work of File_Find_Rule.
     *
     * @param array $params
     *   An associative array with the following key-value pairs:
     *   - dir => $current_directory
     *   - orig_dir => $current_directory // as the user defined it
     *   - depth => $integer // depth of the current directory relavite to the root
     * @return array $matches
     */
    //protected function search_in($dir, $depth = 0) {
    protected function search_in($params)
    {
        $dir = $params['dir'];
        if (array_key_exists('orig_dir', $params)) {
            $orig_dir = $params['orig_dir'];
        }
        $depth = isset($params['depth']) ? $params['depth'] : 0;
        //if ($depth >= $this->maxdepth) {
        if ($depth > $this->maxdepth) {
            return array();
        }
        
        $dir = realpath($dir);
        
        if ((!$this->follow_link) && is_link($dir)) {
            return array();
        }
        
        $files = array();
        $temp_files = array();
        $temp_folders = array();
        if (is_dir($dir)) {
            $current_dir = opendir($dir);
            $found_top_dir = false;
            while (false !== $entryname = readdir($current_dir)) {
                if ($entryname == '.' || $entryname == '..') {
                    continue;
                }
                
                $current_entry = $dir . DIRECTORY_SEPARATOR . $entryname;
                if ((!$this->follow_link) && is_link($current_entry)) {
                    continue;
                }
                
                if (is_dir($current_entry)) {
                    if ($this->sort == 'type') {
                        $temp_folders[$entryname] = $current_entry;
                    } else {
                        if (($this->type == 'directory' || $this->type == 'any') && ($depth >= $this->mindepth) && !$this->is_discarded($dir, $entryname) && $this->match_names($dir, $entryname) && $this->exec_ok($dir, $entryname)) {
                            $files[] = $current_entry;
                        }
                        if (!$this->is_pruned($dir, $entryname)) {
                            $files += 
                                $this->search_in(array('dir' => $current_entry, 'depth' => $depth + 1));
                        }
                    }
                } else {
                    if (($this->type != 'directory' || $this->type == 'any') && ($depth >= $this->mindepth) && !$this->is_discarded($dir, $entryname) && $this->match_names($dir, $entryname) && $this->size_ok($dir, $entryname) && $this->exec_ok($dir, $entryname)) {
                        if ($this->sort == 'type') {
                            $temp_files[] = $current_entry;
                        } else {
                            $files[] = $current_entry;
                        }
                    }
                }
            }
            
            if ($this->sort == 'type') {
                ksort($temp_folders);
                foreach ($temp_folders as $entryname => $current_entry) {
                    if (($this->type == 'directory' || $this->type == 'any') && ($depth >= $this->mindepth) && !$this->is_discarded($dir, $entryname) && $this->match_names($dir, $entryname) && $this->exec_ok($dir, $entryname)) {
                        $files[] = $current_entry;
                    }
                    if (!$this->is_pruned($dir, $entryname)) {
                        $files +=
                            $this->search_in(array('dir' => $current_entry, 'depth' => $depth + 1));
                    }
                }
                sort($temp_files);
                $files += $temp_files;
            }
            closedir($current_dir);
        }
        return $files;
    }
    
    /**
     * Attempts to match an item against all name rules.
     *
     * @param $dir
     *   This doesn't appear to be used.
     * @param $entry
     *   The item to match.
     * @return $boolean
     */
    protected function match_names($dir, $entry)
    {
        if (!count($this->names)) {
            return true;
        }
        
        // we must match one "not_name" rule to be ok
        $one_not_name_rule = false;
        foreach($this->names as $args) {
            list($not, $regex) = $args;
            if ($not) {
                $one_not_name_rule = true;
                if (preg_match($regex, $entry)) {
                    return false;
                }
            }
        }
        
        $one_name_rule = false;
        // we must match one "name" rules to be ok
        foreach($this->names as $args) {
            list($not, $regex) = $args;
            if (!$not) {
                $one_name_rule = true;
                if (preg_match($regex, $entry)) {
                    return true;
                }
            }
        }
        
        if ($one_not_name_rule && $one_name_rule) {
            return false;
        } else if ($one_not_name_rule) {
            return true;
        } else if ($one_name_rule) {
            return false;
        } else {
            return true;
        }
    }
    
    /**
     * Attempts to match an item against all size rules.
     *
     * @param $dir
     *  The path to the item.
     * @param $entry
     *   The item to match.
     * @return $boolean
     */
    protected function size_ok($dir, $entry)
    {
        if (!count($this->sizes)) {
            return true;
        }
        if (!is_file($dir . DIRECTORY_SEPARATOR . $entry)) {
            return true;
        }
        $filesize = filesize( $dir . DIRECTORY_SEPARATOR . $entry);
        foreach($this->sizes as $number_compare) {
            if (!$number_compare->test($filesize)) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Determines whether or not an item should be pruned.
     *
     * @param $dir
     *  The path to the item. Seems to be unused.
     * @param $entry
     *   The item to match.
     * @return $boolean
     */
    protected function is_pruned($dir, $entry)
    {
        if (!count($this->prunes)) {
            return false;
        }
        foreach($this->prunes as $args) {
            $regex = $args[1];
            if (preg_match($regex, $entry)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Determines whether or not an item should be discarded.
     *
     * @param $dir
     *  The path to the item. Seems to be unused.
     * @param $entry
     *   The item to match.
     * @return $boolean
     */
    protected function is_discarded($dir, $entry)
    {
        if (!count($this->discards)) {
            return false;
        }
        foreach($this->discards as $args) {
            $regex = $args[1];
            if (preg_match($regex, $entry)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Executes user defined rules against an item.
     *
     * @param $dir
     *  The path to the item.
     * @param $entry
     *   The item to match.
     * @return $boolean
     */
    protected function exec_ok($dir, $entry)
    {
        if (!count($this->execs)) {
            return true;
        }
        foreach($this->execs as $exec) {
            if (!call_user_func_array($exec, array($dir, $entry))) return false;
        }
        return true;
    }
    
    /**
     * Determines whether or not a path is absolute, supposedly in a
     * portable, cross-platform way. This is untested, so not sure it
     * really is portable.
     *
     * @param $path
     * @return $boolean
     */
    public static function is_path_absolute($path)
    {
        if (
            $path{0} == '/'  || # *n*x 
            $path{0} == '\\' || # ?
            (
             strlen($path) > 3     && # Windows drive-letter paths, apparently.
             ctype_alpha($path{0}) &&
             $path{1} == ':'       &&
             ($path{2} == '\\' || $path{2} == '/')
            )
           ) return true;

        return false;
    }
}
// end class File_Find_Rule
