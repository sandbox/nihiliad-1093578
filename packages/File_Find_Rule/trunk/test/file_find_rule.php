#!/usr/bin/php -q
<?php

require_once 'simpletest/autorun.php';
SimpleTest :: prefer(new TextReporter());
set_include_path('../php' . PATH_SEPARATOR . get_include_path());
require_once 'File/Find/Rule.php';

//error_reporting( E_STRICT );

class FileFindRuleTest extends UnitTestCase
{

    protected $cwd;
    protected $tests;
    
    // Almost all the comments that follow are commented-out tests from the
    // original Perl version of this module.

    public function __construct()
    {
        //my @tests = qw( t/File-Find-Rule.t t/findrule.t );
        $cwd = getcwd();
        $this->tests = array( "$cwd/file_find_fule.php", "$cwd/findrule.php" );
        $this->cwd = $cwd;
    }

    public function test_new()
    {
        //my $f = $class->new;
        $f = new File_Find_Rule();
        //isa_ok($f, $class);
        $this->assertIsA( $f, 'File_Find_Rule' );

    }

    public function test_name()
    {

        $f = new File_Find_Rule();
    
        // TODO: Add some tests to make sure we're in the dir we think we are.
        $cwd = $this->cwd;
        $tests = $this->tests;
        
        //$f = $class->name( qr/\.t$/ );
        $f->name( '/\.t$/' );
        //is_deeply( [ sort $f->in('t') ],
        //           [ @tests ],
        //           "name( qr/\\.t\$/ )" );
        $this->assertTrue( sort($f->in($cwd)) === sort($tests) );

        //$f = $class->name( 'foobar' );
        $f = new File_Find_Rule();
        $f->name( 'foobar' );
        //is_deeply( [ $f->in('t') ],
        //           [ 't/foobar' ],
        //           "name( 'foobar' )" );
        $files = $f->in( $cwd );
        $this->assertTrue( $files[0] == "$cwd/foobar" );

        //$f = $class->name( '*.t' );
        $f = new File_Find_Rule();
        $f->name( '*.t' );
        //is_deeply( [ sort $f->in('t') ],
        //          \@tests,
        //          "name( '*.t' )" );
        $this->assertTrue( sort($f->in($cwd)) === sort($tests) );
        
        //$f = $class->name( 'foobar', '*.t' );
        $f = new File_Find_Rule();
        $f->name( 'foobar', '*.t' );
        //is_deeply( [ sort $f->in('t') ],
        //           [ @tests, 't/foobar' ],
        //           "name( 'foobar', '*.t' )" );
        $tests[] = "$cwd/foobar";
        $this->assertTrue( sort($f->in($cwd)) === sort($tests) );
 
        // TODO: In lieu of arrayrefs, modify API to accept arrays as well as "lists".
        //$f = $class->name( [ 'foobar', '*.t' ] );
        //is_deeply( [ sort $f->in('t') ],
        //           [ @tests, 't/foobar' ],
        //           "name( [ 'foobar', '*.t' ] )" );
        //
    }

    public function test_exec()
    {
        $cwd = $this->cwd;
        $tests = $this->tests;

        //$f = $class->exec(sub { length == 6 })->maxdepth(1);
        $f = new File_Find_Rule();
        $f->exec('basename_length_eq_6')->maxdepth(1);
        //is_deeply( [ $f->in('t') ],
        //           [ 't/foobar' ],
        //           "exec (short)" );
        $this->assertTrue( $f->in($cwd) === array("$cwd/foobar") );
        
        //$f = $class->exec(sub { length > $foobar_size })->maxdepth(1);
        $f = new File_Find_Rule();
        $f->exec('basename_length_gt_6')->maxdepth(1);
        //is_deeply( [ $f->in('t') ],
        // NOTE: Don't know how this next line could have been true...
        //           [ 't/File-Find-Rule.t' ],
        //           "exec (long)" );
        $this->assertTrue( sort($f->in($cwd)) === sort($tests) );
        
        // NOTE: The PHP version API seems a little different here: we don't get an "arg 2".
        // NOTE: We'll check the first ($path) arg instead.
        //is_deeply( [ find( maxdepth => 1, exec => sub { $_[2] eq 't/foobar' }, in => 't' ) ],
        $f = new File_Find_Rule();
        $f->exec('foobar_full_path')->maxdepth(1);
        //           [ 't/foobar' ],
        //           "exec (check arg 2)" );
        $this->assertTrue( $f->in($cwd) === array("$cwd/foobar") );
    }

    public function test_name_exec_chained()
    {        
        $cwd = $this->cwd;
        $tests = $this->tests;

        //$f = $class
        //  ->exec(sub { length > $foobar_size })
        //  ->name( qr/\.t$/ );
        $f = new File_Find_Rule();
        $f->exec('basename_length_gt_6')->name('/\.php$/');
        //is_deeply( [ $f->in('t') ],
        //           [ 't/File-Find-Rule.t' ],
        //           "exec(match) and name(match)" );
        $this->assertTrue( sort($f->in($cwd)) === sort($tests) );
        
        //$f = $class
        //  ->exec(sub { length > $foobar_size })
        //  ->name( qr/foo/ )
        //  ->maxdepth(1);
        $f = new File_Find_Rule();
        $f->exec('basename_length_gt_6')->name('/foo/')->maxdepth(1);
        //is_deeply( [ $f->in('t') ],
        //           [ ],
        //           "exec(match) and name(fail)" );
        $this->assertTrue( $f->in($cwd) === array() );
    }

    public function test_directory()
    {        
        $cwd = $this->cwd;
        $tests = $this->tests;

        //$f = $class
        //  ->directory
        //  ->maxdepth(1)
        //  ->exec(sub { $_ !~ /(\.svn|CVS)/ }); # ignore .svn/CVS dirs
        $f = new File_Find_Rule();
        $f->directory()
          ->maxdepth(1)
          ->ignore_version_control(false)
          ->exec('ignore_cvs_svn_dirs');
        //is_deeply( [ $f->in('t') ],
        //           [ qw( t t/lib  ) ],
        //           "directory autostub" );
        // NOTE: The PHP version interprets maxdepth differently than the Perl version.
        $found = $f->in($cwd);
        $expected = array("$cwd/lib","$cwd/File");
        //print_r( $found );
        $this->assertTrue(sort($found) === sort($expected));
    }

    public function test_any_or()
    {
        //$f = $class->any( $class->exec( sub { length == 6 } ),
        //                  $class->name( qr/\.t$/ )
        //                        ->exec( sub { length > $foobar_size } )
        //                )->maxdepth(1);
        //
        //is_deeply( [ sort $f->in('t') ],
        //           [ 't/File-Find-Rule.t', 't/foobar' ],
        //           "any" );
        //
        //$f = $class->or( $class->exec( sub { length == 6 } ),
        //                 $class->name( qr/\.t$/ )
        //                       ->exec( sub { length > $foobar_size } )
        //               )->maxdepth(1);
        //
        //is_deeply( [ sort $f->in('t') ],
        //           [ 't/File-Find-Rule.t', 't/foobar' ],
        //           "or" );
    }

    public function test_not_none()        
    {
        //$f = $class
        //  ->file
        //  ->not( $class->name( qr/^[^.]{1,8}(\.[^.]{0,3})?$/ ) )
        //  ->maxdepth(1)
        //  ->exec(sub { length == 6 || length > 10 });
        //is_deeply( [ $f->in('t') ],
        //           [ 't/File-Find-Rule.t' ],
        //           "not" );
    }

    public function test_not_star()
    {        
        $cwd = $this->cwd;

        //# not as not_*
        //$f = $class
        //  ->file
        //  ->not_name( qr/^[^.]{1,8}(\.[^.]{0,3})?$/ )
        //  ->maxdepth(1)
        //  ->exec(sub { length == 6 || length > 10 });
        $f = new File_Find_Rule();
        $f->file()
          ->not_name( '/^[^.]{1,8}(\.[^.]{0,3})?$/' ) 
          ->maxdepth(1)
          ->exec('basename_length_eq_6_or_gt_10');
        //is_deeply( [ $f->in('t') ],
        //           [ 't/File-Find-Rule.t' ],
        //           "not_*" );
        $this->assertTrue($f->in($cwd) === array("$cwd/file_find_rule.php"));
    }
     
    public function test_prune_discard()
    {   
        //# this test may be a little meaningless for a cpan release, but it
        //# fires perfectly in my dev sandbox
        //$f = $class->or( $class->directory
        //                        ->name(qr/(\.svn|CVS)/)
        //                        ->prune
        //                        ->discard,
        //                 $class->new->file );
        //
        //is_deeply( [ sort $f->in('t') ],
        //           [ @tests, 't/foobar', 't/lib/File/Find/Rule/Test/ATeam.pm' ],
        //           "prune/discard .svn"
        //         );
    }

    public function test_procedural_prune_discard()
    {        
        //$f = find(or => [ find( directory =>
        //                        name      => qr/(\.svn|CVS)/,
        //                        prune     =>
        //                        discard   => ),
        //                  find( file => ) ]);
        //
        //is_deeply( [ sort $f->in('t') ],
        //           [ @tests, 't/foobar', 't/lib/File/Find/Rule/Test/ATeam.pm' ],
        //           "procedural prune/discard .svn"
        //         );
    }     
   
    public function test_size()
    {
        $cwd = $this->cwd; 
        //# on win32 systems the t/foobar file isn't 10 bytes it's 11, so the
        //# previous tests on the magic number 10 failed.  rt.cpan.org #3838
        //my $foobar_size = -s 't/foobar';
        $foobar_size = filesize("$cwd/foobar");

        //# size (stat test)
        //is_deeply( [ find( maxdepth => 1, file => size => $foobar_size, in => 't' ) ],
        $f = new File_Find_Rule();
        $f->file()
          ->maxdepth(1)
          ->size( $foobar_size );
        //           [ 't/foobar' ],
        //           "size $foobar_size (stat)" );
        $this->assertTrue($f->in($cwd) === array("$cwd/foobar"));
        
        //is_deeply( [ find( maxdepth => 1, file => size => "<= $foobar_size",
        //                   in => 't' ) ],
        $f = new File_Find_Rule();
        $f->file()
          ->maxdepth(1)
          ->size( "<= $foobar_size" );
        //           [ 't/foobar' ],
        //           "size <= $foobar_size (stat)" );
        $this->assertTrue($f->in($cwd) === array("$cwd/foobar"));
        
        //is_deeply( [ find( maxdepth => 1, file => size => "<".($foobar_size + 1),
        //                   in => 't' ) ],
        $f = new File_Find_Rule();
        $f->file()
          ->maxdepth(1)
          ->size( "<=" . ($foobar_size + 1) );
        //           [ 't/foobar' ],
        //           "size <($foobar_size + 1) (stat)" );
        $this->assertTrue($f->in($cwd) === array("$cwd/foobar"));
        
        //is_deeply( [ find( maxdepth => 1, file => size => "<1K",
        //                   exec => sub { length == 6 },
        //                   in => 't' ) ],
        $f = new File_Find_Rule();
        $f->file()
          ->maxdepth(1)
          ->size( '<1K' )
          ->exec('basename_length_eq_6');
        //           [ 't/foobar' ],
        //           "size <1K (stat)" );
        $this->assertTrue($f->in($cwd) === array("$cwd/foobar"));
        
        //is_deeply( [ find( maxdepth => 1, file => size => ">3K", in => 't' ) ],
        $f = new File_Find_Rule();
        $f->file()
          ->maxdepth(1)
          ->size( '>3K' );
        //           [ 't/File-Find-Rule.t' ],
        //           "size >3K (stat)" );
        $this->assertTrue($f->in($cwd) === array("$cwd/file_find_rule.php"));
        
        //# these next two should never fail.  if they do then the testing fairy
        //# went mad
        //is_deeply( [ find( file => size => ">3M", in => 't' ) ],
        $f = new File_Find_Rule();
        $f->file()
          ->maxdepth(1)
          ->size( '>3M' );
        //           [ ],
        //           "size >3M (stat)" );
        $this->assertTrue($f->in($cwd) === array());
        
        //is_deeply( [ find( file => size => ">3G", in => 't' ) ],
        $f = new File_Find_Rule();
        $f->file()
          ->maxdepth(1)
          ->size( '>3G' );
        //           [ ],
        //           "size >3G (stat)" );
        $this->assertTrue($f->in($cwd) === array());
    }        

    public function test_min_max_depth()
    {
        $cwd = $this->cwd;
        $tests = $this->tests;

        //is_deeply( [ find( maxdepth => 0, in => 't' ) ],
        $f = new File_Find_Rule();
        $f->any()
          ->maxdepth(0);
        //           [ 't' ],
        //           "maxdepth == 0" );
        $found = $f->in($cwd);
        $expected = $tests;
        $expected[] = 'foobar';
        $expected[] = 'lib';
        // NOTE: The PHP version interprets depth differently than the Perl version,
        // so these tests needed to be adjusted.
        $this->assertTrue(sort($found) === sort($expected));
        
        //my $rule = find( or => [ find( name => qr/(\.svn|CVS)/,
        //                               discard =>),
        //                         find(),
        //                        ],
        //                 maxdepth => 1 );
        //
        //is_deeply( [ sort $rule->in( 't' ) ],
        //           [ 't', @tests, 't/foobar', 't/lib' ],
        //           "maxdepth == 1" );
        //is_deeply( [ sort $rule->in( 't/' ) ],
        //           [ 't', @tests, 't/foobar', 't/lib' ],
        //           "maxdepth == 1, trailing slash on the path" );
        //
        //is_deeply( [ sort $rule->in( './t' ) ],
        //           [ 't', @tests, 't/foobar', 't/lib' ],
        //           "maxdepth == 1, ./t" );
        //is_deeply( [ sort $rule->in( './././///./t' ) ],
        //           [ 't', @tests, 't/foobar', 't/lib' ],
        //           "maxdepth == 1, ./././///./t" );
        //
        //my @ateam_path = qw( t/lib
        //                     t/lib/File
        //                     t/lib/File/Find
        //                     t/lib/File/Find/Rule
        //                     t/lib/File/Find/Rule/Test
        //                     t/lib/File/Find/Rule/Test/ATeam.pm );
        //
        //is_deeply( [ sort +find( or => [ find( name => qr/(\.svn|CVS)/,
        //                                       prune =>
        //                                       discard =>),
        //                                 find( ),
        //                               ],
        //                         mindepth => 1,
        //                         in => 't' ) ],
        //           [ @tests, 't/foobar', @ateam_path ],
        //           "mindepth == 1" );
        //
        //
        //is_deeply( [ sort +find( or => [ find( name => qr/(\.svn|CVS)/,
        //                                       discard =>),
        //                                 find(),
        //                               ],
        //                         maxdepth => 1,
        //                         mindepth => 1,
        //                         in => 't' ) ],
        //           [ @tests, 't/foobar', 't/lib' ],
        //           "maxdepth = 1 mindepth == 1" );
    }    

    public function test_extras()
    {
        //my $ok = 0;
        //find( extras => { preprocess => sub { $ok = 1 } }, in => 't' );
        //ok( $ok, "extras preprocess fired" );
    }

    public function test_iterator()
    {
        //$f = find( or => [ find( name => qr/(\.svn|CVS)/,
        //                         prune =>
        //                         discard =>),
        //                   find(),
        //                 ],
        //           start => 't' );
        //
        //{
        //my @found;
        //while ($_ = $f->match) { push @found, $_ }
        //is_deeply( [ sort @found ], [ 't', @tests, 't/foobar', @ateam_path ], "iterator" );
        //}
    }
        
    public function test_procedural_negating()
    {
        //is_deeply( [ find( file => '!name' => qr/^[^.]{1,8}(\.[^.]{0,3})?$/,
        //                   maxdepth => 1,
        //                   in => 't' ) ],
        //           [ 't/File-Find-Rule.t' ],
        //           "negating in the procedural interface" );
        //
        //# grep
        //is_deeply( [ find( maxdepth => 1, file => grep => [ qr/bytes./, [ qr/.?/ ] ], in => 't' ) ],
        //           [ 't/foobar' ],
        //           "grep" );
        //
        //
    }

    public function test_relative()
    {
        $cwd = $this->cwd;
        //# relative
        //is_deeply( [ find( 'relative', maxdepth => 1, name => 'foobar', in => 't' ) ],
        $f = new File_Find_Rule();
        $f->relative()
          ->maxdepth(1)
          ->name('foobar');
        //           [ 'foobar' ],
        //           'relative' );
        $this->assertTrue($f->in($cwd) === array('foobar'));
    }        

    public function test_bootstrapping()
    {
        //# bootstrapping extensions via import
        //
        //use lib qw(t/lib);
        //
        //eval { $class->import(':Test::Elusive') };
        //like( $@, qr/^couldn't bootstrap File::Find::Rule::Test::Elusive/,
        //      "couldn't find the Elusive extension" );
        //
        //eval { $class->import(':Test::ATeam') };
        //is ($@, "",  "if you can find them, maybe you can hire the A-Team" );
        //can_ok( $class, 'ba' );

    }
} // end class FileFindRuleTest

function basename_length_eq_6($path, $basename)
{
    return (strlen($basename) == 6 ? true : false);
}

function basename_length_gt_6($path, $basename)
{
    return (strlen($basename) > 6 ? true : false);
}

function basename_length_eq_6_or_gt_10($path, $basename)
{
    $length = strlen($basename);
    return ($length == 6 || $length > 10 ? true : false);
}

function foobar_full_path($path, $basename)
{
    return ($path == getcwd() && $basename == 'foobar' ? true : false);
}

function ignore_cvs_svn_dirs($path, $basename)
{
    return (preg_match('/^(\.svn|CVS)$/', $basename) ? false : true);
}

