<?php
// $Id$

/**
 * @file
 * Moose
 *
 * @package Moose
 *
 * @author    David Naughton <nihiliad@gmail.com>
 * @copyright 2009 David Naughton <nihiliad@gmail.com>
 * @see       http://search.cpan.org/dist/Moose/
 * @version   0.0.1
 */
class Moose
{
    protected static $registry = array();

    public function __construct()
    {
        // If all params are optional, no params will be passed in,
        // so we use func_get_args to allow for that:
        // TODO: Add a test for this!!
        $args = func_get_args();
        if (is_array( $args[0] )) {
            // Assume in this case that args[0] is an array of property
            // key-value pairs.
            $params = $args[0];
        } else {
            // Assume in this case that the args include no property
            // key-value pairs.
            $params = array();
        }

        list($object, $class) = self::constructor_caller();
        //echo "class = $class\n";
        if (!array_key_exists($class, self::$registry)) {
            self::$registry[$class] = array();
            self::$registry[$class]['properties'] = array();

            // TODO: Much work remains to fully-support inheritance,
            // i.e. call properties() methods of all parents.
            call_user_func(array($class, 'properties'));
            /*
            echo "new registry = \n";
            print_r( self::$registry );
            echo "end new registry\n";
            */
        }

        // set values for all the properties
        foreach (self::$registry[$class]['properties'] as $property => $options) {
            $mutator = 'set_' . $property;

            if (array_key_exists($property, $params)) {
                $this->$mutator( $params[$property] );
                continue;
            }

            if (array_key_exists('builder', $options)) {
                $builder = $options['builder'];
                $this->$mutator( $this->$builder() );
                continue;
            }

            // TODO: What to do, if anything, if we have
            // both a builder and a default? See below!
            if (array_key_exists('default', $options)) {
                $this->$mutator( $options['default'] );
                continue;
            }

            if (array_key_exists('default_function', $options)) {
                $function = $options['default_function'];
                $this->$mutator( $function() );
                continue;
            }

            // Assume 'required' == 1 unless specified otherwise.
            if (
                array_key_exists('required', $options) &&
                $options['required'] == 0
            ) continue;

            throw new Exception("No value supplied for required property '$property'");
        }
        
        $ref_class = new ReflectionClass( $class );

        // See docs for Moose's BUILD method.
        // TODO: Much work remains to fully-support inheritance,
        // i.e. call BUILD() methods of all parents.
        if ($ref_class->hasMethod('BUILD')) {
            $this->BUILD( $args );
        }
    }
    
    /**
     * Generates properties, as well as getter and setter methods for them.
     *
     * @param 
     * @return 
     */
    public static function has($property, $options)
    {
        $class = self::properties_caller();
        //echo "has class = $class\n";

        foreach (array('reader','writer') as $accessor) {
            if (!array_key_exists($accessor, $options)) {
                $options[$accessor] = $accessor;
            }
        }

        self::$registry[$class]['properties'][$property] = $options;
        /*
        echo "has registry = \n";
        print_r( self::$registry );
        echo "end has registry\n";
        */
    }

    // Global function to find the calling class from within a static method

    function constructor_caller()
    {
        $trace = debug_backtrace();
        $object = $trace[1]['object'];    
        return array($object, get_class($object));
    }

    function properties_caller()
    {
        $trace = debug_backtrace();
        //echo "properties_caller() trace =\n";
        //print_r( $trace );
        foreach ($trace as $frame) {
            if ($frame['function'] == 'call_user_func' && $frame['args'][0][1] == 'properties') {
                return $frame['args'][0][0];
            }    
        }
        throw new Exception("Cannot find properties() calling class.");
    }

    public function __call($func, $args)
    {
        $class = get_class($this);
        $properties = self::$registry[$class]['properties'];

        // extract any property name that the $func may be referencing
        $property = $func;
        $method = 'reader';
        if (preg_match('/^set_(.*)$/', $func, $matches)) {
            $property = $matches[1];
            $method = 'writer';
        }

        // if it's an accessor/mutator method, call the implementation
        // referenced in the options
        if (array_key_exists($property, $properties)) {
            $options = $properties[$property];
            $accessor = $options[$method];
            return call_user_func_array(array($this, $accessor), array($property, $args));
        } else {
            /*
            echo "$class properties = \n";
            print_r( $properties );
            echo "\n";
            */
            throw new Exception("Method '$func' does not exist in class '$class'.");
        }
    }

    protected function reader($property)
    {
        // Added to eliminate the error...
        // PHP Notice:  Undefined property:  ISBN13::$input_isbn in /opt/ethicshare-dev/usr/local/share/php/Moose.php on line 176
        /* ... but now it's causing new errors! :( */
        $class = get_class($this);
        //echo "class = $class\n";
        /* The following won't work, because the reflection class only reports on properties
         * explicitly-defined in a class. It does NOT report on dynamically-created properties.
         */
        //$ref_class = new ReflectionClass( $class );

        $object_vars = get_object_vars($this);
        //echo "object_vars = "; var_export($object_vars); echo "\n";
        
        // This would work, too:
        /*
        $ref_object = new ReflectionObject( $this );
        echo "ref_object = "; var_export($ref_object); echo "\n";
        $properties = $ref_object->getProperties();
        echo "properties = "; var_export($properties); echo "\n";
        */
 
        // TODO: Make better use of defaults here for lazy loading!!!
        if (!array_key_exists($property, $object_vars)) {
            //echo "Property '$property' does not exist in this '$class' object.\n";
            $this->$property = NULL;
        }

        return $this->$property; // Formerly line 176.
    }

    protected function writer($property, $value_array)
    {
        $value = $value_array[0];

        $class = get_class($this);
        //echo "Called writer() on class '$class' for property '$property'.\n";
        $options = self::$registry[$class]['properties'][$property];

        if (array_key_exists('isa', $options)) {
            $isa = $options['isa'];
            if (is_array($isa) && !in_array($value, $isa)) {
                throw new Exception("Invalid value '$value' for property '$property'");
            } 
            if (is_string($isa)) {
                // Assume that isa is a class name:
                throw new Exception("isa class-checking not yet implemented!");
            }
        }

        $this->$property = $value;
        return $this;
    }

    protected function enum()
    {
        $args = func_get_args();
        if (!is_string($args[0])) {
            throw new Exception("First argument to enum() must be a string");
        }
        if (is_string($args[1])) {
            // Assume this is an array of strings:
            return $args;
        }
        if (is_array($args[1])) {
            // Assume we are creating a new Str type with a list of allowed values:
            throw new Exception("Creation of enum Str types not implemented yet!");
        }
    }


} // end class Moose

/*
// From: http://www.sitepoint.com/forums//showthread.php?t=416044
// Global function to find the calling class from within a static method

function get_calling_class() {

    $trace = debug_backtrace();

    for ($i = 1; $i < ($max = count($trace)); $i ++) {
        $item = $trace[$i];
        if (isset($trace[$i + 1]['class']) && get_parent_class($trace[$i + 1]['class']) == $item['class']) {
            continue;
        } else {
            break;
        }
    }

    $lines = file($item['file']);
    $line = $lines[$item['line'] -1];
    preg_match('@[a-z_][a-z0-9_]*(?=::' . $item['function'] . ')@i', $line, $matches);
    return $matches[0];
}

// useage

class Pappa {

    function nameCallingChild() {
        echo get_calling_class();
    }
}

class SonnyBoy extends Pappa {
}

SonnyBoy::nameCallingChild();
// echos "SonnyBoy" 

*/
