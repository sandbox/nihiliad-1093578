#!/usr/bin/php -q
<?php

require_once 'simpletest/autorun.php';
SimpleTest :: prefer(new TextReporter());
require_once '../php/Moose.php';

//error_reporting( E_STRICT );

class MooseClass extends Moose
{
    public static function properties()
    {
        self::has('foo', array('is' => 'protected',));
        self::has('bar', array('is' => 'protected', 'isa' => self::enum('fee','fye','foe','fum')));
        self::has('baz', array('is' => 'protected', 'default' => 'luhrmann',));
        self::has('manchu', array('is' => 'protected', 'required' => 0,));
        self::has('optional', array('is' => 'protected', 'required' => 0,));
        self::has('date', array('is' => 'protected', 'builder' => 'now',));
        self::has('kung',
             array(
                 'is' => 'protected',
                 'default_function' => create_function('', 'return new Foo();'),
             )
        );
    }

    public function now()
    {
        return date("Ymd");
    }

    protected function BUILD()
    {
        $this->set_manchu( 'fu' );
    }

} // end class MooseClass

// Only for testing purposes.
class MooseClassChild extends MooseClass { }
class Foo { } 

class MooseTest extends UnitTestCase
{

    function test_new()
    {
        $mc = new MooseClass(array('foo' => 1, 'bar' => 'fee'));  
        $this->assertIsA($mc, 'MooseClass');
        $this->mc = $mc;
    }

    function test_accessors()
    {
        $mc = $this->mc;
        $this->assertEqual( $mc->foo(), 1 );
        $this->assertEqual( $mc->bar(), 'fee' );
    }

    function test_mutators()
    {
        $mc = $this->mc;
        $mc->set_foo( 'baz' );
        $this->assertEqual( $mc->foo(), 'baz' );
    }

    function test_enum()
    {
        $mc = $this->mc;

        // Setting a disallowed value should throw an exception:
        $this->expectException();
        $mc->set_bar( 'hickory' );

        // Setting an allowed value should succeed:
        $mc->set_bar( 'fye' );
        $this->assertEqual( $mc->bar(), 'fye' );
    }

    function test_default_values()
    {
        $mc = $this->mc;
        $this->assertEqual( $mc->baz(), 'luhrmann' );
    }

    function test_default_functions()
    {
        $mc = $this->mc;
        $this->assertIsA( $mc->kung(), 'Foo' );
    }

    function test_builders()
    {
        $mc = $this->mc;
        $this->assertEqual( $mc->date(), date('Ymd') );
    }

    function test_BUILD()
    {
        $mc = $this->mc;
        $this->assertEqual( $mc->manchu(), 'fu' );
    }

    function test_nonexistent_methods()
    {
        $mc = $this->mc;
        $this->expectException();
        $mc->hello();
    }

    function test_optional_properties()
    {
        $mc = $this->mc;
        $optional = $mc->optional();
        $this->assertEqual( $optional, NULL );
    }

    function test_child_new()
    {
        $mcc = new MooseClassChild(array('foo' => 1, 'bar' => 'fee'));  
        $this->assertIsA($mcc, 'MooseClassChild');
        $this->mcc = $mcc;
    }

    function test_child_accessors()
    {
        $mcc = $this->mcc;
        $this->assertEqual( $mcc->foo(), 1 );
        $this->assertEqual( $mcc->bar(), 'fee' );
    }

    function test_child_mutators()
    {
        $mcc = $this->mcc;
        $mcc->set_foo( 'baz' );
        $this->assertEqual( $mcc->foo(), 'baz' );
    }

    function test_child_enum()
    {
        $mcc = $this->mcc;

        // Setting a disallowed value should throw an exception:
        $this->expectException();
        $mcc->set_bar( 'hickory' );

        // Setting an allowed value should succeed:
        $mcc->set_bar( 'fye' );
        $this->assertEqual( $mcc->bar(), 'fye' );
    }

    function test_child_default_values()
    {
        $mcc = $this->mcc;
        $this->assertEqual( $mcc->baz(), 'luhrmann' );
    }

    function test_child_default_functions()
    {
        $mcc = $this->mcc;
        $this->assertIsA( $mcc->kung(), 'Foo' );
    }

    function test_child_builders()
    {
        $mcc = $this->mcc;
        $this->assertEqual( $mcc->date(), date('Ymd') );
    }

    function test_child_BUILD()
    {
        $mcc = $this->mcc;
        $this->assertEqual( $mcc->manchu(), 'fu' );
    }

    function test_child_nonexistent_methods()
    {
        $mcc = $this->mcc;
        $this->expectException();
        $mcc->hello();
    }

} // end class MooseTest
