#!/usr/bin/php -q
<?php

require_once 'simpletest/autorun.php';
SimpleTest :: prefer(new TextReporter());
set_include_path('../php' . PATH_SEPARATOR . get_include_path());
require_once 'Text/Glob.php';

//error_reporting( E_STRICT );

class TextGlobTest extends UnitTestCase {
    
    function test() {
        
        $tg = new Text_Glob();
        $this->assertIsA( $tg, 'Text_Glob' );

    
        $regex = $tg->glob_to_regex( 'foo' );

        // All comments that follow are the original Perl version tests.    

        //ok( 'foo'    =~ $regex, "matched foo" );
        $this->assertTrue( preg_match($regex, 'foo') );
        //ok( 'foobar' !~ $regex, "didn't match foobar" );
        $this->assertFalse( preg_match($regex, 'foobar') );
    
        //ok(  match_glob( 'foo', 'foo'      ), "absolute string" );
        $this->assertTrue( $tg->match_glob('foo', 'foo') );
        //ok( !match_glob( 'foo', 'foobar'   ) );
        $this->assertFalse( $tg->match_glob('foo', 'foobar') );
    
        //ok(  match_glob( 'foo.*', 'foo.'     ), "* wildcard" );
        $this->assertTrue( $tg->match_glob('foo.*', 'foo.') );
        //ok(  match_glob( 'foo.*', 'foo.bar'  ) );
        $this->assertTrue( $tg->match_glob('foo.*', 'foo.bar') );
        //ok( !match_glob( 'foo.*', 'gfoo.bar' ) );
        $this->assertFalse( $tg->match_glob('foo.*', 'gfoo.bar') );
        
        //ok(  match_glob( 'foo.?p', 'foo.cp' ), "? wildcard" );
        $this->assertTrue( $tg->match_glob('foo.?p', 'foo.cp') );
        //ok( !match_glob( 'foo.?p', 'foo.cd' ) );
        $this->assertFalse( $tg->match_glob('foo.?p', 'foo.cd') );
        
        //ok(  match_glob( 'foo.{c,h}', 'foo.h' ), ".{alternation,or,something}" );
        $this->assertTrue( $tg->match_glob('foo.{c,h}', 'foo.h') );
        //ok(  match_glob( 'foo.{c,h}', 'foo.c' ) );
        $this->assertTrue( $tg->match_glob('foo.{c,h}', 'foo.c') );
        //ok( !match_glob( 'foo.{c,h}', 'foo.o' ) );
        $this->assertFalse( $tg->match_glob('foo.{c,h}', 'foo.o') );
        
        //ok(  match_glob( 'foo.\\{c,h}\\*', 'foo.{c,h}*' ), '\escaping' );
        $this->assertTrue( $tg->match_glob('foo.\\{c,h}\\*', 'foo.{c,h}*') );
        //ok( !match_glob( 'foo.\\{c,h}\\*', 'foo.\\c' ) );
        $this->assertFalse( $tg->match_glob('foo.\\{c,h}\\*', 'foo.\\c') );
        
        //ok(  match_glob( 'foo.(bar)', 'foo.(bar)'), "escape ()" );
        $this->assertTrue( $tg->match_glob('foo.(bar)', 'foo.(bar)') );
        
        //ok( !match_glob( '*.foo',  '.file.foo' ), "strict . rule fail" );
        $this->assertFalse( $tg->match_glob('*.foo', '.file.foo') );
        //ok(  match_glob( '.*.foo', '.file.foo' ), "strict . rule match" );
        $this->assertTrue( $tg->match_glob('.*.foo', '.file.foo') );
        //{
        //local $Text::Glob::strict_leading_dot;
        //ok(  match_glob( '*.foo', '.file.foo' ), "relaxed . rule" );
        //}
        $tg->set_strict_leading_dot( false );
        $this->assertTrue( $tg->match_glob('*.foo', '.file.foo') );
        $tg->set_strict_leading_dot( true );
        
        //ok( !match_glob( '*.fo?',   'foo/file.fob' ), "strict wildcard / fail" );
        $this->assertFalse( $tg->match_glob('*.fo?', 'foo/file.fob') );
        //ok(  match_glob( '*/*.fo?', 'foo/file.fob' ), "strict wildcard / match" );
        $this->assertTrue( $tg->match_glob('*/*.fo?', 'foo/file.fob') );
        //{
        //local $Text::Glob::strict_wildcard_slash;
        //ok(  match_glob( '*.fo?', 'foo/file.fob' ), "relaxed wildcard /" );
        //}
        $tg->set_strict_wildcard_slash( false );
        $this->assertTrue( $tg->match_glob('*.fo?', 'foo/file.fob') );
        $tg->set_strict_wildcard_slash( true );
        
        //ok( !match_glob( 'foo/*.foo', 'foo/.foo' ), "more strict wildcard / fail" );
        $this->assertFalse( $tg->match_glob('foo/*.foo', 'foo/.foo') );
        //ok(  match_glob( 'foo/.f*',   'foo/.foo' ), "more strict wildcard / match" );
        $this->assertTrue( $tg->match_glob('foo/.f*', 'foo/.foo') );
        //{
        //local $Text::Glob::strict_wildcard_slash;
        //ok(  match_glob( '*.foo', 'foo/.foo' ), "relaxed wildcard /" );
        //}
        $tg->set_strict_wildcard_slash( false );
        $this->assertTrue( $tg->match_glob('*.foo', 'foo/.foo') );
        $tg->set_strict_wildcard_slash( true );
        
        //ok(  match_glob( 'f+.foo', 'f+.foo' ), "properly escape +" );
        $this->assertTrue( $tg->match_glob('f+.foo', 'f+.foo') );
        //ok( !match_glob( 'f+.foo', 'ffff.foo' ) );
        $this->assertFalse( $tg->match_glob('f+.foo', 'ffff.foo') );
        
        //ok(  match_glob( "foo\nbar", "foo\nbar" ), "handle embedded \\n" );
        $this->assertTrue( $tg->match_glob("foo\nbar", "foo\nbar") );
        //ok( !match_glob( "foo\nbar", "foobar" ) );
        $this->assertFalse( $tg->match_glob("foo\nbar", "foobar") );
        
        //ok(  match_glob( 'test[abc]', 'testa' ), "[abc]" );
        $this->assertTrue( $tg->match_glob('test[abc]', 'testa') );
        //ok(  match_glob( 'test[abc]', 'testb' ) );
        $this->assertTrue( $tg->match_glob('test[abc]', 'testb') );
        //ok(  match_glob( 'test[abc]', 'testc' ) );
        $this->assertTrue( $tg->match_glob('test[abc]', 'testc') );
        //ok( !match_glob( 'test[abc]', 'testd' ) );
        $this->assertFalse( $tg->match_glob('test[abc]', 'testd') );
        
        //ok(  match_glob( 'foo$bar.*', 'foo$bar.c'), "escaping \$" );
        $this->assertTrue( $tg->match_glob('foo$bar.*', 'foo$bar.c') );
        
        //ok(  match_glob( 'foo^bar.*', 'foo^bar.c'), "escaping ^" );
        $this->assertTrue( $tg->match_glob('foo^bar.*', 'foo^bar.c') );
        
        //ok(  match_glob( 'foo|bar.*', 'foo|bar.c'), "escaping |" );
        $this->assertTrue( $tg->match_glob('foo|bar.*', 'foo|bar.c') );
        
        
        //ok(  match_glob( '{foo,{bar,baz}}', 'foo'), "{foo,{bar,baz}}" );
        $this->assertTrue( $tg->match_glob('{foo,{bar,baz}}', 'foo') );
        //ok(  match_glob( '{foo,{bar,baz}}', 'bar') );
        $this->assertTrue( $tg->match_glob('{foo,{bar,baz}}', 'bar') );
        //ok(  match_glob( '{foo,{bar,baz}}', 'baz') );
        $this->assertTrue( $tg->match_glob('{foo,{bar,baz}}', 'baz') );
        //ok( !match_glob( '{foo,{bar,baz}}', 'foz') );
        $this->assertFalse( $tg->match_glob('{foo,{bar,baz}}', 'foz') );
        
        //ok(  match_glob( 'foo@bar', 'foo@bar'), '@ character');
        $this->assertTrue( $tg->match_glob('foo@bar', 'foo@bar') );
        //ok(  match_glob( 'foo$bar', 'foo$bar'), '$ character');
        $this->assertTrue( $tg->match_glob('foo$bar', 'foo$bar') );
        //ok(  match_glob( 'foo%bar', 'foo%bar'), '% character');
        $this->assertTrue( $tg->match_glob('foo%bar', 'foo%bar') );
    }
} // end class TextGlobTest
