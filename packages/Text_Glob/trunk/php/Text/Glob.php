<?php
// $Id$

/**
 * @file
 * Match globbing patterns against text.
 *
 * Text_Glob implements glob(3) style matching that can be used to match
 * against text, rather than fetching names from a filesystem.
 *
 * <h2>Synopsis</h2>
 *
 * @code
 *
 * require_once 'Text/Glob.php';
 * $tg = new Text_Glob();
 *   
 * if ($tg->match_glob("foo.*", "foo.bar")) echo "matched\n";
 *
 * // prints foo.bar and foo.baz
 * $regex = $tg->glob_to_regex("foo.*");
 * foreach (array('foo.bar', 'foo.baz', 'foo', 'bar') as $string) {
 *     if (preg_match($regex, $string))
 *         echo "matched: $string\n";
 * }
 *
 * @endcode
 *
 * <h1>Syntax</h1>
 *
 * The following metacharacters and rules are respected.
 *
 * - * - match zero or more characters
 *   a* matches a, aa, aaaa and many many more.
 * - ? - match exactly one character
 *   a? matches aa, but not a, or aa
 * - Character sets/ranges
 *   - example.[ch] matches example.c and example.h
 *   - demo.[a-c] matches demo.a, demo.b, and demo.c
 * - alternation
 *   example.{foo,bar,baz} matches example.foo, example.bar, and example.baz
 * - leading . must be explictly matched
 *   *.foo does not match .bar.foo. For this you must either specify the
 *   leading . in the glob pattern (.*.foo), or call set_strict_leading_dot(false).
 * - and ? do not match /
 *   *.foo does not match bar/baz.foo. For this you must either explicitly
 *   match the / in the glob (*\/*.foo), or call set_strict_wildcard_slash(false).
 *
 * <h1>Blame</h1>
 *
 * Based on the Perl Text::Glob module, by Richard Clamp, and available on
 * CPAN: http://search.cpan.org/dist/Text-Glob/lib/Text/Glob.pm
 *
 * Partially ported to PHP for the symfony project by Fabien Potencier, as part
 * of the sfFinder class. Port completed and broken out into its own package
 * by David Naughton.
 *
 * @package Text_Glob
 *
 * @author     David Naughton <nihiliad@gmail.com>
 * @author     Fabien Potencier <fabien.potencier@gmail.com>
 * @author     Richard Clamp <richardc@unixbeard.net>
 * @copyright  2009 David Naughton <nihiliad@gmail.com>
 * @copyright  2004-2005 Fabien Potencier <fabien.potencier@gmail.com>
 * @copyright  2002, 2003, 2006, 2007 Richard Clamp <richardc@unixbeard.net>
 * @version    1.0.0
 */
class Text_Glob
{
    protected $strict_leading_dot = true;
    /**
     * By default strict_leading_dot is true, and a leading . must be explictly
     * matched. Pass false to this method if you don't want that behavior.
     *
     * @param $boolean
     * @see strict_leading_dot()
     */
    public function set_strict_leading_dot($boolean)
    {
        $this->strict_leading_dot = $boolean;
    }
    /**
     * @return $boolean
     * @see set_strict_leading_dot()
     */
    public function strict_leading_dot()
    {
        return $this->strict_leading_dot;
    }

    protected $strict_wildcard_slash = true;
    /**
     * By default strict_wildcard_slash is true, and * and ? do not match /.
     * Pass false to this method if you don't want that behavior.
     *
     * @param $boolean
     * @see strict_wildcard_slash()
     */
    public function set_strict_wildcard_slash($boolean)
    {
        $this->strict_wildcard_slash = $boolean;
    }
    /**
     * @return $boolean
     * @see set_strict_wildcard_slash()
     */
    public function strict_wildcard_slash()
    {
        return $this->strict_wildcard_slash;
    }

    /**
     * Returns a regex, with the uppercase S (study) modifier, which is the 
     * equiavlent of the globbing pattern.
     *
     * Using the S modifier seems to be as close as we can get in PHP to
     * returning a compiled regex like the Perl version does.
     *
     * @param $glob
     * @return $regex
     */
    public function glob_to_regex($glob) {
        $regex = $this->glob_to_regex_string($glob);
        return $regex . "S";
    }

    /**
     * Returns a regex string, without the uppercase S (study) modifier, which
     * is the equiavlent of the globbing pattern.
     *
     * @param $glob
     * @return $regex
     */
    public function glob_to_regex_string($glob)
    {
        $first_byte = true;
        $escaping = false;
        $in_curlies = 0;
        $regex = '';

        $chars = str_split($glob);
        foreach ($chars as $char) {
            if ($first_byte) {
                if ($this->strict_leading_dot() && $char != '.') {
                    $regex .= '(?=[^\.])';
                }
                $first_byte = false;
            }
            if ($char == '/') {
                $first_byte = true;
            }
            if ($char == '.' || $char == '(' || $char == ')' || $char == '|' || $char == '+' || $char == '^' || $char == '$') {
                $regex .= "\\$char";
            } else if ($char == '*') {
                $regex .= ($escaping ? "\\*" : ($this->strict_wildcard_slash() ? "[^/]*" : ".*"));
            } else if ($char == '?') {
                $regex .= ($escaping ? "\\?" : ($this->strict_wildcard_slash() ? "[^/]" : "."));
            } else if ($char == '{') {
                $regex .= ($escaping ? "\\{" : "(");
                if (!$escaping) ++$in_curlies;
            } else if ($char == '}' && $in_curlies) {
                $regex .= ($escaping ? "}" : ")");
                if (!$escaping) --$in_curlies;
            } else if ($char == ',' && $in_curlies) {
                $regex .= ($escaping ? "," : "|");
            } else if ($char == "\\") {
                if ($escaping) {
                    $regex .= "\\\\";
                    $escaping = false;
                } else {
                    $escaping = true;
                }
                continue;
            } else {
                $regex .= $char;
                $escaping = false;
            }
            $escaping = false;
        }
        return "#^$regex$#";
    }

    /**
     * Returns the list of things which match the glob from the source list.
     *
     * @param $glob
     * @param $sources
     *   Sources may be a single string, a comma-separated list of strings, 
     *   or an array of strings. 
     * @return $matches
     *   An array of the $sources that matched the $glob.
     */
    public function match_glob()
    {
        $args = func_get_args();
        $glob = array_shift($args);
        $strings = array();
        foreach ($args as $string) {
            $strings += (array)$string;
        }
        $regex = $this->glob_to_regex($glob);
        return preg_grep($regex, $strings);
    }
} // end class Text_Glob
