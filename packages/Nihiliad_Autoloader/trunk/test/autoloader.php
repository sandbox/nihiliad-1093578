#!/usr/bin/php -q
<?php

require_once 'simpletest/autorun.php';
SimpleTest :: prefer(new TextReporter());
set_include_path('../php' . PATH_SEPARATOR . get_include_path());
require_once 'Nihiliad/Autoloader.php';

ini_set('memory_limit', '512M');

//error_reporting( E_STRICT );

function __autoload( $class )
{
    // Exists only to test that it doesn't get clobbered.
}

class AutoloaderTest extends UnitTestCase
{
    public function testInitialize()
    {
        Nihiliad_Autoloader::destroy();

        $oldAutoloaders = spl_autoload_functions();
        // In some cases, __autoload wasn't showing up in this array. Make sure that doesn't happen again:
        $this->assertEqual($oldAutoloaders, array('__autoload',));

        Nihiliad_Autoloader::initialize();
        $newAutoloaders = spl_autoload_functions();

        // Make sure our new autoloader got registered...
        $this->assertTrue(
            in_array(array('Nihiliad_Autoloader','load'), $newAutoloaders)
        );
        // ...and make sure the old ones are still there:
        $this->assertTrue(
            in_array('__autoload', $newAutoloaders)
        );

        $extensions = Nihiliad_Autoloader::extensions();
        $this->assertEqual($extensions, array('.php',));

        // The parent directory of Nihiliad/Autolader.php should be included in the default paths:
        $cwd = explode(DIRECTORY_SEPARATOR, getcwd());
        $cwd[count($cwd)-1] = 'php';
        $expectedPaths = array(implode(DIRECTORY_SEPARATOR, $cwd));
        $paths = Nihiliad_Autoloader::paths();
        $this->assertEqual($paths, $expectedPaths);

        // Destroy should remove our autoloader from spl_autoload_functions:
        Nihiliad_Autoloader::destroy();
        $resetAutoloaders = spl_autoload_functions();
        $this->assertEqual($resetAutoloaders, $oldAutoloaders);

        // Test overriding of defaults:
        $pathOverrides = array('php','./php-fu');
        $extensionOverrides = array('.class','.inc');
        Nihiliad_Autoloader::initialize(array(
            'paths' => $pathOverrides,
            'overrideDefaultPaths' => true,
            'extensions' => $extensionOverrides,
            'overrideDefaultExtensions' => true,
        ));
        $paths = Nihiliad_Autoloader::paths();
        $this->assertEqual($paths, $pathOverrides);
        $extensions = Nihiliad_Autoloader::extensions();
        $this->assertEqual($extensions, $extensionOverrides);

    }

    public function testLoad()
    {
        Nihiliad_Autoloader::destroy();

        /* Since the following generates a fatal error, can't test
         * this with SimpleTest. Grrrr...
         * $this->expectException();
         * $foo = new Foo();
         */

        Nihiliad_Autoloader::initialize(array(
            'paths' => './php',
            'extensions' => array('.inc','.class'),
        ));

        // ./php/Foo.php
        $foo = new Foo();
        $this->assertIsA($foo, 'Foo');

        // ./php/Foo/Bar.inc
        $foobar = new Foo_Bar();
        $this->assertIsA($foobar, 'Foo_Bar');

        // ./php/Foo/Bar/Baz.class
        $foobarbaz = new Foo_Bar_Baz();
        $this->assertIsA($foobarbaz, 'Foo_Bar_Baz');

        Nihiliad_Autoloader::addPaths('./php-fu');

        // ./php-fu/Fu.php
        $fu = new Fu();
        $this->assertIsA($fu, 'Fu');

        Nihiliad_Autoloader::addExtensions(array('.class.php','.module'));

        // ./php-fu/Fu/Bar.class.php
        $fubar = new Fu_Bar();
        $this->assertIsA($fubar, 'Fu_Bar');

        // ./php-fu/Fu/Bar/Baz.module
        $fubarbaz = new Fu_Bar_Baz();
        $this->assertIsA($fubarbaz, 'Fu_Bar_Baz');
    }

    function testPathPatterns()
    {
        Nihiliad_Autoloader::destroy();
        Nihiliad_Autoloader::initialize(array(
            'paths' => array(
                './pattern-dirs/#autoload\d+#',

                // ./pattern-dirs/autoload-subdir-1/ should not get included,
                // because it has no subdirectories:
                './pattern-dirs/#autoload-subdir-\d+#/#^[a-z]+$#',

                // ./pattern-dirs/autoload-subdir-3/bar/baz/ should match,'
                // ./pattern-dirs/autoload-subdir-2/bar/baz/ should not, because it doesn't exist:
                './pattern-dirs/#autoload-subdir-\d+#/#^[a-z]+$#/baz',

                // None of the rest should match, and not even partial paths should get included:
                // Note: When these properly fail to match, the autoloader will throw a warning.
                // TODO: Does simpletest allow to test for warnings?
                './pattern-dirs/#bogus#',
                './pattern-dirs/#bogus#/#^[a-z]+$#',
                '#bogus#',
                './bogus',
                'bogus',
            ),
        ));

        $expectedPaths = array (
            '/home/naughton/svn/dldl/php-packages/Nihiliad_Autoloader/trunk/php',
            './pattern-dirs/autoload1/',
            './pattern-dirs/autoload2/',
            './pattern-dirs/autoload-subdir-2/foo/',
            './pattern-dirs/autoload-subdir-3/bar/',
            './pattern-dirs/autoload-subdir-3/bar/baz/',
        );
        $this->assertEqual(Nihiliad_Autoloader::paths(), $expectedPaths);
    }

}
