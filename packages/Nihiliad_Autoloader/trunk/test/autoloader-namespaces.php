#!/usr/bin/php -q
<?php

require_once 'simpletest/autorun.php';
SimpleTest :: prefer(new TextReporter());
set_include_path('../php' . PATH_SEPARATOR . get_include_path());
require_once 'Nihiliad/Autoloader.php';

ini_set('memory_limit', '512M');

//error_reporting( E_STRICT );

class AutoloaderNamespacesTest extends UnitTestCase
{
    public function testLoad()
    {
        Nihiliad_Autoloader::destroy();
        Nihiliad_Autoloader::initialize(array(
            'paths' => './php-namespaces',
        ));

        // ./php-namespaces/Phoo/Bar.php
        $phoobar = new Phoo\Bar();
        $this->assertIsA($phoobar, 'Phoo\Bar');

        // ./php/Foo/Bar/Baz.php
        $phoobarbaz = new Phoo\Bar\Baz();
        $this->assertIsA($phoobarbaz, 'Phoo\Bar\Baz');
    }

}
