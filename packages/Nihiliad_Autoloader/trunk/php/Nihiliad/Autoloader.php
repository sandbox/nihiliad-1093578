<?php

// Heavily influenced by \PEAR2\Autoloader v. 0.2.3.

if (!class_exists('Nihiliad_Autoloader', false)) {
    // TODO: Once I know I can rely on PHP >= 5.3.0, make 'Nihiliad' a namespace.
    class Nihiliad_Autoloader
    {
        /**
         * Has this class's "load" method has been spl_autoload_register-ed?
         * 
         * @var bool
         */
        protected static $registered = false;

        /**
         * Array of paths within which the autoloader will search for class files.
         * 
         * @var array
         */
        protected static $paths = array();

        /**
         * Returns the array of paths within which the autoloader will search for class files.
         * 
         * @return array
         */
        static function paths()
        {
            return self::$paths;
        }

        /**
         * Default array of paths within which the autoloader will search for class files.
         * 
         * @var array
         */
        protected static $defaultPaths;

        /**
         * Returns the default array of paths within which the autoloader will search for class files.
         * 
         * @return array
         */
        static function defaultPaths()
        {
            if (!isset(self::$defaultPaths)) {
                self::$defaultPaths = array(dirname(__DIR__));
            }
            return self::$defaultPaths;
        }

        /**
         * Add paths within which the autoloader will search for class files.
         * 
         * @param array $paths The directories to add. Also accepts a scalar containing a single directory.
         * 
         * @return void
         */
        static function addPaths($paths)
        {
            if (!is_array($paths)) {
                $paths = array($paths);
            }

            // Expand any regexes in the paths to actual paths on the file system:
            $expandedPaths = array();
            foreach ($paths as $path) {
                if (!preg_match('/#/', $path)) {
                    $expandedPaths[] = $path;
                    continue;
                }
                $dirs = explode(DIRECTORY_SEPARATOR, $path);

                $protoPaths = array();

                // TODO: Handle cases where a $dir does not exist, and/or is not readable!
                foreach ($dirs as $dir) {
                    // If the path starts with a DIRECTORY_SEPARATOR, the first element created
                    // by the explode() above will be null:
                    if (null == $dir) $dir = '';

                    // There's no pattern here, so assume it's a hard-coded directory and move on:
                    if (!preg_match('/^#.+#$/', $dir)) {
                        $dir .= DIRECTORY_SEPARATOR;
                        if (count($protoPaths) == 0) {
                            $protoPaths = array($dir);
                        } else {
                            for ($i = 0; $i < count($protoPaths); $i++) {
                                $pathSoFar = $protoPaths[$i] . $dir;
                                // Since previous directories in the path may have been regexes,
                                // this pathSoFar may not really exist:
                                if (is_dir($pathSoFar)) {
                                    $protoPaths[$i] = $pathSoFar;
                                }
                            }
                        }
                        continue;
                    }

                    // We found a pattern, so expand it and modify the protoPaths(): 
                    $pattern = $dir;
                    $newProtoPaths = array();
                    foreach ($protoPaths as $protoPath) {
                        foreach (new DirectoryIterator($protoPath) as $item) {
                            $name = $item->getFilename();
                            $pathSoFar =  $protoPath . $name . DIRECTORY_SEPARATOR;
                            if (preg_match($pattern, $name) && is_dir($pathSoFar)) {
                                $newProtoPaths[] = $pathSoFar;
                            }
                        }
                    }
                    $protoPaths = $newProtoPaths;
                }
                if (count($protoPaths) == 0) {
                    error_log("No directories found that match regex path '$path'.");
                }
                foreach ($protoPaths as $protoPath) {
                    if (!in_array($protoPath, $expandedPaths)) {
                        $expandedPaths[] = $protoPath;
                    }
                }
            }

            foreach ($expandedPaths as $path) {
                if (!is_dir($path)) {
                    error_log("Path '$path' is not a diectory. Skipping.");
                    continue;
                }
                if (!(is_readable($path))) {
                    error_log("Path '$path' is not readable. Skipping.");
                    continue;
                }
                if (!in_array($path, self::$paths)) {
                    self::$paths[] = $path;
                }
            }
        }

        /**
         * Array of file name extensions to append to class names when coverting them to file names.
         * 
         * @var array
         */
        protected static $extensions = array();

        /**
         * Returns the array of file name extensions to append to class names when coverting them to file names.
         * 
         * @return array
         */
        static function extensions()
        {
            return self::$extensions;
        }

        /**
         * Default array of file name extensions to append to class names when coverting them to file names.
         * 
         * @var array
         */
        protected static $defaultExtensions;

        /**
         * Returns the default array of file name extensions to append to class names when coverting them to file names.
         * 
         * @return array
         */
        static function defaultExtensions()
        {
            if (!isset(self::$defaultExtensions)) {
                self::$defaultExtensions = array('.php');
            }
            return self::$defaultExtensions;
        }

        /**
         * Add file name extensions to the list of extensions to append to class names when coverting them to file names.
         * 
         * @param array $extensions The extensions to add. Also accepts a scalar containing a single extension.
         * 
         * @return void
         */
        static function addExtensions($extensions)
        {
            if (!is_array($extensions)) {
                $extensions = array($extensions);
            }
            foreach ($extensions as $extension) {
                if (!in_array($extension, self::$extensions)) {
                    self::$extensions[] = $extension;
                }
            }
        }

        /**
         * Initialize the autoloader.
         * 
         * @params array $params
         *   'paths': List of directories within which the autoloader will search for class files. Optional. Value may be an array or a scalar.
         *   'overrideDefaultPaths': Use only the paths passed into this method, excluding the default paths. Ignored if no paths are passed in. Boolean.
         *   'extensions': List of extensions to append to class names when coverting them to file names. Optional. Value may be an array or a scalar.
         *   'overrideDefaultExtensions': Use only the extensions passed into this method, excluding the default extensions. Ignored if no extensions are passed in. Boolean.
         * 
         * @return void
         */
        static function initialize(array $params=null)
        {
            // defaults
            $paths = self::defaultPaths();
            $extensions = self::defaultExtensions();

            if (isset($params)) {
                if (array_key_exists('paths', $params)) {
                    if (!is_array($params['paths'])) {
                        $params['paths'] = array($params['paths']);
                    }
                    if (array_key_exists('overrideDefaultPaths', $params) && true == $params['overrideDefaultPaths']) {
                        $paths = array();
                    }
                    $paths = array_merge($paths, $params['paths']);
                }
    
                if (array_key_exists('extensions', $params)) {
                    if (!is_array($params['extensions'])) {
                        $params['extensions'] = array($params['extensions']);
                    }
                    if (array_key_exists('overrideDefaultExtensions', $params) && true == $params['overrideDefaultExtensions']) {
                        $extensions = array();
                    }
                    $extensions = array_merge($extensions, $params['extensions']);
                }
            }

            self::register();
            self::addPaths($paths);
            self::addExtensions($extensions);
        }

        /**
         * Register this autoloader class method with spl_autoload_register.
         * 
         * @return void
         */
        protected static function register()
        {
            if (self::$registered) return;

            // Set up __autoload:
            $autoloaders = spl_autoload_functions();
            //echo "in register(): "; var_dump($autoloaders); echo "\n";

            // Do I need to hard-code the class name here, or can I depend on __CLASS__ being available?
            //spl_autoload_register('Nihiliad_Autoloader::load');
            spl_autoload_register(__ClASS__ . '::load');

            /* None of the following, copied from/inspired by \PEAR2\Autoload, works in my tests. Don't know why.
            //if (function_exists('__autoload') && ($autoload_functions === false)) {
            if (function_exists('__autoload') && (!in_array('__autoload', $autoloaders))) {
                // __autoload() was being used, but now would be ignored, so add
                // it to the autoload stack:
                spl_autoload_register('__autoload');
            }
            */

            // Unlike the above, this does work, makes sure that '__autoload' doesn't get removed from the list:
            if (is_array($autoloaders)) { // If spl_autoload_functions is empty, it returns false, not an empty array. Grrr....
                foreach ($autoloaders as $autoloader) {
                    spl_autoload_register($autoloader);
                }
            }
            self::$registered = true;
        }

        /**
         * Unregister this autoloader class method with spl_autoload_register.
         * 
         * @return void
         */
        protected static function unregister()
        {
            if (!self::$registered) return;
            // Do I need to hard-code the class name here, or can I depend on __CLASS__ being available?
            //spl_autoload_unregister('Nihiliad_Autoloader::load');
            spl_autoload_unregister(__ClASS__ . '::load');
            self::$registered = false;
        }

        /**
         * Remove this autoloader from spl_autoload_functions and reset the paths and extensions to empty arrays. Named "destroy" because you probably do not want to call this in production.
         * 
         * @return void
         */
        static function destroy()
        {
            self::$paths = array();
            self::$extensions = array();
            self::unregister();
        }

        /**
         * Load the given class.
         * 
         * @param string $class The class to load.
         * 
         * @return bool
         */
        static function load($class)
        {
            // Do I want to make this namespace specific, like PEAR2 does? Don't think so.
            /*
            if (strtolower(substr($class, 0, 6)) !== 'pear2\\') {
                return false;
            }
            */
            $file = str_replace(array('_', '\\'), DIRECTORY_SEPARATOR, $class);
            foreach (self::$paths as $path) {
                foreach (self::$extensions as $extension) {
                    unset($file_plus_ext);
                    $file_plus_ext = $path . DIRECTORY_SEPARATOR . $file . $extension;
                    if (file_exists($file_plus_ext)) {
                        require $file_plus_ext;
                        if (!class_exists($class, false) && !interface_exists($class, false)) {
                            // Throw an exception here, or somewhere close by, once we know we can rely on PHP >= 5.3.0?
                            /*
                            die(new \Exception('Class ' . $class . ' was not present in ' .
                                $path . DIRECTORY_SEPARATOR . $file .
                                '") [PEAR2_Autoload-0.2.3]'));
                            */
                            error_log("Class '$class' was not present in file '$file_plus_ext'.");
                        }
                        return true;
                    }
                }
            }
            return false;
            // Throw an exception here, or somewhere close by, once we know we can rely on PHP >= 5.3.0?
            /*
            $e = new \Exception('Class ' . $class . ' could not be loaded from ' .
                $file . ', file does not exist (registered paths="' .
                implode(PATH_SEPARATOR, self::$paths) .
                '") [PEAR2_Autoload-0.2.3]');
            $trace = $e->getTrace();
            if (isset($trace[2]) && isset($trace[2]['function']) &&
                  in_array($trace[2]['function'], array('class_exists', 'interface_exists'))) {
                return false;
            }
            if (isset($trace[1]) && isset($trace[1]['function']) &&
                  in_array($trace[1]['function'], array('class_exists', 'interface_exists'))) {
                return false;
            }
            die ((string) $e);
            */
        }
    }
}
