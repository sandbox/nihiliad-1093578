<?php

require_once 'simplepie.inc';

class LocalFeed_Item_Iterator implements Iterator
{
    protected $items = array();
    
      /**
      * A switch to keep track of the end of the array
      */
    protected $valid = FALSE;
    
    function __construct($params)
    {
    
        $feed = new SimplePie();
        //$feed->set_feed_url('http://www.nytimes.com/services/xml/rss/nyt/Health.xml');
        $feed->set_feed_url( $params['feed_url'] );
    
        // Must have php5-curl installed!! fsockopen fails 
        // with gzip decompression errors.
        //$feed->force_fsockopen(true);
    
        $feed->init();
    
        if ($feed->error()) {
            throw new Exception( $feed->error() );
        }
    
        // NYT content/descriptions always seem to have ads and images:
        $feed->strip_htmltags( $params['strip_html_tags'] );
    
        $this->items = $feed->get_items();
    }
    
      /**
      * Return the array "pointer" to the first element
      * PHP's reset() returns false if the array has no elements
      */
    function rewind()
    {
        $this->valid = (FALSE !== reset($this->items));
    }
    
      /**
      * Return the current array element
      */
    function current()
    {
        return current($this->items);
    }
    
      /**
      * Return the key of the current array element
      */
    function key()
    {
        return key($this->items);
    }
    
      /**
      * Move forward by one
      * PHP's next() returns false if there are no more elements
      */
    function next()
    {
        $this->valid = (FALSE !== next($this->items));
    }
    
      /**
      * Is the current element valid?
      */
    function valid()
    {
        return $this->valid;
    }

} # end class LocalFeed_Item_Iterator
