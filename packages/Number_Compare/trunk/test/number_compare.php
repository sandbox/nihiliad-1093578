#!/usr/bin/php -q
<?php

require_once 'simpletest/autorun.php';
SimpleTest :: prefer(new TextReporter());
set_include_path('../php' . PATH_SEPARATOR . get_include_path());
require_once 'Number/Compare.php';

//error_reporting( E_STRICT );

class NumberCompareTest extends UnitTestCase {
    
    function test() {
    
        // Most comments that follow are lines from the original Perl tests.

        $c = new Number_Compare('>20');
        $this->assertIsA( $c, 'Number_Compare' );

        //ok(  $c->test(21), ">20" );
        $this->assertTrue( $c->test(21) );
        //ok( !$c->test(20) );
        $this->assertFalse( $c->test(20) );
        //ok( !$c->test(19) );
        $this->assertFalse( $c->test(19) );
        
        //$c = Number::Compare->new('<20');
        $c = new Number_Compare('<20');
        //ok( !$c->test(21), "<20" );
        $this->assertFalse( $c->test(21) );
        //ok( !$c->test(20) );
        $this->assertFalse( $c->test(20) );
        //ok(  $c->test(19) );
        $this->assertTrue( $c->test(19) );
        
        //$c = Number::Compare->new('>=20');
        $c = new Number_Compare('>=20');
        //ok(  $c->test(21), ">=20" );
        $this->assertTrue( $c->test(21) );
        //ok(  $c->test(20) );
        $this->assertTrue( $c->test(20) );
        //ok( !$c->test(19) );
        $this->assertFalse( $c->test(19) );
        
        //$c = Number::Compare->new('<=20');
        $c = new Number_Compare('<=20');
        //ok( !$c->test(21), "<=20" );
        $this->assertFalse( $c->test(21) );
        //ok(  $c->test(20) );
        $this->assertTrue( $c->test(20) );
        //ok(  $c->test(19) );
        $this->assertTrue( $c->test(19) );
        
        //$c = Number::Compare->new('20');
        $c = new Number_Compare('20');
        //ok( !$c->test(21), "== 20" );
        $this->assertFalse( $c->test(21) );
        //ok(  $c->test(20) );
        $this->assertTrue( $c->test(20) );
        //ok( !$c->test(19) );
        $this->assertFalse( $c->test(19) );
        
        // well that's all the comparisons done, we'll not repeat that for each
        // of the magnitudes though
        
        //ok( Number::Compare->new("2K")->test(        2_000), "K" );
        $c = new Number_Compare('2K');
        $this->assertTrue( $c->test(2000) );
        //ok( Number::Compare->new("2M")->test(    2_000_000), "M" );
        $c = new Number_Compare('2M');
        $this->assertTrue( $c->test(2000000) );
        //ok( Number::Compare->new("2G")->test(2_000_000_000), "G" );
        $c = new Number_Compare('2G');
        $this->assertTrue( $c->test(2000000000) );
        
        //ok( Number::Compare->new("2Ki")->test(        2_048), "Ki" );
        $c = new Number_Compare('2Ki');
        $this->assertTrue( $c->test(2048) );
        //ok( Number::Compare->new("2Mi")->test(    2_097_152), "Mi" );
        $c = new Number_Compare('2Mi');
        $this->assertTrue( $c->test(2097152) );
        //ok( Number::Compare->new("2Gi")->test(2_147_483_648), "Gi" );
        $c = new Number_Compare('2Gi');
        $this->assertTrue( $c->test(2147483648) );
        
        //# okay, how about if we become a blessed coderef
        //
        //ok( Number::Compare->new("1Ki")->(1024), "directly call the coderef" );
        //
        //# expose parse_to_perl
        //
        //is( Number::Compare->parse_to_perl(">1Ki"), '> 1024', "->parse_to_perl" );
    
    }

} // end class NumberCompareTest
