<?php
// $Id$

/**
 * @file
 * Numeric comparisons.
 *
 * Number_Compare understands magnitudes. The target value may use magnitudes
 * of kilobytes (k, ki), megabytes (m, mi), or gigabytes (g, gi). Those suffixed
 * with an i use the appropriate 2**n version in accordance with the IEC 
 * standard: http://physics.nist.gov/cuu/Units/binary.html
 *
 * <h2>Synopsis</h2>
 *
 * @code
 * require_once 'Number/Compare.php';
 * $c = new Number_Compare('>1Ki');
 * $c->test(1025); // is 1025 > 1024
 * @endcode
 *
 * <h1>Blame</h1>
 *
 * Based on the Perl Number::Compare module, by Richard Clamp, and available on
 * CPAN: http://search.cpan.org/dist/Number-Compare/Compare.pm
 *
 * Partially ported to PHP for the symfony project by Fabien Potencier, as part
 * of the sfFinder class. Port completed and broken out into its own package
 * by David Naughton.
 *
 * @package Number_Compare   
 *
 * @author    David Naughton <nihiliad@gmail.com>
 * @author    Fabien Potencier <fabien.potencier@gmail.com>
 * @author    Richard Clamp <richardc@unixbeard.net>
 * @copyright 2009 David Naughton <nihiliad@gmail.com>
 * @copyright 2004-2005 Fabien Potencier <fabien.potencier@gmail.com>
 * @copyright 2002 Richard Clamp <richardc@unixbeard.net>
 * @see       http://physics.nist.gov/cuu/Units/binary.html
 * @version   1.0.0
 */
class Number_Compare
{
    protected $test = '';
    
    /**
     * Returns a new object that compares the specified test.
     *
     * @param $test
     * @return A Number_Compare instance.
     */
    public function __construct($test)
    {
        $this->test = $test;
    }
    
    /**
     * Compares the given number against the test represented by this instance. 
     *
     * @param $number
     * @return $boolean
     */
    public function test($number)
    {
        if (!preg_match('{^([<>]=?)?(.*?)([kmg]i?)?$}i', $this->test, $matches)) {
            throw new sfException(sprintf('don\'t understand "%s" as a test.', $this->test));
        }
        
        $target = array_key_exists(2, $matches) ? $matches[2] : '';
        $magnitude = array_key_exists(3, $matches) ? $matches[3] : '';
        if (strtolower($magnitude) == 'k') {
            $target *=           1000;
        }
        if (strtolower($magnitude) == 'ki') {
            $target *=           1024;
        }
        if (strtolower($magnitude) == 'm') {
            $target *=        1000000;
        }
        if (strtolower($magnitude) == 'mi') {
            $target *=      1024*1024;
        }
        if (strtolower($magnitude) == 'g') {
            $target *=     1000000000;
        }
        if (strtolower($magnitude) == 'gi') {
            $target *= 1024*1024*1024;
        }
        
        $comparison = array_key_exists(1, $matches) ? $matches[1] : '==';
        if ($comparison == '==' || $comparison == '') {
            return($number == $target);
        } else if ($comparison == '>') {
            return($number > $target);
        } else if ($comparison == '>=') {
            return($number >= $target);
        } else if ($comparison == '<') {
            return($number < $target);
        } else if ($comparison == '<=') {
            return($number <= $target);
        }
        
        return false;
    }
} // end class Number_Compare
