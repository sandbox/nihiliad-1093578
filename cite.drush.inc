<?php // cite.drush.inc

// TODO: Move this, and all other ETL stuff, to a cite_etl module.
// Also, is this a drush function, i.e. does it belong here?
/*
function cite_help($section) {
    switch ($section) {
        case 'drush:cite-load':
            print t('Usage: drush cite-load [options]') . "\n\n";
            print t('Options: ') . "\n";
            foreach (cite_get_options() as $option => $description) {

                // Some descriptions are longer than 80 chars...
                $description_parts = split('/\n+/', $description);
                $rows[] = array($option, array_shift($description_parts));
                foreach ($description_parts as $part)
                {
                    $rows[] = array('',$part);
                }
            }
            drush_print_table($rows, 2);
    }
}
*/

/**
 *
 * TODO: Don't think we're using this. Probably should be updated or deleted.
 *
 * Get the available options for the "cite-load" drush command.
 *
 * @return
 *    An associative array containing the option definition as the key, and the
 *    description as the value, for each of the available options.
 */
function cite_get_options() {
  $options = array(
   '--E=<extractor class>'   => dt("Base name of an extractor class, excluding
                                the CiteETL/E/ parent path & '.php'. Required."),
   '--T=<transformer class>' => dt("Base name of an transformer class, excluding
                                the CiteETL/T/ parent path & '.php'. Required."),
   '--L=<loader class>'      => dt("Base name of an loader class, excluding the
                                CiteETL/L/ parent path & '.php'. Optional:
                                default is 'Loader'.\n"),
   '--dbuser=<db username>'  => dt("Optional: 'cite-load' will authenticate the
                                user only if both dbuser & dbpass are present."),
   '--dbpass=<db password>'  => dt("Optional: 'cite-load' will authenticate the
                                user only if both dbuser & dbpass are present."),
   '--memory_limit=<memory limit>' => dt('Optional: default is 512M.'),
  );
  return $options;
}

function cite_drush_command() {
  
    $items = array(
        'cite-load' => array(
            'description' => dt('Load data to create new citations.')
            /* This doesn't seem to do anything...
            'arguments' => array('options' => dt('Run "drush help cite-load" for available options')),
            */
        ),
        'cite-batch-insert' => array(
            // TODO: Improve the name and/or description of this command.
            'description' => dt('Load data to create new citations, the new-and-better-way!')
        ),
        'cite-batch-update' => array(
            // TODO: Improve the name and/or description of this command.
            'description' => dt('Load data to update citations, the new-and-better-way!')
        ),
        'cite-internal-update' => array(
            // TODO: Improve the name and/or description of this command.
            'description' => dt('Update citations based on an a query of the Drupal database itself.')
        ),
    );
    return $items;
}

function cite_modify_include_path( $path_to_add )
{
    $include_paths = array();

    if ($path_to_add) {
        $include_paths[] = $path_to_add;
    }

    $cite_module_path = drupal_get_path('module', 'cite');
    $old_include_path = get_include_path();

    $include_paths[] = "./$cite_module_path";
    $include_paths[] = "./$cite_module_path/contrib";
    $include_paths[] = $old_include_path;

    $new_include_path = join(PATH_SEPARATOR, $include_paths);

    return array($old_include_path, $new_include_path);
}

function drush_cite_load()
{
    $options = array();
    foreach (array('include_path','config_base','drupal_config','mysql_config','etl_config') as $option) {
        $options[$option] = drush_get_option($option); 
    }

    echo "options = "; print_r( $options );

    list($old_include_path, $new_include_path) = 
        cite_modify_include_path( $options['include_path'] );
    set_include_path( $new_include_path );
    echo "include_path = " . get_include_path() . "\n";
    
    //require_once 'Autoloader.php';
    //$a = new Autoloader();

    $config_base = array_key_exists('config_base', $options)
        ? $options['config_base']
        : null;
    $config_files = array();
    foreach (array('drupal','mysql','etl') as $config_type) {
        $config_file = $options[$config_type . '_config'];
        if ($config_base) {
            $config_file = "$config_base/$config_file";
        }
        $config_files[$config_type] = $config_file;
    }

    require_once 'Config/JSON.php';
    $etl_config = new Config_JSON(array(
        '_json_file' => $config_files['etl'],
        '_required_properties' => array(
            'extractor_factory_class',
            'transformer_class',
            //'loader_class = CiteETL_L_Loader', // TODO: Fix bugs that prevent setting defaults & overrides!
            'loader_class',
            'loader_log_message',
            'input_directory',
            'file_set_class = File_Set_DateSequence',
            'file_suffix = .xml',
            'memory_limit = 2G',
        ),
    ));

    // Batch loading will often require more than the default memory.
    ini_set('memory_limit', $etl_config->memory_limit);

    if (isset($etl_config->log)) {
        $log = $etl_config->log;
        //echo "log = $log\n";
        $log_fh = fopen( $log, 'a' );
        if ( $log_fh == null ) {
            throw new Exception( "Could not open log file '$log'" );
        }
        fclose( $log_fh );
        ini_set('error_log', $log);
    }
    ini_set('log_errors', true);

    $drupal_config = new Config_JSON(array(
        '_json_file' => $config_files['drupal'],
        '_required_properties' => array(
            'username',
            'password',
        ),
    ));
    // Will this fix the "must join a group" error? Yes!
    user_authenticate($drupal_config->username, $drupal_config->password);

    $mysql_config = new Config_JSON(array(
        '_json_file' => $config_files['mysql'],
        '_required_properties' => array(
            'host = localhost',
            'username',
            'password',
            'database',
        ),
    ));
    $mysqli = new mysqli(
        $mysql_config->host,
        $mysql_config->username,
        $mysql_config->password,
        $mysql_config->database
    );
    if (mysqli_connect_errno()) {
        throw new Exception("Connect failed: " . mysqli_connect_error());
    }

    // Load classes configured for ETL:
    foreach (array('extractor_factory_class','transformer_class','loader_class','file_set_class') as $class) {
        $class_file = preg_replace('/_/', '/', $etl_config->$class);
        $class_file .= '.php';
        require_once $class_file;
    }

    $extractor_factory = new $etl_config->extractor_factory_class();
    $transformer = new $etl_config->transformer_class();
    $loader = new $etl_config->loader_class(array(
        'mysqli' => $mysqli,
        'log_message' => $etl_config->loader_log_message,
    ));

    $file_set = new $etl_config->file_set_class(array(
        'directory' => $etl_config->input_directory,
        'suffix' => $etl_config->file_suffix,
    ));

    $cite_etl_params = array(
        'extractor_factory' => $extractor_factory,
        'transformer' => $transformer,
        'loader' => $loader,
        'file_names' => $file_set->members(),
    );
    //print_r( $cite_etl_params );

    require_once 'CiteETL.php';
    $etl = new CiteETL( $cite_etl_params );
    $etl->run();
}

function drush_cite_batch_insert()
{
    $options = array();
    foreach (array('include_path','config_base','drupal_config','mysql_config','etl_config') as $option) {
        $options[$option] = drush_get_option($option); 
    }

    echo "options = "; print_r( $options );

    list($old_include_path, $new_include_path) = 
        cite_modify_include_path( $options['include_path'] );
    set_include_path( $new_include_path );
    echo "include_path = " . get_include_path() . "\n";
    
    //require_once 'Autoloader.php';
    //$a = new Autoloader();

    $config_base = array_key_exists('config_base', $options)
        ? $options['config_base']
        : null;
    $config_files = array();
    foreach (array('drupal','mysql','etl') as $config_type) {
        $config_file = $options[$config_type . '_config'];
        if ($config_base) {
            $config_file = "$config_base/$config_file";
        }
        $config_files[$config_type] = $config_file;
    }

    require_once 'Config/JSON.php';
    $etl_config = new Config_JSON(array(
        '_json_file' => $config_files['etl'],
        '_required_properties' => array(
            'record_iterator_factory_class',
            'transformer_class',
            //'loader_class = CiteETL_L_Loader', // TODO: Fix bugs that prevent setting defaults & overrides!
            'loader_class',
            'loader_log_message',
            'input_directory',
            'file_set_class = File_Set_DateSequence',
            'file_suffix = .xml',
            'memory_limit = 2G',
        ),
    ));

    // Batch loading will often require more than the default memory.
    ini_set('memory_limit', $etl_config->memory_limit);

    if (isset($etl_config->log)) {
        $log = $etl_config->log;
        //echo "log = $log\n";
        $log_fh = fopen( $log, 'a' );
        if ( $log_fh == null ) {
            throw new Exception( "Could not open log file '$log'" );
        }
        fclose( $log_fh );
        ini_set('error_log', $log);
    }
    ini_set('log_errors', true);

    $drupal_config = new Config_JSON(array(
        '_json_file' => $config_files['drupal'],
        '_required_properties' => array(
            'username',
            'password',
        ),
    ));
    // Will this fix the "must join a group" error? Yes!
    user_authenticate($drupal_config->username, $drupal_config->password);

    $mysql_config = new Config_JSON(array(
        '_json_file' => $config_files['mysql'],
        '_required_properties' => array(
            'host = localhost',
            'username',
            'password',
            'database',
        ),
    ));
    $mysqli = new mysqli(
        $mysql_config->host,
        $mysql_config->username,
        $mysql_config->password,
        $mysql_config->database
    );
    if (mysqli_connect_errno()) {
        throw new Exception("Connect failed: " . mysqli_connect_error());
    }

    // Load classes configured for ETL:
    foreach (array('record_iterator_factory_class','transformer_class','loader_class','file_set_class') as $class) {
        $class_file = preg_replace('/_/', '/', $etl_config->$class);
        $class_file .= '.php';
        require_once $class_file;
    }

    $record_iterator_factory = new $etl_config->record_iterator_factory_class();
    $transformer = new $etl_config->transformer_class();
    $loader = new $etl_config->loader_class(array(
        'mysqli' => $mysqli,
        'log_message' => $etl_config->loader_log_message,
    ));

    $file_set = new $etl_config->file_set_class(array(
        'directory' => $etl_config->input_directory,
        'suffix' => $etl_config->file_suffix,
    ));

/*
    $cite_etl_params = array(
        'extractor_factory' => $extractor_factory,
        'transformer' => $transformer,
        'loader' => $loader,
        'file_names' => $file_set->members(),
    );
    //print_r( $cite_etl_params );
*/

    require_once 'CompressingFileIterator.php';
    require_once 'Pipeline.php';
    require_once 'Pipeline/Processor/XMLRecordIteratorFactory.php';
    require_once 'Pipeline/IteratorProcessor/XMLRecordTransformer.php';
    require_once 'Pipeline/Processor/DrupalCiteLoader.php';

    $pipeline = new Pipeline(
        new CompressingFileIterator( $file_set->members() ),
        new Pipeline_Processor_XMLRecordIteratorFactory( $record_iterator_factory ),
        new Pipeline_IteratorProcessor_XMLRecordTransformer( $transformer ),
        new Pipeline_Processor_DrupalCiteLoader( $loader )
    );

    $iterator = $pipeline->iterator();
    $iterator->rewind();
    while ($iterator->valid()) {

        $pipeline_context = $iterator->pipeline_context();
        echo "pipeline_context = "; print_r( $pipeline_context ); echo "\n";
        $iterator->next();
    }

    /*
    require_once 'CiteETL.php';
    $etl = new CiteETL( $cite_etl_params );
    $etl->run();
    */
}

function drush_cite_batch_update()
{
    $options = array();
    foreach (array('include_path','config_base','drupal_config','mysql_config','update_config') as $option) {
        $options[$option] = drush_get_option($option); 
    }

    echo "options = "; print_r( $options );

    list($old_include_path, $new_include_path) = 
        cite_modify_include_path( $options['include_path'] );
    set_include_path( $new_include_path );
    echo "include_path = " . get_include_path() . "\n";
    
    //require_once 'Autoloader.php';
    //$a = new Autoloader();

    $config_base = array_key_exists('config_base', $options)
        ? $options['config_base']
        : null;
    $config_files = array();
    foreach (array('drupal','mysql','update') as $config_type) {
        $config_file = $options[$config_type . '_config'];
        if ($config_base) {
            $config_file = "$config_base/$config_file";
        }
        $config_files[$config_type] = $config_file;
    }

    require_once 'Config/JSON.php';
    $update_config = new Config_JSON(array(
        '_json_file' => $config_files['update'],
        '_required_properties' => array(
            'record_iterator_factory_class',
            'transformer_class',
            //'loader_class = CiteETL_L_Loader', // TODO: Fix bugs that prevent setting defaults & overrides!
            'updater_class',
            'updater_log_message',
            'input_directory',
            'file_set_class = File_Set_DateSequence',
            'file_suffix = .xml',
            'memory_limit = 2G',
        ),
    ));

    // Batch loading will often require more than the default memory.
    ini_set('memory_limit', $update_config->memory_limit);

    if (isset($update_config->log)) {
        $log = $update_config->log;
        //echo "log = $log\n";
        $log_fh = fopen( $log, 'a' );
        if ( $log_fh == null ) {
            throw new Exception( "Could not open log file '$log'" );
        }
        fclose( $log_fh );
        ini_set('error_log', $log);
    }
    ini_set('log_errors', true);

    $drupal_config = new Config_JSON(array(
        '_json_file' => $config_files['drupal'],
        '_required_properties' => array(
            'username',
            'password',
        ),
    ));
    // Will this fix the "must join a group" error? Yes!
    user_authenticate($drupal_config->username, $drupal_config->password);

    $mysql_config = new Config_JSON(array(
        '_json_file' => $config_files['mysql'],
        '_required_properties' => array(
            'host = localhost',
            'username',
            'password',
            'database',
        ),
    ));
    $mysqli = new mysqli(
        $mysql_config->host,
        $mysql_config->username,
        $mysql_config->password,
        $mysql_config->database
    );
    if (mysqli_connect_errno()) {
        throw new Exception("Connect failed: " . mysqli_connect_error());
    }

    // Load classes configured for ETL:
    foreach (array('record_iterator_factory_class','transformer_class','updater_class','file_set_class') as $class) {
        $class_file = preg_replace('/_/', '/', $update_config->$class);
        $class_file .= '.php';
        require_once $class_file;
    }

    $record_iterator_factory = new $update_config->record_iterator_factory_class();
    $transformer = new $update_config->transformer_class();
    $updater = new $update_config->updater_class(array(
        'mysqli' => $mysqli,
        'log_message' => $update_config->updater_log_message,
    ));

    $file_set = new $update_config->file_set_class(array(
        'directory' => $update_config->input_directory,
        'suffix' => $update_config->file_suffix,
    ));

    require_once 'CompressingFileIterator.php';
    require_once 'Pipeline.php';
    require_once 'Pipeline/Processor/XMLRecordIteratorFactory.php';
    require_once 'Pipeline/IteratorProcessor/XMLRecordTransformer.php';
    require_once 'Pipeline/Processor/DrupalCiteUpdater.php';

    $pipeline = new Pipeline(
        new CompressingFileIterator( $file_set->members() ),
        new Pipeline_Processor_XMLRecordIteratorFactory( $record_iterator_factory ),
        new Pipeline_IteratorProcessor_XMLRecordTransformer( $transformer ),
        new Pipeline_Processor_DrupalCiteUpdater( $updater )
    );

    $iterator = $pipeline->iterator();
    $iterator->rewind();
    while ($iterator->valid()) {

        $pipeline_context = $iterator->pipeline_context();
        //echo "pipeline_context = "; print_r( $pipeline_context ); echo "\n";
        $iterator->next();
    }
}

function drush_cite_internal_update()
{
    $options = array();
    foreach (array('include_path','config_base','drupal_config','mysql_config','update_config') as $option) {
        $options[$option] = drush_get_option($option); 
    }

    echo "options = "; print_r( $options );

    list($old_include_path, $new_include_path) = 
        cite_modify_include_path( $options['include_path'] );
    set_include_path( $new_include_path );
    echo "include_path = " . get_include_path() . "\n";
    
    //require_once 'Autoloader.php';
    //$a = new Autoloader();

    $config_base = array_key_exists('config_base', $options)
        ? $options['config_base']
        : null;
    $config_files = array();
    foreach (array('drupal','mysql','update') as $config_type) {
        $config_file = $options[$config_type . '_config'];
        if ($config_base) {
            $config_file = "$config_base/$config_file";
        }
        $config_files[$config_type] = $config_file;
    }

    require_once 'Config/JSON.php';
    $update_config = new Config_JSON(array(
        '_json_file' => $config_files['update'],
        '_required_properties' => array(
            'query',
            'transformer_class',
            //'loader_class = CiteETL_L_Loader', // TODO: Fix bugs that prevent setting defaults & overrides!
            'updater_class',
            'updater_log_message',
            'memory_limit = 2G',
        ),
    ));

    // Batch loading will often require more than the default memory.
    ini_set('memory_limit', $update_config->memory_limit);

    if (isset($update_config->log)) {
        $log = $update_config->log;
        //echo "log = $log\n";
        $log_fh = fopen( $log, 'a' );
        if ( $log_fh == null ) {
            throw new Exception( "Could not open log file '$log'" );
        }
        fclose( $log_fh );
        ini_set('error_log', $log);
    }
    ini_set('log_errors', true);

    $drupal_config = new Config_JSON(array(
        '_json_file' => $config_files['drupal'],
        '_required_properties' => array(
            'username',
            'password',
        ),
    ));
    // Will this fix the "must join a group" error? Yes!
    user_authenticate($drupal_config->username, $drupal_config->password);

    $mysql_config = new Config_JSON(array(
        '_json_file' => $config_files['mysql'],
        '_required_properties' => array(
            'host = localhost',
            'username',
            'password',
            'database',
        ),
    ));
    $mysqli = new mysqli(
        $mysql_config->host,
        $mysql_config->username,
        $mysql_config->password,
        $mysql_config->database
    );
    if (mysqli_connect_errno()) {
        throw new Exception("Connect failed: " . mysqli_connect_error());
    }

    // Load classes configured for ETL:
    foreach (array('transformer_class','updater_class') as $class) {
        $class_file = preg_replace('/_/', '/', $update_config->$class);
        $class_file .= '.php';
        require_once $class_file;
    }

    $transformer = new $update_config->transformer_class();
    $updater = new $update_config->updater_class(array(
        'mysqli' => $mysqli,
        'log_message' => $update_config->updater_log_message,
    ));

    require_once 'MySQLiResultIterator.php';
    require_once 'Pipeline.php';
    require_once 'Pipeline/Processor/MySQLiRowTransformer.php';
    require_once 'Pipeline/Processor/DrupalCiteUpdater.php';

    $pipeline = new Pipeline(
        new MySQLiResultIterator( $mysqli, $update_config->query ),
        new Pipeline_Processor_MySQLiRowTransformer( $transformer ),
        new Pipeline_Processor_DrupalCiteUpdater( $updater )
    );

    $iterator = $pipeline->iterator();
    $iterator->rewind();
    while ($iterator->valid()) {

        $pipeline_context = $iterator->pipeline_context();
        //echo "pipeline_context = "; print_r( $pipeline_context ); echo "\n";
        $iterator->next();
    }
}

