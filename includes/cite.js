$(document).ready(function(){
  
  var is_checked = $("#edit-field-submission-policy-ok-value").attr('checked');  
  if (is_checked == false) {
    $("#field-files-items").hide();
    $(".cite-licensing").hide();    
  }
  
  $("#edit-field-submission-policy-ok-value").click(function(){
    var is_checked = $("#edit-field-submission-policy-ok-value").attr('checked');
    if (is_checked == false) {
      $("#field-files-items").hide();
      $(".cite-licensing").hide();      
    }
    else {
      $("#field-files-items").show();
      $(".cite-licensing").show();      
    }    
  });  
});

function textbox_to_dropdown_init(options, name, label) {
	textbox_to_dropdown(options, name, label);

	//bind event to to "add another item" buttons	
	var events = $("#field-"+ name +"-items :submit").data('events');
	$("#field-"+ name +"-items :submit").unbind();

	// call previously-bound functions at the end of my event-handler
	for (var i in events.mousedown) {
		if (events.mousedown.hasOwnProperty(i)) { events.mousedown[i](); }
	}
}



function textbox_to_dropdown(options, name, label) {
	var count = $("#field_"+ name +"_values tr").size() - 2;
	count = (count > 0) ? count : 1;
	for (var delta = 0; delta <= count; delta++) {			
		var selected = $('#edit-field-'+ name +'-'+ delta +'-value').val();
		var select = '';	
		select = '<select name="'+ name +'_select-'+ delta +'" id="'+ name +'_select-'+ delta +'">';		
		$.each(options, function(key, value) { 
			if (key == selected) {
				select += '<option value="'+ key +'" SELECTED>'+ value +'</option>';
			}
			else {
				select += '<option value="'+ key +'">'+ value +'</option>';
			}
		});		
		select += '</select>';
		$("#edit-field-"+ name +"-"+ delta +"-value-wrapper").html(
			'<label for="'+ name +'_select">'+ label +' </label>' +
			select	+
			'<input type="hidden" maxlength="255" name="field_'+ name +'['+ delta +'][value]" id="edit-field-'+ name +'-'+ delta +'-value" size="60" value="'+ selected +'" class="form-text">'
		);			
	}

	$("body.node-type-cite #node-form").submit(function() {
		var count = $("#field_"+ name +"_values tr").size() - 2;		
		for (var delta = 0; delta <= count; delta++) {			
			var lang = $("#"+ name +"_select-"+ delta).val();	
			$('#edit-field-'+ name +'-'+ delta +'-value').val(lang);
		}	
	});
}


function cite_language_options() {
	return {
		xxx : 'None Selected',
		afr : 'Afrikaans',
		alb : 'Albanian',
		amh : 'Amharic',
		ara : 'Arabic',
		arm : 'Armenian',
		aze : 'Azerbaijani',
		ben : 'Bengali',
		bos : 'Bosnian',
		bul : 'Bulgarian',
		cat : 'Catalan',
		chi : 'Chinese',
		cze : 'Czech',
		dan : 'Danish',
		dut : 'Dutch',
		eng : 'English',    
		epo : 'Esperanto',
		est : 'Estonian',
		fin : 'Finnish',
		fre : 'French',
		geo : 'Georgian',
		ger : 'German',
		gla : 'Scottish Gaelic',
		gre : 'Greek, Modern',
		heb : 'Hebrew',
		hin : 'Hindi',
		hun : 'Hungarian',
		ice : 'Icelandic',
		ind : 'Indonesian',
		ita : 'Italian',
		jpn : 'Japanese',
		kin : 'Kinyarwanda',
		kor : 'Korean',
		lat : 'Latin',
		lav : 'Latvian',
		lit : 'Lithuanian',
		mac : 'Macedonian',
		mal : 'Malayalam',
		mao : 'Maori',
		may : 'Malay',
		mul : 'Multiple languages',
		nor : 'Norwegian',
		per : 'Persian',
		pol : 'Polish',
		por : 'Portuguese',
		pus : 'Pushto',
		rum : 'Romanian, Rumanian',
		rus : 'Russian',
		san : 'Sanskrit',
		scc : 'Serbian',
		scr : 'Croatian',
		slo : 'Slovak',
		slv : 'Slovenian',
		spa : 'Spanish',
		swe : 'Swedish',
		tha : 'Thai',
		tur : 'Turkish',
		ukr : 'Ukrainian',
		urd : 'Urdu',
		vie : 'Vietnamese',
		wel : 'Welsh'
	};
}